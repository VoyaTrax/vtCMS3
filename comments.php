<?php
/**
 * XOOPS feed creator
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @since           2.0.0
 * @version         $Id: comments.php 10330 2012-12-08 19:11:36Z trabis $
 */
if (!function_exists('xoopsTweetString')) {
    function xoopsTweetString($title, $doit = false, $wordlen = 4)
    {
        if ($doit == true) {
            $title_array = explode(' ', $title);
            $title = '';
            foreach ($title_array as $item) {
                if (strlen($item) > $wordlen) {
                    $title .= ' #' . $item;
                } else {
                    $title .= ' ' . $item;
                }
            }
        }
        return trim($title);
    }
}

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mainfile.php';
$xoops = Xoops::getInstance();
$xoops->disableErrorReporting();

if (function_exists('mb_http_output')) {
    mb_http_output('pass');
}
header('Content-Type:text/xml; charset=utf-8');

$tpl = new XoopsTpl();
$tpl->caching = 2;
$tpl->cache_lifetime = 3600;
if (!$tpl->is_cached('module:system|system_rss.html')) {
    $tpl->assign('channel_title', XoopsLocal::convert_encoding(htmlspecialchars($xoops->getConfig('sitename'), ENT_QUOTES)));
    $tpl->assign('channel_link', XOOPS_URL . '/');
    $tpl->assign('channel_desc', XoopsLocal::convert_encoding(htmlspecialchars($xoops->getConfig('slogon'), ENT_QUOTES)));
    $tpl->assign('channel_lastbuild', XoopsLocal::formatTimestamp(time(), 'rss'));
    $tpl->assign('channel_webmaster', $xoops->checkEmail($xoops->getConfig('adminmail'), true));
    $tpl->assign('channel_editor', $xoops->checkEmail($xoops->getConfig('adminmail'), true));
    $tpl->assign('channel_category', 'Comments');
    $tpl->assign('channel_generator', XOOPS_VERSION);
    $tpl->assign('channel_language', _LANGCODE);
    $tpl->assign('image_url', XOOPS_URL . '/images/logo.png');
    $dimention = getimagesize(XOOPS_ROOT_PATH . '/images/logo.png');
    if (empty($dimention[0])) {
        $width = 88;
    } else {
        $width = ($dimention[0] > 144) ? 144 : $dimention[0];
    }
    if (empty($dimention[1])) {
        $height = 31;
    } else {
        $height = ($dimention[1] > 400) ? 400 : $dimention[1];
    }
    $tpl->assign('image_width', $width);
    $tpl->assign('image_height', $height);

    $comments_handler = $xoops->getHandler('comment');
    $criteria = new CriteriaCompo(new Criteria('com_status', 2));
    if (isset($_REQUEST['modid']) && is_numeric($_REQUEST['modid'])) {
        $criteria->add(new Criteria('`com_modid`', intval($_REQUEST['modid'])));
    }
    if (isset($_REQUEST['uid']) && is_numeric($_REQUEST['uid'])) {
        $criteria->add(new Criteria('`com_uid`', intval($_REQUEST['uid'])));
    }
    $criteria->setLimit(((isset($_REQUEST['num']) && is_numeric($_REQUEST['num']) && intval($_REQUEST['num']) > 0) ? intval($_REQUEST['num']) : 10));
    $criteria->setSort('`com_created`');
    $criteria->setOrder('DESC');

    $module_handler = $xoops->getModuleHandler();
    $comments = $comments_handler->getObjects($criteria);
    if (!empty($comments) && is_array($comments)) {
        $myts =& MyTextSanitizer::getInstance();
        foreach ($comments as $comment) {
            $xoModule = $module_handler->get($comment->getVar('com_modid'));
            $comment_config = $xoModule->getInfo('comments');
            $tpl->append('items', array(
                'title'       => XoopsLocal::convert_encoding(htmlspecialchars(xoopsTweetString($comment->getVar('com_title'), isset($_REQUEST['tweet']), ((isset($_REQUEST['tweetlen']) && is_numeric($_REQUEST['tweetlen']) && intval($_REQUEST['tweetlen']) > 0) ? intval($_REQUEST['tweetlen']) : 4)), ENT_QUOTES)),
                'link'        => htmlspecialchars(XOOPS_URL . '/modules/' . $xoModule->getVar('dirname') . '/' . $comment_config['pageName'] . '?' . $comment_config['itemName'] . '=' . $comment->getVar('com_itemid') . '&com_id=' . $comment->getVar('com_id') . '&com_rootid=' . $comment->getVar('com_rootid') . '&com_mode=' . $xoops->getConfig('com_mode') . '&com_order=0' . (strlen($comment->getVar('com_exparams')) > 0 ? '&' . $comment->getVar('com_exparams') : '') . '#comment' . $comment->getVar('com_id')),
                'guid'        => htmlspecialchars(XOOPS_URL . '/modules/' . $xoModule->getVar('dirname') . '/' . $comment_config['pageName'] . '?' . $comment_config['itemName'] . '=' . $comment->getVar('com_itemid') . '&com_id=' . $comment->getVar('com_id') . '&com_rootid=' . $comment->getVar('com_rootid') . '&com_mode=' . $xoops->getCOnfig('com_mode') . '&com_order=0' . (strlen($comment->getVar('com_exparams')) > 0 ? '&' . $comment->getVar('com_exparams') : '') . '#comment' . $comment->getVar('com_id')),
                'category'    => XoopsLocal::convert_encoding(htmlspecialchars($xoModule->getVar('name'))),
                'pubdate'     => XoopsLocal::formatTimestamp($comment->getVar('com_created'), 'rss'),
                'description' => XoopsLocal::convert_encoding(htmlspecialchars($myts->displayTarea($comment->getVar('com_text'), $comment->getVar('dohtml'), $comment->getVar('dosmiley'), $comment->getVar('doxcode'), $comment->getVar('doimage'), $comment->getVar('dobr')), ENT_QUOTES))
            ));
        }
    }
}
$tpl->display('system_rss.html');
?>