<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Abstract base class for XOOPS Database access classes
 *
 * @copyright       The XOOPS project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         class
 * @subpackage      database
 * @since           1.0.0
 * @author          Kazumi Ono <onokazu@xoops.org>
 * @version         $Id: database.php 10328 2012-12-07 00:56:07Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

/**
 * Abstract base class for Database access classes
 *
 * @abstract
 * @author     Kazumi Ono <onokazu@xoops.org>
 * @package    kernel
 * @subpackage database
 */
abstract class XoopsDatabase
{
    /**
     * Database connection
     *
     * @var resource
     */
    public $conn;

    /**
     * Prefix for tables in the database
     *
     * @var string
     */
    public $prefix = '';

    /**
     * If statements that modify the database are selected
     *
     * @var boolean
     */
    public $allowWebChanges = false;


    /**
     * set the prefix for tables in the database
     *
     * @param string $value table prefix
     */
    public function setPrefix($value)
    {
        $this->prefix = $value;
    }

    /**
     * attach the prefix.'_' to a given tablename
     * if tablename is empty, only prefix will be returned
     *
     * @param string $tablename tablename
     *
     * @return string prefixed tablename, just prefix if tablename is empty
     */
    public function prefix($tablename = '')
    {
        if ($tablename != '') {
            return $this->prefix . '_' . $tablename;
        } else {
            return $this->prefix;
        }
    }

    /**
     * @abstract
     *
     * @param bool $selectdb
     *
     * @return void
     */
    abstract function connect($selectdb = true);

    /**
     * @param $sequence
     *
     * @abstract
     */
    abstract function genId($sequence);

    /**
     * @param $result
     *
     * @abstract
     */
    abstract function fetchRow($result);

    /**
     * @param $result
     *
     * @return array
     * @abstract
     */
    abstract function fetchArray($result);

    /**
     * @param $result
     *
     * @abstract
     */
    abstract function fetchBoth($result);

    /**
     * @param $result
     *
     * @abstract
     */
    abstract function fetchObject($result);

    /**
     * @abstract
     */
    abstract function getInsertId();

    /**
     * @param $result
     *
     * @abstract
     */
    abstract function getRowsNum($result);

    /**
     * @abstract
     */
    abstract function getAffectedRows();

    /**
     * @abstract
     */
    abstract function close();

    /**
     * @param $result
     *
     * @abstract
     */
    abstract function freeRecordSet($result);

    /**
     * @abstract
     */
    abstract function error();

    /**
     * @abstract
     */
    abstract function errno();

    /**
     * @param $str
     *
     * @abstract
     */
    abstract function quoteString($str);

    /**
     * @param $string
     *
     * @abstract
     */
    abstract function quote($string);

    /**
     * @param     $sql
     * @param int $limit
     * @param int $start
     *
     * @abstract
     */
    abstract function queryF($sql, $limit = 0, $start = 0);

    /**
     * @param     $sql
     * @param int $limit
     * @param int $start
     *
     * @abstract
     */
    abstract function query($sql, $limit = 0, $start = 0);

    /**
     * @param $file
     *
     * @abstract
     */
    abstract function queryFromFile($file);

    /**
     * @param $result
     * @param $offset
     *
     * @abstract
     */
    abstract function getFieldName($result, $offset);

    /**
     * @param $result
     * @param $offset
     *
     * @abstract
     */
    abstract function getFieldType($result, $offset);

    /**
     * @param $result
     *
     * @abstract
     */
    abstract function getFieldsNum($result);
}