<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Database manager for XOOPS
 *
 * @copyright       The XOOPS project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         class
 * @subpackage      database
 * @since           2.6.0
 * @author          Haruki Setoyama  <haruki@planewave.org>
 * @version         $Id: manager.php 10677 2013-01-06 01:12:16Z trabis $
 */

class XoopsDatabaseManager
{
    /**
     * @var array
     */
    private $s_tables = array();

    /**
     * @var array
     */
    private $f_tables = array();

    /**
     * @var XoopsDatabase
     */
    public $db;

    /**
     * @var array
     */
    public $successStrings = array();

    /**
     * @var array
     */
    public $failureStrings = array();

    /**
     *
     */
    public function __construct()
    {
        $xoops = Xoops::getInstance();
        $xoops->loadLanguage('database');
        $this->db = $xoops->db();
        $this->db->setPrefix(XOOPS_DB_PREFIX);
        $this->successStrings = array(
            'create' => _DB_TABLE_CREATED,
            'insert' => _DB_ROWS_INSERTED,
            'alter'  => _DB_TABLE_ALTERED,
            'drop'   => _DB_TABLE_DROPPED,
        );
        $this->failureStrings = array(
            'create' => _DB_TABLE_NOT_CREATED,
            'insert' => _DB_ROWS_FAILED,
            'alter'  => _DB_TABLE_NOT_ALTERED,
            'drop'   => _DB_TABLE_NOT_DROPPED,
        );
    }

    /**
     * @return bool
     */
    public function isConnectable()
    {
        return ($this->db->connect(false) != false) ? true : false;
    }

    /**
     * @return bool
     */
    public function dbExists()
    {
        return ($this->db->connect() != false) ? true : false;
    }

    /**
     * @return bool
     */
    public function createDB()
    {
        $this->db->connect(false);

        $result = $this->db->query("CREATE DATABASE " . XOOPS_DB_NAME);

        return ($result != false) ? true : false;
    }

    /**
     * @param string $sql_file_path
     * @param bool   $force
     *
     * @return bool
     */
    public function queryFromFile($sql_file_path, $force = false)
    {
        if (!XoopsLoad::fileExists($sql_file_path)) {
            return false;
        }
        $queryFunc = (bool)$force ? "queryF" : "query";
        $sql_query = trim(fread(fopen($sql_file_path, 'r'), filesize($sql_file_path)));
        SqlUtility::splitMySqlFile($pieces, $sql_query);
        $this->db->connect();
        foreach ($pieces as $piece) {
            $piece = trim($piece);
            // [0] contains the prefixed query
            // [4] contains unprefixed table name
            $prefixed_query = SqlUtility::prefixQuery($piece, $this->db->prefix());
            if ($prefixed_query != false) {
                $table = $this->db->prefix($prefixed_query[4]);
                if ($prefixed_query[1] == 'CREATE TABLE') {
                    if ($this->db->$queryFunc($prefixed_query[0]) != false) {
                        if (!isset($this->s_tables['create'][$table])) {
                            $this->s_tables['create'][$table] = 1;
                        }
                    } else {
                        if (!isset($this->f_tables['create'][$table])) {
                            $this->f_tables['create'][$table] = 1;
                        }
                    }
                } else {
                    if ($prefixed_query[1] == 'INSERT INTO') {
                        if ($this->db->$queryFunc($prefixed_query[0]) != false) {
                            if (!isset($this->s_tables['insert'][$table])) {
                                $this->s_tables['insert'][$table] = 1;
                            } else {
                                $this->s_tables['insert'][$table]++;
                            }
                        } else {
                            if (!isset($this->f_tables['insert'][$table])) {
                                $this->f_tables['insert'][$table] = 1;
                            } else {
                                $this->f_tables['insert'][$table]++;
                            }
                        }
                    } else {
                        if ($prefixed_query[1] == 'ALTER TABLE') {
                            if ($this->db->$queryFunc($prefixed_query[0]) != false) {
                                if (!isset($this->s_tables['alter'][$table])) {
                                    $this->s_tables['alter'][$table] = 1;
                                }
                            } else {
                                if (!isset($this->s_tables['alter'][$table])) {
                                    $this->f_tables['alter'][$table] = 1;
                                }
                            }
                        } else {
                            if ($prefixed_query[1] == 'DROP TABLE') {
                                if ($this->db->$queryFunc('DROP TABLE ' . $table) != false) {
                                    if (!isset($this->s_tables['drop'][$table])) {
                                        $this->s_tables['drop'][$table] = 1;
                                    }
                                } else {
                                    if (!isset($this->s_tables['drop'][$table])) {
                                        $this->f_tables['drop'][$table] = 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * @return string
     */
    public function report()
    {
        $commands = array('create', 'insert', 'alter', 'drop');
        $content = '<ul class="log">';
        foreach ($commands as $cmd) {
            if (!@empty($this->s_tables[$cmd])) {
                foreach ($this->s_tables[$cmd] as $key => $val) {
                    $content .= '<li class="success">';
                    $content .= ($cmd != 'insert') ? sprintf($this->successStrings[$cmd], $key) : sprintf($this->successStrings[$cmd], $val, $key);
                    $content .= "</li>\n";
                }
            }
        }
        foreach ($commands as $cmd) {
            if (!@empty($this->f_tables[$cmd])) {
                foreach ($this->f_tables[$cmd] as $key => $val) {
                    $content .= '<li class="failure">';
                    $content .= ($cmd != 'insert') ? sprintf($this->failureStrings[$cmd], $key) : sprintf($this->failureStrings[$cmd], $val, $key);
                    $content .= "</li>\n";
                }
            }
        }
        $content .= '</ul>';
        return $content;
    }

    /**
     * @param string $sql
     *
     * @return mixed
     */
    public function query($sql)
    {
        $this->db->connect();
        return $this->db->query($sql);
    }

    /**
     * @param $table
     *
     * @return string
     */
    public function prefix($table)
    {
        $this->db->connect();
        return $this->db->prefix($table);
    }

    /**
     * @param $ret
     *
     * @return array
     */
    public function fetchArray($ret)
    {
        $this->db->connect();
        return $this->db->fetchArray($ret);
    }

    /**
     * @param $table
     * @param $query
     *
     * @return bool|void
     */
    public function insert($table, $query)
    {
        $this->db->connect();
        $table = $this->db->prefix($table);
        $query = 'INSERT INTO ' . $table . ' ' . $query;
        if (!$this->db->queryF($query)) {
            if (!isset($this->f_tables['insert'][$table])) {
                $this->f_tables['insert'][$table] = 1;
            } else {
                $this->f_tables['insert'][$table]++;
            }
            return false;
        } else {
            if (!isset($this->s_tables['insert'][$table])) {
                $this->s_tables['insert'][$table] = 1;
            } else {
                $this->s_tables['insert'][$table]++;
            }
            return $this->db->getInsertId();
        }
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return (isset($this->f_tables)) ? true : false;
    }

    /**
     * @param $tables
     *
     * @return array
     */
    public function deleteTables($tables)
    {
        $deleted = array();
        $this->db->connect();
        foreach ($tables as $key => $val) {
            if (!$this->db->query("DROP TABLE " . $this->db->prefix($key))) {
                $deleted[] = $val;
            }
        }
        return $deleted;
    }

    /**
     * @param $table
     *
     * @return bool
     */
    public function tableExists($table)
    {
        $table = trim($table);
        $ret = false;
        if ($table != '') {
            $this->db->connect();
            $sql = 'SELECT COUNT(*) FROM ' . $this->db->prefix($table);
            $ret = (false != $this->db->query($sql)) ? true : false;
        }
        return $ret;
    }

    /**
     * This method allows to copy fields from one table to another
     *
     * @param array  $fieldsMap  Map of the fields
     *                           ex: array('oldfieldname' => 'newfieldname');
     * @param string $oTableName Old Table
     * @param string $nTableName New Table
     * @param bool   $dropTable  Drop old Table
     */
    public function copyFields($fieldsMap, $oTableName, $nTableName, $dropTable = false)
    {
        $sql = "SHOW COLUMNS FROM " . $this->db->prefix($oTableName);
        $result = $this->db->queryF($sql);
        if (($rows = $this->db->getRowsNum($result)) == count($fieldsMap)) {
            $sql = "SELECT * FROM " . $this->db->prefix($oTableName);
            $result = $this->db->queryF($sql);
            while ($myrow = $this->db->fetchArray($result)) {
                ksort($fieldsMap);
                ksort($myrow);
                $sql = "INSERT INTO `" . $this->db->prefix($nTableName) . "` " . "(`" . implode("`,`", $fieldsMap) . "`)" . " VALUES ('" . implode("','", $myrow) . "')";

                $this->db->queryF($sql);
            }
            if ($dropTable) {
                $sql = "DROP TABLE " . $this->db->prefix($oTableName);
                $this->db->queryF($sql);
            }
        }
    }
}