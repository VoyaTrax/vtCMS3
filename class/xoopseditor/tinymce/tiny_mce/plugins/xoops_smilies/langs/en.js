/**
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: en.js 10709 2013-01-08 14:44:03Z dugris $
 */

tinyMCE.addI18n('en.xoops_smilies',{
    desc : 'Insert smiley from Smilies module',
});