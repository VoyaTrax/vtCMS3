/**
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: en_dlg.js 10709 2013-01-08 14:44:03Z dugris $
 */

tinyMCE.addI18n('en.xoops_smilies_dlg',{
    title : 'Insert Smiley from Smilies module',
});