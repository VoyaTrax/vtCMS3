<?php
/**
 *  xoops_quote plugin for tinymce
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU public license
 * @package         class / xoopseditor
 * @subpackage      tinymce / xoops plugins
 * @since           2.6.0
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: xoops_quote.php 10713 2013-01-08 16:28:10Z dugris $
 */

$xoops_root_path = dirname( dirname ( dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) ) ) ) ;
include_once $xoops_root_path . '/mainfile.php';
defined('XOOPS_ROOT_PATH') or die('Restricted access');

$xoops = Xoops::getInstance();
$xoops->disableErrorReporting();
$xoops->loadLanguage('misc');

$xoops->simpleHeader(true);

$form = new XoopsThemeForm('', 'imagecat_form', '#', false, 'vertical');
$form->addElement( new XoopsFormTextArea(_MSC_TINYMCE_QUOTE_TITLE, 'text_id', '', 9, 7) );
/**
 * Buttons
 */
$button_tray = new XoopsFormElementTray('', '');
$button_tray->addElement(new XoopsFormHidden('op', 'save'));

$button = new XoopsFormButton('', 'submit', _SUBMIT, 'submit');
$button->setExtra('onclick="Xoops_quoteDialog.insert();"');
$button->setClass('btn btn-success');
$button_tray->addElement($button);

$button_2 = new XoopsFormButton('', 'reset', _RESET, 'reset');
$button_2->setClass('btn btn-warning');
$button_tray->addElement($button_2);

$button_3 = new XoopsFormButton('', 'button', _CLOSE, 'button');
$button_3->setExtra('onclick="tinyMCEPopup.close();"');
$button_3->setClass('btn btn-danger');
$button_tray->addElement($button_3);

$form->addElement($button_tray);

$xoopsTpl = new XoopsTpl();
$xoopsTpl->assign('js_file', 'js/xoops_quote.js');
$xoopsTpl->assign('css_file', 'css/xoops_quote.css');
$xoopsTpl->assign('form', $form->render());
$xoopsTpl->assign('include_html', '');

$xoopsTpl->display('module:system|system_tinymce.html');
$xoops->simpleFooter();
