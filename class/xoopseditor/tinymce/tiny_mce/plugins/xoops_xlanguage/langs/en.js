/**
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: en.js 10698 2013-01-07 23:28:45Z dugris $
 */

tinyMCE.addI18n('en.xoops_xlanguage',{
    desc : 'Insert multi-language content',
});