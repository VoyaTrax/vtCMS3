/**
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: en_dlg.js 10698 2013-01-07 23:28:45Z dugris $
 */

tinyMCE.addI18n('en.xoops_xlanguage_dlg',{
    chooselang : 'You must choose a language',
    maxstring : ' character(s) of %maxchar% entered',
    alertmaxstring : 'You have reached the maximum number of characters',
});