/**
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: en_dlg.js 10710 2013-01-08 14:44:59Z dugris $
 */

tinyMCE.addI18n('en.xoops_image_dlg',{
    dialog_title:"Xoops Imagebrowser",
    tab_listimages:"Images",
    tab_loadimage:"Add image",
    tab_listcategories:"Categories",
    tab_createcategory:"Add category",
    select_image: "click on a image to select"
});
