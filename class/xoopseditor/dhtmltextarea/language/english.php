<?php
/**
 * XOOPS editor
 *
 * @copyright The XOOPS project http://www.xoops.org/
 * @license GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author Taiwen Jiang (phppp or D.J.) <php_pp@hotmail.com>
 * @since 2.3.0
 * @version $Id: english.php 10056 2012-08-11 14:29:14Z beckmi $
 * @package xoopseditor
 */
/**
 * Assocated with editor_registry.php
 */
define('_XOOPS_EDITOR_DHTMLTEXTAREA','DHTML Form with xCode');

?>
