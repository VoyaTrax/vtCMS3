<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * XOOPS message list
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         core
 * @since           2.0.0
 * @version         $Id: readpmsg.php 10676 2013-01-05 21:21:40Z trabis $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mainfile.php';
$xoops = Xoops::getInstance();
$xoops->preload()->triggerEvent('core.readpmsg.start');

$xoops->loadLanguage('pmsg');

if (!$xoops->isUser()) {
    $xoops->redirect("user.php", 0);
    exit();
} else {
    $pm_handler = $xoops->getHandlerPrivmessage();
    if (!empty($_POST['delete'])) {
        if (!$xoops->security()->check()) {
            echo implode('<br />', $xoops->security()->getErrors());
            exit();
        } else if (empty($_REQUEST['ok'])) {
            $xoops->header();
            $xoops->confirm(array('ok' => 1, 'delete' => 1, 'msg_id'=> intval($_POST['msg_id'])), $_SERVER['REQUEST_URI'], _PM_SURE_TO_DELETE);
            $xoops->footer();
            exit();
        }
        $pm = $pm_handler->get(intval($_POST['msg_id']));
        if (!is_object($pm) || $pm->getVar('to_userid') != $xoops->user->getVar('uid') || !$pm_handler->delete($pm)) {
            exit();
        } else {
            $xoops->redirect("viewpmsg.php", 1, _PM_DELETED);
            exit();
        }
    }
    $id = !empty($_GET['msg_id']) ? intval($_GET['msg_id']) : 0;
    $start = !empty($_GET['start']) ? intval($_GET['start']) : 0;
    $total_messages = !empty($_GET['total_messages']) ? intval($_GET['total_messages']) : 0;
    $xoops->header();
    $criteria = new CriteriaCompo(new Criteria('to_userid', $xoops->user->getVar('uid')));
    if ($id) {
        $criteria->add(new Criteria('msg_id', $id));
    }
    $criteria->setLimit(1);
    $criteria->setStart($start);
    $criteria->setSort('msg_time');
    $pm_arr = $pm_handler->getObjects($criteria);
    echo "<div><h4>" . _PM_PRIVATEMESSAGE . "</h4></div><br /><a href='userinfo.php?uid=" . $xoops->user->getVar("uid") . "' title=''>" . _PM_PROFILE . "</a>&nbsp;<span class='bold'>&raquo;&raquo;</span>&nbsp;<a href='viewpmsg.php' title=''>" . _PM_INBOX . "</a>&nbsp;<span class='bold'>&raquo;&raquo;</span>&nbsp;\n";
    if (empty($pm_arr)) {
        echo '<br /><br />' . _PM_YOUDONTHAVE;
    } else {
        if (!$pm_handler->setRead($pm_arr[0])) {
            //echo "failed";
        }
        echo $pm_arr[0]->getVar("subject") . "<br /><form action='readpmsg.php' method='post' name='delete" . $pm_arr[0]->getVar("msg_id") . "'><table cellpadding='4' cellspacing='1' class='outer width100 bnone'><tr><th colspan='2'>" . _PM_FROM . "</th></tr><tr class='even'>\n";
        $poster = new XoopsUser($pm_arr[0]->getVar("from_userid"));
        if (!$poster->isActive()) {
            $poster = false;
        }
        echo "<td valign='top'>";
        if ($poster != false) { // we need to do this for deleted users
            echo "<a href='userinfo.php?uid=" . $poster->getVar("uid") . "' title=''>" . $poster->getVar("uname") . "</a><br />\n";
            if ($poster->getVar("user_avatar") != "") {
                echo "<img src='uploads/" . $poster->getVar("user_avatar") . "' alt='' /><br />\n";
            }
            if ($poster->getVar("user_from") != "") {
                echo _PM_FROMC . "" . $poster->getVar("user_from") . "<br /><br />\n";
            }
            if ($poster->isOnline()) {
                echo "<span class='red bold'>" . _PM_ONLINE . "</span><br /><br />\n";
            }
        } else {
            echo $xoops->getConfig('anonymous'); // we need to do this for deleted users
        }
//------------- mamba
        $iconName=$pm_arr[0]->getVar("msg_image", "E");
        if ($iconName != '') {
           echo "</td><td><img src='images/subject/" .$iconName . "' alt='' />&nbsp;" . _PM_SENTC . "" . XoopsLocal::formatTimestamp($pm_arr[0]->getVar("msg_time"));
        }
        else {
           echo "</td><td>" . _PM_SENTC . "" . XoopsLocal::formatTimestamp($pm_arr[0]->getVar("msg_time"));
        }
//------------- mamba

        echo "<hr /><br /><strong>" . $pm_arr[0]->getVar("subject") . "</strong><br /><br />\n";
        echo $pm_arr[0]->getVar("msg_text") . "<br /><br /></td></tr><tr class='foot'><td class='width20 txtleft' colspan='2'>";
        // we dont want to reply to a deleted user!
        if ($poster != false) {
            echo "<a href='#' onclick='javascript:openWithSelfMain(\"" . XOOPS_URL . "/pmlite.php?reply=1&amp;msg_id=" . $pm_arr[0]->getVar("msg_id") . "\",\"pmlite\",565,500);'><img src='" . XOOPS_URL . "/images/icons/reply.gif' alt='" . _PM_REPLY . "' /></a>\n";
        }
        echo "<input type='hidden' name='delete' value='1' />";
        echo $xoops->security()->getTokenHTML();
        echo "<input type='hidden' name='msg_id' value='" . $pm_arr[0]->getVar("msg_id") . "' />";
        echo "<a href='#" . $pm_arr[0]->getVar("msg_id") . "' onclick='javascript:document.delete" . $pm_arr[0]->getVar("msg_id") . ".submit();'><img src='" . XOOPS_URL . "/images/icons/delete.gif' alt='" . _PM_DELETE . "' /></a>";
        echo "</td></tr><tr><td class='txtright' colspan='2'>";
        $previous = $start - 1;
        $next = $start + 1;
        if ($previous >= 0) {
            echo "<a href='readpmsg.php?start=" . $previous . "&amp;total_messages=" . $total_messages . "' title=''>" . _PM_PREVIOUS . "</a> | ";
        } else {
            echo _PM_PREVIOUS . " | ";
        }
        if ($next < $total_messages) {
            echo "<a href='readpmsg.php?start=" . $next . "&amp;total_messages=" . $total_messages . "' title=''>" . _PM_NEXT . "</a>";
        } else {
            echo _PM_NEXT;
        }
        echo "</td></tr></table></form>\n";
    }
    $xoops->footer();
}