<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * XOOPS User
 *
 * See the enclosed file license.txt for licensing information.
 * If you did not receive this file, get it at http://www.fsf.org/copyleft/gpl.html
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU General Public License (GPL)
 * @package         core
 * @since           2.0.0
 * @author          Kazumi Ono <webmaster@myweb.ne.jp>
 * @version         $Id: user.php 10408 2012-12-16 18:43:15Z trabis $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mainfile.php';
$xoopsPreload = XoopsPreload::getInstance();
$xoopsPreload->triggerEvent('core.user.start');

$xoops = Xoops::getInstance();
$xoops->preload()->triggerEvent('core.user.start');

$xoops->loadLanguage('user');

$op = 'main';
if (isset($_POST['op'])) {
    $op = trim($_POST['op']);
} elseif (isset($_GET['op'])) {
    $op = trim($_GET['op']);
}

if ($op == 'login') {
    include_once $xoops->path('include/checklogin.php');
    exit();
}

if ($op == 'main') {
    if (!$xoops->isUser()) {
        $xoops->header('system_userform.html');
        $xoops->tpl()->assign('xoops_pagetitle', _LOGIN);
        $xoops->theme()->addMeta('meta', 'keywords', _USERNAME . ", " . _US_PASSWORD . ", " . _US_LOSTPASSWORD);
        $xoops->theme()->addMeta('meta', 'description', _US_LOSTPASSWORD . " " . _US_NOPROBLEM);
        $xoops->tpl()->assign('lang_login', _LOGIN);
        $xoops->tpl()->assign('lang_username', _USERNAME);
        if (isset($_GET['xoops_redirect'])) {
            $xoops->tpl()->assign('redirect_page', htmlspecialchars(trim($_GET['xoops_redirect']), ENT_QUOTES));
        }
        if ($xoops->getConfig('usercookie')) {
            $xoops->tpl()->assign('lang_rememberme', _US_REMEMBERME);
        }
        $xoops->tpl()->assign('lang_password', _PASSWORD);
        $xoops->tpl()->assign('lang_notregister', _US_NOTREGISTERED);
        $xoops->tpl()->assign('lang_lostpassword', _US_LOSTPASSWORD);
        $xoops->tpl()->assign('lang_noproblem', _US_NOPROBLEM);
        $xoops->tpl()->assign('lang_youremail', _US_YOUREMAIL);
        $xoops->tpl()->assign('lang_sendpassword', _US_SENDPASSWORD);
        $xoops->tpl()->assign('mailpasswd_token', $xoops->security()->createToken());
        $xoops->footer();
    }
    if (!empty($_GET['xoops_redirect'])) {
        $redirect = trim($_GET['xoops_redirect']);
        $isExternal = false;
        if ($pos = strpos($redirect, '://')) {
            $xoopsLocation = substr(XOOPS_URL, strpos(XOOPS_URL, '://') + 3);
            if (strcasecmp(substr($redirect, $pos + 3, strlen($xoopsLocation)), $xoopsLocation)) {
                $isExternal = true;
            }
        }
        if (!$isExternal) {
            header('Location: ' . $redirect);
            exit();
        }
    }
    header('Location: ' . XOOPS_URL . '/userinfo.php?uid=' . $xoopsUser->getVar('uid'));
    exit();
}

if ($op == 'logout') {
    $message = '';
    // Regenerate a new session id and destroy old session
    $xoops->getHandlerSession()->regenerate_id(true);
    $_SESSION = array();
    setcookie($xoops->getConfig('usercookie'), 0, -1, '/', XOOPS_COOKIE_DOMAIN, 0);
    setcookie($xoops->getConfig('usercookie'), 0, -1, '/');
    // clear entry from online users table
    if ($xoops->isUser()) {
        $xoops->getHandlerOnline()->destroy($xoops->user->getVar('uid'));
    }
    $message = _US_LOGGEDOUT . '<br />' . _US_THANKYOUFORVISIT;
    $xoops->redirect(XOOPS_URL . '/', 1, $message);
}

if ($op == 'delete') {
    $xoopsConfigUser = $xoops->getConfigs();
    if (!$xoops->isUser() || $xoopsConfigUser['self_delete'] != 1) {
        $xoops->redirect('index.php', 5, _US_NOPERMISS);
    } else {
        $groups = $xoops->user->getGroups();
        if (in_array(XOOPS_GROUP_ADMIN, $groups)) {
            // users in the webmasters group may not be deleted
            $xoops->redirect('user.php', 5, _US_ADMINNO);
        }
        $ok = !isset($_POST['ok']) ? 0 : intval($_POST['ok']);
        if ($ok != 1) {
            $xoops->header();
            $xoops->confirm(array('op' => 'delete', 'ok' => 1), 'user.php', _US_SURETODEL . '<br/>' . _US_REMOVEINFO);
            $xoops->footer();
        } else {
            $del_uid = $xoops->user->getVar("uid");
            $member_handler = $xoops->getHandlerMember();
            if (false != $member_handler->deleteUser($xoops->user)) {
                $xoops->getHandlerOnline()->destroy($del_uid);
                $xoops->getHandlerNotification()->unsubscribeByUser($del_uid);
                $xoops->redirect('index.php', 5, _US_BEENDELED);
            }
            $xoops->redirect('index.php', 5, _US_NOPERMISS);
        }
    }
}