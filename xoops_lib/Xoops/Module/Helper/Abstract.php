<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/**
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU public license
 * @author          trabis <lusopoemas@gmail.com>
 * @version         $Id: Abstract.php 10670 2013-01-04 22:59:42Z trabis $
 */

defined("XOOPS_ROOT_PATH") or die("XOOPS root path not defined");

abstract class Xoops_Module_Helper_Abstract
{
    /**
     * @var string dirname of the module
     */
    protected $_dirname = '';

    /**
     * @var null|XoopsModule
     */
    private $_module = null;

    /**
     * @var bool
     */
    private $_debug = false;

    public function init()
    {
    }

    /**
     * @param string $dirname dirname of the module
     */
    protected function setDirname($dirname)
    {
        $this->_dirname = strtolower($dirname);
    }

    /**
     * @param bool $debug
     */
    protected function setDebug($debug)
    {
        $this->_debug = (bool)$debug;
    }

    /**
     * @return Xoops_Module_Helper_Abstract
     */
    static function getInstance()
    {
        static $instance = false;
        $id = $className = get_called_class();
        if (strtolower($className) == 'xoops_module_helper_dummy') {
            $id = Xoops::getInstance()->registry()->get('module_helper_id');
        }
        if (!isset($instance[$id])) {
            /* @var $class Xoops_Module_Helper_Abstract */
            $class = new $className();
            $class->init();
            $instance[$id] = $class;
        }
        return $instance[$id];
    }

    /**
     * @return null|XoopsModule
     */
    public function getModule()
    {
        if ($this->_module == null) {
            $this->_initModule();
        }
        return $this->_module;
    }

    public function xoops()
    {
        return Xoops::getInstance();
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getConfig($name)
    {
        $name = strtolower($name);
        $result = $this->xoops()->getModuleConfig($name, $this->_dirname);
        $this->_addLog("Getting config '{$name}' : " . $result);
        return $result;
    }

    /**
     * @param string $name
     *
     * @return XoopsObjectHandler
     */
    public function getHandler($name)
    {
        $name = strtolower($name);
        $this->_addLog("Getting handler '{$name}'");
        return $this->xoops()->getModuleHandler($name, $this->_dirname);
    }

    public function disableCache()
    {
        $this->xoops()->appendConfig('module_cache', array($this->getModule()->getVar('mid') => 0), true);
        $this->_addLog("Disabling module cache");
    }

    /**
     * @return bool
     */
    public function isCurrentModule()
    {
        if ($this->xoops()->moduleDirname == $this->_dirname) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isUserAdmin()
    {
        if ($this->xoops()->isUser()) {
            return $this->xoops()->user->isAdmin($this->getModule()->getVar('mid'));
        }
        return false;
    }

    public function getUserGroups()
    {
        return $this->xoops()->isUser() ? $this->xoops()->user->getGroups() : XOOPS_GROUP_ANONYMOUS;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function url($url = '')
    {
        return $this->xoops()->url('modules/' . $this->_dirname . '/' . $url);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function path($path = '')
    {
        return $this->xoops()->path('modules/' . $this->_dirname . '/' . $path);
    }

    /**
     * Function to redirect a user to certain pages
     *
     * @param        $url
     * @param int    $time
     * @param string $message
     * @param bool   $addredirect
     * @param bool   $allowExternalLink
     *
     * @return void
     */
    public function redirect($url, $time = 3, $message = '', $addredirect = true, $allowExternalLink = false)
    {
        $this->xoops()->redirect($this->url($url), $time, $message, $addredirect, $allowExternalLink);
    }

    /**
     * @param string $language
     *
     * @return string
     */
    public function loadLanguage($language)
    {
        $this->xoops()->loadLanguage($language, $this->_dirname);
        $this->_addLog("Loading language '{$language}'");
    }

    /**
     * @param null|XoopsObject       $obj
     * @param string                 $name
     *
     * @return bool|XoopsForm
     */
    public function getForm($obj, $name)
    {
        $name = strtolower($name);
        $this->_addLog("Loading form '{$name}'");
        return $this->xoops()->getModuleForm($obj, $name, $this->_dirname);
    }

    /**
     * Initialize module
     */
    private function _initModule()
    {
        if ($this->isCurrentModule()) {
            $this->_module = $this->xoops()->module;
        } else {
            $this->_module = $this->xoops()->getModuleByDirname($this->_dirname);
        }
        if (!$this->_module instanceof XoopsModule) {
            $this->_module = $this->xoops()->getHandlerModule()->create();
        }
        $this->_addLog('Loading module');
    }

    /**
     * @param string $log
     */
    private function _addLog($log)
    {
        if ($this->_debug) {
            $this->xoops()->preload()->triggerEvent('core.module.addlog', array(
                $this->getModule()->getVar('name'),
                $log
            ));
        }
    }
}