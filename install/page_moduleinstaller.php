<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * @copyright   The XOOPS project http://www.xoops.org/
 * @license     http://www.fsf.org/copyleft/gpl.html GNU General Public License (GPL)
 * @package     installer
 * @since       2.3.0
 * @author      Haruki Setoyama  <haruki@planewave.org>
 * @author      Kazumi Ono <webmaster@myweb.ne.jp>
 * @author      Skalpa Keo <skalpa@xoops.org>
 * @author      Taiwen Jiang <phppp@users.sourceforge.net>
 * @author      DuGris (aka L. JEN) <dugris@frxoops.org>
 * @version     $Id: page_moduleinstaller.php 10561 2012-12-26 18:38:18Z trabis $
 */

$xoopsOption['checkadmin'] = true;
$xoopsOption['hascommon'] = true;

require_once dirname(__FILE__) . '/include/common.inc.php';

$xoops = Xoops::getInstance();

/* @var $wizard XoopsInstallWizard */
$wizard = $_SESSION['wizard'];

$xoops->loadLanguage('admin', 'system', $wizard->language);
$xoops->loadLanguage('global', '', $wizard->language);
$xoops->loadLanguage('admin/modulesadmin', 'system', $wizard->language);

$pageHasForm = true;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include_once XOOPS_ROOT_PATH . "/modules/system/class/module.php";
    include_once XOOPS_ROOT_PATH . "/modules/system/class/system.php";

    $system_module = new SystemModule();
    $system = System::getInstance();
    $system->loadLanguage('modulesadmin','system');

    $msgs = array();
    foreach ($_REQUEST['modules'] as $dirname => $installmod) {
        if ($installmod) {
            $msgs[] = $system_module->install($dirname);
        }
    }

    $pageHasForm = false;

    if (count($msgs) > 0) {
        $content = "<div class='x2-note successMsg'>" . INSTALLED_MODULES . "</div><ul class='log'>";
        foreach ($msgs as $msg) {
            $tempvar = $msg->modinfo['name'];
            $content .= "<dt>{$tempvar}</dt>";
        }
        $content .= "</ul>";
    } else {
        $content = "<div class='x2-note confirmMsg'>" . NO_INSTALLED_MODULES . "</div>";
    }

    //Set active modules in cache folder
    $xoops->setActiveModules();
} else {
    if (!$xoops->getConfig('language')) {
        $xoops->setConfig('language', $_COOKIE['xo_install_lang']);
    }

    include_once XOOPS_ROOT_PATH . "/modules/system/class/module.php";
    include_once XOOPS_ROOT_PATH . "/modules/system/class/system.php";

    $system_module = new SystemModule();
    $system = System::getInstance();
    $system->loadLanguage('modulesadmin','system');
    // Get installed modules
    $system_module = new SystemModule();

    $dirlist = $system_module->getModuleInstall();
    $toinstal = 0;

    $javascript = "";
    $content = "<ul class='log'><li style='background: none;'>";
    $content .= "<table class='module'>\n";
    /* @var $module XoopsModule */
    foreach ($dirlist as $module) {
        clearstatcache();
        $value = 0;
        $style = "";
        if (in_array($module->getInfo('dirname'), $wizard->configs['modules'])) {
            $value = 1;
            $style = " style='background-color:#E6EFC2;'";
        }
        $form = new XoopsThemeForm('', 'modules', 'index.php', 'post');
        $moduleYN = new XoopsFormRadioYN('', 'modules[' . $module->getInfo('dirname') . ']', $value, _YES, _NO);
        $moduleYN->setExtra("onclick='selectModule(\"" . $module->getInfo('dirname') . "\", this)'");
        $form->addElement($moduleYN);

        $content .= "<tr id='" . $module->getInfo('dirname') . "'" . $style . ">\n";
        $content .= "    <td class='img' ><img src='" . XOOPS_URL . "/modules/" . $module->getInfo('dirname') . "/" . $module->getInfo('image') . "' alt='" . $module->getInfo('name') . "'/></td>\n";
        $content .= "    <td>";
        $content .= "        " . $module->getInfo('name') . "&nbsp;" . number_format(round($module->getInfo('version'), 2), 2) . "&nbsp;(" . $module->getInfo('dirname') . ")";
        $content .= "        <br />" . $module->getInfo('description');
        $content .= "    </td>\n";
        $content .= "    <td class='yesno'>";
        $content .= $moduleYN->render();
        $content .= "    </td></tr>\n";
        $toinstal++;
    }
    $content .= "</table>";
    $content .= "</li></ul><script type='text/javascript'>" . $javascript . "</script>";
    if ($toinstal == 0) {
        $pageHasForm = false;
        $content = "<div class='x2-note confirmMsg'>" . NO_MODULES_FOUND . "</div>";
    }
}

$_SESSION['pageHasHelp'] = false;
$_SESSION['pageHasForm'] = $pageHasForm;
$_SESSION['content'] = $content;
include XOOPS_INSTALL_PATH . '/include/install_tpl.php';