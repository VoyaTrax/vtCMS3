<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * @copyright   The XOOPS project http://www.xoops.org/
 * @license     http://www.fsf.org/copyleft/gpl.html GNU General Public License (GPL)
 * @package     installer
 * @author      Andricq Nicolas (AKA MusS)
 * @version     $Id: page_extensioninstaller.php 10688 2013-01-06 21:46:52Z mageg $
 */

$xoopsOption['checkadmin'] = true;
$xoopsOption['hascommon'] = true;

require_once dirname(__FILE__) . '/include/common.inc.php';

$xoops = Xoops::getInstance();

/* @var $wizard XoopsInstallWizard */
$wizard = $_SESSION['wizard'];

$xoops->loadLanguage('admin', 'system', $wizard->language);
$xoops->loadLanguage('global', '', $wizard->language);
$xoops->loadLanguage('admin/extension', 'system', $wizard->language);

$pageHasForm = true;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include_once XOOPS_ROOT_PATH . "/modules/system/class/module.php";
    include_once XOOPS_ROOT_PATH . "/modules/system/class/system.php";

    $system = System::getInstance();
    $system->loadLanguage('modulesadmin','system');

    $system_module = new SystemModule();

    $msgs = array();
    foreach ($_REQUEST['modules'] as $dirname => $installmod) {
        if ($installmod) {
            $msgs[] = $system_module->install($dirname);
        }
    }

    $pageHasForm = false;

    if (count($msgs) > 0) {
        $content = "<div class='x2-note successMsg'>" . INSTALLED_EXTENSION . "</div><ul class='log'>";
        foreach ($msgs as $msg) {
            $tempvar = $msg->modinfo['name'];
            $content .= "<dt>{$tempvar}</dt>";
        }
        $content .= "</ul>";
    } else {
        $content = "<div class='x2-note confirmMsg'>" . NO_INSTALLED_EXTENSION . "</div>";
    }

    //Set active modules in cache folder
    $xoops->setActiveModules();
} else {
    if (!$xoops->getConfig('language')) {
        $xoops->setConfig('language', $_COOKIE['xo_install_lang']);
    }
    include_once XOOPS_ROOT_PATH . "/modules/system/class/module.php";
    include_once XOOPS_ROOT_PATH . "/modules/system/class/extension.php";
    include_once XOOPS_ROOT_PATH . "/modules/system/class/system.php";

    $system = System::getInstance();
    $system->loadLanguage('modulesadmin','system');

    // Get installed modules
    $system_module = new SystemExtension();

    $dirlist = $system_module->getExtensionInstall();
    $toinstal = 0;

    $javascript = "";
    $content = "<ul class='log'><li style='background: none;'>";
    $content .= "<table class='module'>\n";
    /* @var $ext XoopsModule */
    foreach ($dirlist as $ext) {
        clearstatcache();
        $value = 0;
        $style = "";

        if (in_array($ext->getInfo('dirname'), $wizard->configs['ext'])) {
            $value = 1;
            $style = " style='background-color:#E6EFC2;'";
        }

        $form = new XoopsThemeForm('', 'modules', 'index.php', 'post');
        $moduleYN = new XoopsFormRadioYN('', 'modules[' . $ext->getInfo('dirname') . ']', $value, _YES, _NO);
        $moduleYN->setExtra("onclick='selectModule(\"" . $ext->getInfo('dirname') . "\", this)'");
        $form->addElement($moduleYN);

        $content .= "<tr id='" . $ext->getInfo('dirname') . "'" . $style . ">\n";
        $content .= "    <td class='img' ><img src='" . XOOPS_URL . "/modules/" . $ext->getInfo('dirname') . "/" . $ext->getInfo('image') . "' alt='" . $ext->getInfo('name') . "'/></td>\n";
        $content .= "    <td>";
        $content .= "        " . $ext->getInfo('name') . "&nbsp;" . number_format(round($ext->getInfo('version'), 2), 2) . "&nbsp;(" . $ext->getInfo('dirname') . ")";
        $content .= "        <br />" . $ext->getInfo('description');
        $content .= "    </td>\n";
        $content .= "    <td class='yesno'>";
        $content .= $moduleYN->render();
        $content .= "    </td></tr>\n";
        $toinstal++;
    }
    $content .= "</table>";
    $content .= "</li></ul><script type='text/javascript'>" . $javascript . "</script>";
    if ($toinstal == 0) {
        $pageHasForm = false;
        $content = "<div class='x2-note confirmMsg'>" . NO_EXTENSION_FOUND . "</div>";
    }
}
$_SESSION['pageHasHelp'] = false;
$_SESSION['pageHasForm'] = $pageHasForm;
$_SESSION['content'] = $content;
include XOOPS_INSTALL_PATH . '/include/install_tpl.php';