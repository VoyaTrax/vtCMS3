<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU public license
 * @author          trabis <lusopoemas@gmail.com>
 * @version         $Id: install.php 10530 2012-12-23 20:25:39Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

function xoops_module_install_comments(&$module)
{
    $xoops = Xoops::getInstance();
    $sql = "SHOW COLUMNS FROM " . $xoops->db()->prefix("xoopscomments");
    $result = $xoops->db()->queryF($sql);
    if (($rows = $xoops->db()->getRowsNum($result)) == 20) {
        $sql = "SELECT * FROM " . $xoops->db()->prefix("xoopscomments");
        $result = $xoops->db()->query($sql);
        while ($myrow = $xoops->db()->fetchArray($result)) {
            $sql = "INSERT INTO `" . $xoops->db()->prefix("comments") . "` (`id`, `pid`, `rootid`, `modid`, `itemid`, `icon`, `created`, `modified`, `uid`, `ip`, `title`, `text`, `sig`, `status`, `exparams`, `dohtml`, `domsiley`, `doxcode`, `doimage`, `dobr`) VALUES (" . $myrow['com_id'] . ", " . $myrow['com_pid'] . ", " . $myrow['com_rootid'] . ", " . $myrow['com_modid'] . ", " . $myrow['com_itemid'] . ", " . $myrow['com_icon'] . ", " . $myrow['com_created'] . ", " . $myrow['com_modified'] . ", " . $myrow['com_uid'] . ", " . $myrow['com_ip'] . ", " . $myrow['com_title'] . ", " . $myrow['com_text'] . ", ". $myrow['com_sig'] . ", " . $myrow['com_status'] . ", " . $myrow['com_exparams'] . ", " . $myrow['dohtml'] . ", " . $myrow['dosmiley'] .", " . $myrow['doxcode'] . ", " . $myrow['doimage'] . ", " . $myrow['dobr'] . ")";
            $xoops->db()->queryF($sql);
        }
        //Don't drop old table for now
        //$sql = "DROP TABLE " . $xoops->db()->prefix("xoopscomments");
        //$xoops->db()->queryF($sql);
    }
    return true;
}