<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * avatars module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         avatar
 * @since           2.6.0
 * @author          Mage Grégory (AKA Mage)
 * @version         $Id: admin.php 10173 2012-09-12 18:46:05Z mageg $
 */

define("_AM_AVATARS_ADD", "Add a new avatar");
define("_AM_AVATARS_DELETE","Delete avatar");
define("_AM_AVATARS_DISPLAY","Display this avatar");
define("_AM_AVATARS_EDIT","Edit avatar");
define("_AM_AVATARS_FILE","Avatar file");
define("_AM_AVATARS_IMAGE_PATH","Files exist in: %s");
define("_AM_AVATARS_LIST", "List of avatars");
define("_AM_AVATARS_NAME","Name");
define("_AM_AVATARS_NBTOTAL_C","There are %s custom avatars in our database");
define("_AM_AVATARS_NBTOTAL_S","There are %s system avatars in our database");
define("_AM_AVATARS_NBDISPLAY_C","There are %s visible custom avatars");
define("_AM_AVATARS_NBDISPLAY_S","There are %s visible system avatars");
define("_AM_AVATARS_NBNOTDISPLAY_C","There are %s custom avatars not visible");
define("_AM_AVATARS_NBNOTDISPLAY_S","There are %s system avatars not visible");
define("_AM_AVATARS_OFF","Display in the form ");
define("_AM_AVATARS_ON","Do not display in the form");
define("_AM_AVATARS_SAVE","Avatar saved");
define("_AM_AVATARS_SUREDEL","Are you sure to delete this avatar?");
define("_AM_AVATARS_WEIGHT","Display order in avatar manager:");
define("_AM_AVATARS_UPLOADS","Upload");
define("_AM_AVATARS_USERS","Users using this avatar");
define("_AM_AVATARS_ERROR_WEIGHT","You need a positive integer");

// Tips
define("_AM_AVATARS_SYSTEM_TIPS","<ul><li>Add, update or delete system avatars</li></ul>");
define("_AM_AVATARS_CUSTOM_TIPS","<ul><li>Add, update or delete custom avatars</li></ul>");
define("_AM_AVATARS_TIPS_FORM1","<ul><li>Authorized mime types: %s</li>");
define("_AM_AVATARS_TIPS_FORM2","<li>Max uploaded files size (KB): %s</li>");
define("_AM_AVATARS_TIPS_FORM3","<li>Max Pixels: %s x %s (width x height)</li></ul>");