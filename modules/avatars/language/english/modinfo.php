<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Avatars extension
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         avatar
 * @since           2.6.0
 * @author          Mage Grégory (AKA Mage)
 * @version         $Id: modinfo.php 9798 2012-07-07 17:50:40Z beckmi $
 */
// Info module
define("_MI_AVATARS_NAME", "Avatars");
define("_MI_AVATARS_DESC", "Extension for managing avatars");
// Menu
define("_MI_AVATARS_INDEX", "Home");
define("_MI_AVATARS_SYSTEM", "System avatars");
define("_MI_AVATARS_CUSTOM", "Custom avatars");
define("_MI_AVATARS_ABOUT", "About");
// Preferences
define("_MI_AVATARS_PREFERENCE_ALLOWUPLOAD", "Allow custom avatar upload?");
define("_MI_AVATARS_PREFERENCE_POSTSREQUIRED", "Minimum posts required");
define("_MI_AVATARS_PREFERENCE_POSTSREQUIREDDSC", "Enter the minimum number of posts required to upload a custom avatar");
define("_MI_AVATARS_PREFERENCE_IMAGEWIDTH", "Avatar image max width (pixel)");
define("_MI_AVATARS_PREFERENCE_IMAGEHEIGHT", "Avatar image max height (pixel)");
define("_MI_AVATARS_PREFERENCE_IMAGEFILESIZE", "Avatar image max filesize (byte)");
define("_MI_AVATARS_PREFERENCE_PAGER", "Number of avatars to display per page");