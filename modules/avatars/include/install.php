<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * avatars module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         avatar
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: install.php 10714 2013-01-08 21:11:35Z mageg $
 */
function xoops_module_install_avatars(&$module)
{
    // delete avatar_allow_upload, avatar_width, avatar_height, avatar_maxsize and avatar_minposts
    $xoops = Xoops::getInstance();
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'avatar_allow_upload'";
    $xoops->db()->queryF($sql);
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'avatar_width'";
    $xoops->db()->queryF($sql);
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'avatar_height'";
    $xoops->db()->queryF($sql);
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'avatar_maxsize'";
    $xoops->db()->queryF($sql);
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'avatar_minposts'";
    $xoops->db()->queryF($sql);

    $dbManager = new XoopsDatabaseManager();
    $map = array(
        'avatar_id'       => 'avatar_id',
        'avatar_file'     => 'avatar_file',
        'avatar_name'     => 'avatar_name',
        'avatar_mimetype' => 'avatar_mimetype',
        'avatar_created'  => 'avatar_created',
        'avatar_display'  => 'avatar_display',
        'avatar_weight'   => 'avatar_weight',
        'avatar_type'     => 'avatar_type',
    );
    $dbManager->copyFields($map, 'avatar', 'avatars_avatar', false);

    $map = array(
        'avatar_id' => 'avatar_id',
        'user_id'   => 'user_id',
    );
    $dbManager->copyFields($map, 'avatar_user_link', 'avatars_user_link', false);
    return true;
}