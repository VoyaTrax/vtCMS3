<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * avatars module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         avatar
 * @since           2.6.0
 * @author          Mage Grégory (AKA Mage)
 * @version         $Id: avatar_system.php 10622 2013-01-01 20:27:01Z trabis $
 */
include dirname(__FILE__) . '/header.php';
// Get main instance
$xoops = Xoops::getInstance();
$helper = Avatars::getInstance();
$request = $xoops->request();

// Parameters
$nb_avatars = $helper->getConfig('avatars_pager');
$mimetypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png', 'image/png');
$upload_size = $helper->getConfig('avatars_imagefilesize');
$width = $helper->getConfig('avatars_imagewidth');
$height = $helper->getConfig('avatars_imageheight');
// Get Action type
$op = $request->asStr('op', 'list');
// Get avatar handler
$avatar_Handler = $helper->getHandlerAvatar();
// Call Header
$xoops->header('avatars_system.html');

$admin_page = new XoopsModuleAdmin();
$admin_page->renderNavigation('avatars_system.php');

switch ($op) {

    case 'list':
    default:
        // Add Scripts
        $xoops->theme()->addScript('media/xoops/xoops.js');
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/avatars/css/admin.css');

        $admin_page->addTips(_AM_AVATARS_SYSTEM_TIPS);
        $admin_page->addItemButton(_AM_AVATARS_ADD, 'avatar_system.php?op=new', 'add');
        $admin_page->renderTips();
        $admin_page->renderButton();
        // Get start pager
        $start = $request->asInt('start', 0);
        // Filter avatars
        $criteria = new Criteria('avatar_type', 'S');
        $avatar_count = $avatar_Handler->getCount($criteria);
        $xoops->tpl()->assign('avatar_count', $avatar_count);
        // Get avatar list
        $criteria->setStart($start);
        $criteria->setLimit($nb_avatars);
        $criteria->setSort("avatar_weight");
        $criteria->setOrder("ASC");
        $avatars_arr = $avatar_Handler->getObjects($criteria, true);
        // Construct avatars array
        $avatar_list = array();
        $i = 0;
        foreach (array_keys($avatars_arr) as $i) {
            $avatar_list[$i] = $avatars_arr[$i]->getValues();
            $avatar_list[$i]['count'] = count($avatar_Handler->getUser($avatars_arr[$i]));
        }
        $xoops->tpl()->assign('avatars_list', $avatar_list);
        // Display Page Navigation
        if ($avatar_count > $nb_avatars) {
            $nav = new XoopsPageNav($avatar_count, $nb_avatars, $start, 'start', 'op=list');
            $xoops->tpl()->assign('nav_menu', $nav->renderNav(4));
        }
        break;

    // New
    case "new":
        $admin_page->addTips(sprintf(_AM_AVATARS_TIPS_FORM1, implode(', ', $mimetypes)) . sprintf(_AM_AVATARS_TIPS_FORM2, $upload_size / 1000) . sprintf(_AM_AVATARS_TIPS_FORM3, $width, $height));
        $admin_page->addItemButton(_AM_AVATARS_LIST, 'avatar_system.php', 'list');
        $admin_page->renderTips();
        $admin_page->renderButton();
        // Create form
        $obj = $avatar_Handler->create();
        $form = $xoops->getModuleForm($obj, 'avatar');
        // Assign form
        $xoops->tpl()->assign('form', $form->render());
        break;

    // Edit
    case "edit":
        $admin_page->addTips(sprintf(_AM_AVATARS_TIPS_FORM1, implode(', ', $mimetypes)) . sprintf(_AM_AVATARS_TIPS_FORM2, $upload_size / 1000) . sprintf(_AM_AVATARS_TIPS_FORM3, $width, $height));
        $admin_page->addItemButton(_AM_AVATARS_ADD, 'avatar_system.php?op=new', 'add');
        $admin_page->addItemButton(_AM_AVATARS_LIST, 'avatar_system.php', 'list');
        $admin_page->renderTips();
        $admin_page->renderButton();
        // Create form
        $obj = $avatar_Handler->get($request->asInt('avatar_id', 0));
        $form = $xoops->getModuleForm($obj, 'avatar');
        // Assign form
        $xoops->tpl()->assign('form', $form->render());
        break;

    // Save
    case "save":
        // Check security
        if (!$xoops->security()->check()) {
            $xoops->redirect('avatar_system.php', 3, implode('<br />', $xoops->security()->getErrors()));
        }
        $uploader_avatars_img = new XoopsMediaUploader(XOOPS_UPLOAD_PATH . '/avatars', $mimetypes, $upload_size, $width, $height);
        // Get avatar id
        $avatar_id = $request->asInt('avatar_id', 0);
        if ($avatar_id > 0) {
            $obj = $avatar_Handler->get($avatar_id);
        } else {
            $obj = $avatar_Handler->create();
        }
        $obj->setVars($_POST);
        $obj->setVar('avatar_type', 's');
        if ($uploader_avatars_img->fetchMedia('avatar_file')) {
            $uploader_avatars_img->setPrefix('savt');
            $uploader_avatars_img->fetchMedia('avatar_file');
            if (!$uploader_avatars_img->upload()) {
                $errors = $uploader_avatars_img->getErrors();
                $xoops->redirect("javascript:history.go(-1)", 3, $errors);
            } else {
                $obj->setVar('avatar_mimetype', $uploader_avatars_img->getMediaType());
                $obj->setVar('avatar_file', 'avatars/' . $uploader_avatars_img->getSavedFileName());
            }
        } else {
            $file = $request->asStr('avatar_file', 'blank.gif');
            $obj->setVar('avatar_file', 'avatars/' . $file);
        }
        if ($avatar_Handler->insert($obj)) {
            $xoops->redirect('avatar_system.php', 2, _AM_AVATARS_SAVE);
        }
        $xoops->error($obj->getHtmlErrors());
        $form = $xoops->getModuleForm($obj, 'avatar');
        $xoops->tpl()->assign('form', $form->render());
        break;

    //Delete
    case "delete":
        $admin_page->addItemButton(_AM_AVATARS_ADD, 'avatar_system.php?op=new', 'add');
        $admin_page->addItemButton(_AM_AVATARS_LIST, 'avatar_system.php', 'list');
        $admin_page->renderButton();
        $avatar_id = $request->asInt('avatar_id', 0);
        $obj = $avatar_Handler->get($avatar_id);
        if (isset($_POST["ok"]) && $_POST["ok"] == 1) {
            if (!$xoops->security()->check()) {
                $xoops->redirect("avatar_system.php", 3, implode(",", $xoops->security()->getErrors()));
            }
            if ($avatar_Handler->delete($obj)) {
                // Delete file
                $file = $obj->getVar('avatar_file');
                if (is_file(XOOPS_UPLOAD_PATH . '/' . $file)) {
                    chmod(XOOPS_UPLOAD_PATH . '/' . $file, 0777);
                    unlink(XOOPS_UPLOAD_PATH . '/' . $file);
                }
                // Update member profil
                $xoops->db()->query("UPDATE " . $xoops->db()->prefix('users') . " SET user_avatar='blank.gif' WHERE user_avatar='" . $file . "'");
                $xoops->redirect("avatar_system.php", 2, _AM_AVATARS_SAVE);
            } else {
                $xoops->error($obj->getHtmlErrors());
            }
        } else {
            if ($avatar_id > 0) {
                // Define Stylesheet
                $xoops->theme()->addStylesheet('modules/system/css/admin.css');
                $msg = '<div class="spacer"><img src="' . XOOPS_UPLOAD_URL . '/' . $obj->getVar('avatar_file', 's') . '" alt="" /></div><div class="txtcenter bold">' . $obj->getVar('avatar_name', 's') . '</div>' . _AM_AVATARS_SUREDEL;
                // Display message
                $xoops->confirm(array('ok' => 1, 'op' => 'delete', 'avatar_id' => $avatar_id), 'avatar_system.php', $msg);
            } else {
                $xoops->redirect('avatar_system.php', 1, _AM_SYSTEM_DBERROR);
            }
        }
        break;

    case "update_display":
        $avatar_id = $request->asInt('avatar_id', 0);
        if ($avatar_id > 0) {
            $obj = $avatar_Handler->get($avatar_id);
            $old = $obj->getVar('avatar_display');
            $obj->setVar('avatar_display', !$old);
            if ($avatar_Handler->insert($obj)) {
                exit;
            }
            echo $obj->getHtmlErrors();
        }
        break;
}
$xoops->footer();