<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Center Form Class
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         Protector
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: center.php 9560 2012-05-20 12:36:17Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class ProtectorCenterForm extends XoopsThemeForm
{
    /**
     * @param null $obj
     */
    public function __construct($obj = null)
    {
    }

    /**
     * Maintenance Form
     * @return void
     */
    public function getPrefIp($bad_ips4disp, $group1_ips4disp)
    {
        $db = XoopsDatabaseFactory::getDatabaseConnection();
        $protector = Protector::getInstance($db->conn);
        require_once dirname(dirname(__FILE__)) . '/gtickets.php';
        
        parent::__construct('', "form_prefip", "center.php", 'post', true);
        
        $bad_ips = new XoopsFormTextArea(_AM_TH_BADIPS, 'bad_ips', $bad_ips4disp, 3, 90);
        $bad_ips->setDescription('<br />' . htmlspecialchars($protector->get_filepath4badips()));
        $bad_ips->setClass('span3');
        $this->addElement($bad_ips);
        
        $group1_ips = new XoopsFormTextArea(_AM_TH_GROUP1IPS, 'group1_ips', $group1_ips4disp, 3, 90);
        $group1_ips->setDescription('<br />' . htmlspecialchars($protector->get_filepath4group1ips()));
        $group1_ips->setClass('span3');
        $this->addElement($group1_ips);
        $formTicket = new xoopsGTicket;
        $this->addElement(new XoopsFormHidden("action", "update_ips"));
        $ticket = $formTicket->getTicketXoopsForm(__LINE__, 1800, 'protector_admin');
        $this->addElement($ticket);
        $this->addElement(new XoopsFormButton('', "submit_prefip", _SUBMIT, "submit"));
    }
}
