<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Private Message
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         pm
 * @since           2.3.0
 * @author          Taiwen Jiang <phppp@users.sourceforge.net>
 * @version         $Id: update.php 10328 2012-12-07 00:56:07Z trabis $
 */

/**
 * @param XoopsModule $module
 * @param string|null $oldversion
 * @return bool|void
 */
function xoops_module_update_pm(&$module, $oldversion = null)
{
    $xoops = Xoops::getInstance();
    if ($oldversion <= 100) {
        // Check pm table version
        $sql = "SHOW COLUMNS FROM " . $xoops->db()->prefix("priv_msgs");
        if (!$result = $xoops->db()->queryF($sql)) {
            return false;
        }
        // Migrate from existent pm module
        if (($rows = $xoops->db()->getRowsNum($result)) == 12) {
            return true;
        } elseif ($rows == 8) {
            return $xoops->db()->queryFromFile(XOOPS_ROOT_PATH . "/modules/" . $module->getVar('dirname', 'n') . "/sql/mysql.upgrade.sql");
        } else {
            return false;
        }
    }
    return true;
}