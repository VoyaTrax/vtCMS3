<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Blocks Form Class
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author          Andricq Nicolas (AKA MusS)
 * @package         system
 * @subpackage      blocksadmin
 * @version         $Id: block.php 10173 2012-09-12 18:46:05Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class SystemBlockForm extends XoopsThemeForm
{
    /**
     * @var null|XoopsObject $_obj
     */
    private $_obj=null;

    /**
     * @param XoopsBlock|XoopsObject $obj
     */
    public function __construct(XoopsBlock &$obj)
    {
        $this->_obj = $obj;
    }

    /**
     * @param string $mode
     * @return void
     */
    public function getForm($mode='edit'){
        $xoops = Xoops::getInstance();
        $xoops->loadLanguage('blocks', 'system');
        if ($this->_obj->isNew()) {
            $title = _AM_SYSTEM_BLOCKS_ADDBLOCK;
            $modules = array(-1);
            $groups = array(XOOPS_GROUP_USERS, XOOPS_GROUP_ANONYMOUS, XOOPS_GROUP_ADMIN);
            $this->_obj->setVar('block_type', 'C');
            $this->_obj->setVar('visible', 1);
            $op = 'save';
        } else {
            $title = '';
            $modules = array();
            // Search modules
            $blockmodulelink_handler = $xoops->getHandlerBlockmodulelink();
            $criteria = new CriteriaCompo(new Criteria('block_id', $this->_obj->getVar('bid')));
            $blockmodulelink = $blockmodulelink_handler->getObjects($criteria);
            /* @var $link XoopsBlockmodulelink */
            foreach ($blockmodulelink as $link) {
                $modules[] = $link->getVar('module_id');
            }
            // Search perms
            $groupperm_handler = $xoops->getHandlerGroupperm();
            $groups = $groupperm_handler->getGroupIds('block_read', $this->_obj->getVar('bid'));
            switch ($mode) {
                case 'edit':
                    $title = _AM_SYSTEM_BLOCKS_EDITBLOCK;
                    break;
                case 'clone':
                    $title = _AM_SYSTEM_BLOCKS_CLONEBLOCK;
                    $this->_obj->setVar('bid', 0);
                    if ($this->_obj->isCustom()) {
                        $this->_obj->setVar('block_type', 'C');
                    } else {
                        $this->_obj->setVar('block_type', 'D');
                    }
                    break;
            }
            $op = 'save';
        }
        parent::__construct($title, 'blockform', 'admin.php', 'post', true);
        if (!$this->_obj->isNew()) {
            $this->addElement(new XoopsFormLabel(_AM_SYSTEM_BLOCKS_NAME, $this->_obj->getVar('name')));
        }
        // Side position
        $side_select = new XoopsFormSelect(_AM_SYSTEM_BLOCKS_TYPE, 'side', $this->_obj->getVar('side'));
        $side_select->addOptionArray(array(
                0 => _AM_SYSTEM_BLOCKS_SBLEFT, 1 => _AM_SYSTEM_BLOCKS_SBRIGHT, 3 => _AM_SYSTEM_BLOCKS_CBLEFT,
                4 => _AM_SYSTEM_BLOCKS_CBRIGHT, 5 => _AM_SYSTEM_BLOCKS_CBCENTER, 7 => _AM_SYSTEM_BLOCKS_CBBOTTOMLEFT,
                8 => _AM_SYSTEM_BLOCKS_CBBOTTOMRIGHT, 9 => _AM_SYSTEM_BLOCKS_CBBOTTOM
            ));
        $this->addElement($side_select);
        // Order
        $weight = new XoopsFormText(_AM_SYSTEM_BLOCKS_WEIGHT, 'weight', 1, 5, $this->_obj->getVar('weight'), '');
        $weight->setPattern('^\d+$', _AM_SYSTEM_ERROR_WEIGHT);
        $this->addElement($weight, true);
        // Display
        $this->addElement(new XoopsFormRadioYN(_AM_SYSTEM_BLOCKS_VISIBLE, 'visible', $this->_obj->getVar('visible')));
        // Visible In
        $mod_select = new XoopsFormSelect(_AM_SYSTEM_BLOCKS_VISIBLEIN, 'modules', $modules, 5, true);
        $criteria = new CriteriaCompo(new Criteria('hasmain', 1));
        $criteria->add(new Criteria('isactive', 1));
        $module_list = $xoops->getHandlerModule()->getNameList($criteria);
        $module_list[-1] = _AM_SYSTEM_BLOCKS_TOPPAGE;
        $module_list[0] = _AM_SYSTEM_BLOCKS_ALLPAGES;
        ksort($module_list);
        $mod_select->addOptionArray($module_list);
        $this->addElement($mod_select);
        // Title
        $this->addElement(new XoopsFormText(_AM_SYSTEM_BLOCKS_TITLE, 'title', 5, 255, $this->_obj->getVar('title')), false);
        if ($this->_obj->isNew() || $this->_obj->isCustom()) {
            $editor_configs = array();
            $editor_configs["name"] = "content_block";
            $editor_configs["value"] = $this->_obj->getVar('content', 'e');
            $editor_configs["rows"] = 15;
            $editor_configs["cols"] = 6;
            $editor_configs["editor"] = $xoops->getModuleConfig('blocks_editor', 'system');
            $this->addElement(new XoopsFormEditor(_AM_SYSTEM_BLOCKS_CONTENT, "content_block", $editor_configs), true);
            if (in_array($editor_configs["editor"], array('dhtmltextarea', 'textarea'))) {
                $ctype_select = new XoopsFormSelect(_AM_SYSTEM_BLOCKS_CTYPE, 'c_type', $this->_obj->getVar('c_type'));
                $ctype_select->addOptionArray(array(
                        'H' => _AM_SYSTEM_BLOCKS_HTML, 'P' => _AM_SYSTEM_BLOCKS_PHP, 'S' => _AM_SYSTEM_BLOCKS_AFWSMILE,
                        'T' => _AM_SYSTEM_BLOCKS_AFNOSMILE
                    ));
                $this->addElement($ctype_select);
            } else {
                $this->addElement(new XoopsFormHidden('c_type', 'H'));
            }
        } else {
            if ($this->_obj->getVar('template') != '') {
                $tplfile_handler = $xoops->getHandlerTplfile();
                $btemplate = $tplfile_handler->find($xoops->getConfig('template_set'), 'block', $this->_obj->getVar('bid'));
                if (count($btemplate) > 0) {
                    $this->addElement(new XoopsFormLabel(_AM_SYSTEM_BLOCKS_CONTENT, '<a href="' . XOOPS_URL . '/modules/system/admin.php?fct=tplsets&amp;op=edittpl&amp;id=' . $btemplate[0]->getVar('tpl_id') . '">' . _AM_SYSTEM_BLOCKS_EDITTPL . '</a>'));
                } else {
                    $btemplate2 = $tplfile_handler->find('default', 'block', $this->_obj->getVar('bid'));
                    if (count($btemplate2) > 0) {
                        $this->addElement(new XoopsFormLabel(_AM_SYSTEM_BLOCKS_CONTENT, '<a href="' . XOOPS_URL . '/modules/system/admin.php?fct=tplsets&amp;op=edittpl&amp;id=' . $btemplate2[0]->getVar('tpl_id') . '" rel="external">' . _AM_SYSTEM_BLOCKS_EDITTPL . '</a>'));
                    }
                }
            }
            if ($this->_obj->getOptions() != false) {
                $this->addElement(new XoopsFormLabel(_AM_SYSTEM_BLOCKS_OPTIONS, $this->_obj->getOptions()));
            } else {
                $this->addElement(new XoopsFormHidden('options', $this->_obj->getVar('options')));
            }
            $this->addElement(new XoopsFormHidden('c_type', 'H'));
        }
        $cache_select = new XoopsFormSelect(_AM_SYSTEM_BLOCKS_BCACHETIME, 'bcachetime', $this->_obj->getVar('bcachetime'));
        $cache_select->addOptionArray(array(
                '0' => _NOCACHE, '30' => sprintf(_SECONDS, 30), '60' => _MINUTE, '300' => sprintf(_MINUTES, 5),
                '1800' => sprintf(_MINUTES, 30), '3600' => _HOUR, '18000' => sprintf(_HOURS, 5), '86400' => _DAY,
                '259200' => sprintf(_DAYS, 3), '604800' => _WEEK, '2592000' => _MONTH
            ));
        $this->addElement($cache_select);
        // Groups
        $this->addElement(new XoopsFormSelectGroup(_AM_SYSTEM_BLOCKS_GROUP, 'groups', true, $groups, 5, true));

        $this->addElement(new XoopsFormHidden('block_type', $this->_obj->getVar('block_type')));
        $this->addElement(new XoopsFormHidden('mid', $this->_obj->getVar('mid')));
        $this->addElement(new XoopsFormHidden('func_num', $this->_obj->getVar('func_num')));
        $this->addElement(new XoopsFormHidden('func_file', $this->_obj->getVar('func_file')));
        $this->addElement(new XoopsFormHidden('show_func', $this->_obj->getVar('show_func')));
        $this->addElement(new XoopsFormHidden('edit_func', $this->_obj->getVar('edit_func')));
        $this->addElement(new XoopsFormHidden('template', $this->_obj->getVar('template')));
        $this->addElement(new XoopsFormHidden('dirname', $this->_obj->getVar('dirname')));
        $this->addElement(new XoopsFormHidden('name', $this->_obj->getVar('name')));
        $this->addElement(new XoopsFormHidden('bid', $this->_obj->getVar('bid')));
        $this->addElement(new XoopsFormHidden('op', $op));
        $this->addElement(new XoopsFormHidden('fct', 'blocksadmin'));
        $button_tray = new XoopsFormElementTray('', '&nbsp;');
        if ($this->_obj->isNew() || $this->_obj->isCustom()) {
            $preview = new XoopsFormButton('', 'previewblock', _PREVIEW, 'preview');
            $preview->setExtra("onclick=\"blocks_preview();\"");
            $button_tray->addElement($preview);
        }
        $button_tray->addElement(new XoopsFormButton('', 'submitblock', _SUBMIT, 'submit'));
        $this->addElement($button_tray);
    }
}
