<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 *
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author          Andricq Nicolas (AKA MusS)
 * @package
 * @version         $Id: user.php 10630 2013-01-02 08:46:20Z dugris $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class SystemUserForm extends XoopsThemeForm
{
    /**
     * @param XoopsUser|XoopsObject $obj
     */
    public function __construct(XoopsUser &$obj)
    {
        $xoops = Xoops::getInstance();
        $system = System::getInstance();

        //RMV-NOTIFY
        $xoops->loadLanguage('notification');
        include_once $xoops->path('include/notification_constants.php');

        if ($obj->isNew()) {
            //Add user
            $uid_value = "";
            $uname_value = "";
            $name_value = "";
            $email_value = "";
            $email_cbox_value = 0;
            $url_value = "";
            $timezone_value = $xoops->getConfig('default_TZ');
            $icq_value = "";
            $aim_value = "";
            $yim_value = "";
            $msnm_value = "";
            $location_value = "";
            $occ_value = "";
            $interest_value = "";
            $sig_value = "";
            $sig_cbox_value = 0;
            $umode_value = $xoops->getConfig('com_mode');
            $uorder_value = $xoops->getConfig('com_order');
            // RMV-NOTIFY
            $notify_method_value = XOOPS_NOTIFICATION_METHOD_PM;
            $notify_mode_value = XOOPS_NOTIFICATION_MODE_SENDALWAYS;
            $bio_value = "";
            $rank_value = 0;
            $mailok_value = 0;
            $form_title = _AM_SYSTEM_USERS_ADDUSER;
            $form_isedit = false;
            $groups = array(XOOPS_GROUP_USERS);
        } else {
            //Edit user
            $uid_value = $obj->getVar("uid", "E");
            $uname_value = $obj->getVar("uname", "E");
            $name_value = $obj->getVar("name", "E");
            $email_value = $obj->getVar("email", "E");
            $email_cbox_value = $obj->getVar("user_viewemail") ? 1 : 0;
            $url_value = $obj->getVar("url", "E");
            $timezone_value = $obj->getVar("timezone_offset");
            $icq_value = $obj->getVar("user_icq", "E");
            $aim_value = $obj->getVar("user_aim", "E");
            $yim_value = $obj->getVar("user_yim", "E");
            $msnm_value = $obj->getVar("user_msnm", "E");
            $location_value = $obj->getVar("user_from", "E");
            $occ_value = $obj->getVar("user_occ", "E");
            $interest_value = $obj->getVar("user_intrest", "E");
            $sig_value = $obj->getVar("user_sig", "E");
            $sig_cbox_value = ($obj->getVar("attachsig") == 1) ? 1 : 0;
            $umode_value = $obj->getVar("umode");
            $uorder_value = $obj->getVar("uorder");
            // RMV-NOTIFY
            $notify_method_value = $obj->getVar("notify_method");
            $notify_mode_value = $obj->getVar("notify_mode");
            $bio_value = $obj->getVar("bio", "E");
            $rank_value = $obj->rank(false);
            $mailok_value = $obj->getVar('user_mailok', 'E');
            $form_title = _AM_SYSTEM_USERS_UPDATEUSER . ": " . $obj->getVar("uname");
            $form_isedit = true;
            $groups = array_values($obj->getGroups());
        }

        //Affichage du formulaire
        parent::__construct($form_title, "form_user", "admin.php", "post", true);

        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_NICKNAME, "username", 4, 25, $uname_value), true);
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_NAME, "name", 5, 60, $name_value));
        $email_tray = new XoopsFormElementTray(_AM_SYSTEM_USERS_EMAIL, "<br />");
        $email_text = new XoopsFormText("", "email", 5, 60, $email_value);
        $email_tray->addElement($email_text, true);
        $email_cbox = new XoopsFormCheckBox("", "user_viewemail", $email_cbox_value);
        $email_cbox->addOption(1, _AM_SYSTEM_USERS_AOUTVTEAD);
        $email_tray->addElement($email_cbox);
        $this->addElement($email_tray, true);
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_URL, "url", 5, 100, $url_value));
        $this->addElement(new XoopsFormSelectTimezone(_AM_SYSTEM_USERS_TIMEZONE, "timezone_offset", $timezone_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_ICQ, "user_icq", 3, 15, $icq_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_AIM, "user_aim", 3, 18, $aim_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_YIM, "user_yim", 3, 25, $yim_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_MSNM, "user_msnm", 3, 100, $msnm_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_LOCATION, "user_from", 5, 100, $location_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_OCCUPATION, "user_occ", 5, 100, $occ_value));
        $this->addElement(new XoopsFormText(_AM_SYSTEM_USERS_INTEREST, "user_intrest", 5, 150, $interest_value));
        $sig_tray = new XoopsFormElementTray(_AM_SYSTEM_USERS_SIGNATURE, "<br />");
        $sig_tarea = new XoopsFormTextArea("", "user_sig", $sig_value, 5, 5);
        $sig_tray->addElement($sig_tarea);
        $sig_cbox = new XoopsFormCheckBox("", "attachsig", $sig_cbox_value);
        $sig_cbox->addOption(1, _AM_SYSTEM_USERS_SHOWSIG);
        $sig_tray->addElement($sig_cbox);
        $this->addElement($sig_tray);
        $umode_select = new XoopsFormSelect(_AM_SYSTEM_USERS_CDISPLAYMODE, "umode", $umode_value);
        $umode_select->addOptionArray(array("nest" => _NESTED, "flat" => _FLAT, "thread" => _THREADED));
        $this->addElement($umode_select);
        $uorder_select = new XoopsFormSelect(_AM_SYSTEM_USERS_CSORTORDER, "uorder", $uorder_value);
        $uorder_select->addOptionArray(array("0" => _OLDESTFIRST, "1" => _NEWESTFIRST));
        $this->addElement($uorder_select);
        // RMV-NOTIFY
        $notify_method_select = new XoopsFormSelect(_NOT_NOTIFYMETHOD, 'notify_method', $notify_method_value);
        $notify_method_select->addOptionArray(array(
                                                   XOOPS_NOTIFICATION_METHOD_DISABLE => _NOT_METHOD_DISABLE, XOOPS_NOTIFICATION_METHOD_PM => _NOT_METHOD_PM,
                                                   XOOPS_NOTIFICATION_METHOD_EMAIL => _NOT_METHOD_EMAIL
                                              ));
        $this->addElement($notify_method_select);
        $notify_mode_select = new XoopsFormSelect(_NOT_NOTIFYMODE, 'notify_mode', $notify_mode_value);
        $notify_mode_select->addOptionArray(array(
                                                 XOOPS_NOTIFICATION_MODE_SENDALWAYS => _NOT_MODE_SENDALWAYS,
                                                 XOOPS_NOTIFICATION_MODE_SENDONCETHENDELETE => _NOT_MODE_SENDONCE,
                                                 XOOPS_NOTIFICATION_MODE_SENDONCETHENWAIT => _NOT_MODE_SENDONCEPERLOGIN
                                            ));
        $this->addElement($notify_mode_select);
        $this->addElement(new XoopsFormTextArea(_AM_SYSTEM_USERS_EXTRAINFO, "bio", $bio_value, 5, 5));

        if ($xoops->isActiveModule('userrank')) {
            $rank_select = new XoopsFormSelect(_AM_SYSTEM_USERS_RANK, "rank", $rank_value);
            $ranklist = XoopsLists::getUserRankList();
            $rank_select->addOption(0, "--------------");
            if (count($ranklist) > 0) {
                $rank_select->addOptionArray($ranklist);
            }
            $this->addElement($rank_select);
        } else {            $this->addElement(new XoopsFormHidden("rank", $rank_value));        }
        // adding a new user requires password fields
        if (!$form_isedit) {
            $this->addElement(new XoopsFormPassword(_AM_SYSTEM_USERS_PASSWORD, "password", 3, 32), true);
            $this->addElement(new XoopsFormPassword(_AM_SYSTEM_USERS_RETYPEPD, "pass2", 3, 32), true);
        } else {
            $this->addElement(new XoopsFormPassword(_AM_SYSTEM_USERS_PASSWORD, "password", 3, 32));
            $this->addElement(new XoopsFormPassword(_AM_SYSTEM_USERS_RETYPEPD, "pass2", 3, 32));
        }
        $this->addElement(new XoopsFormRadioYN(_AM_SYSTEM_USERS_MAILOK, 'user_mailok', $mailok_value));

        //Groups administration addition XOOPS 2.0.9: Mith
        $gperm_handler = $xoops->getHandlerGroupperm();
        //If user has admin rights on groups
        if ($gperm_handler->checkRight("system_admin", XOOPS_SYSTEM_GROUP, $xoops->user->getGroups(), 1)) {
            //add group selection
            $group_select[] = new XoopsFormSelectGroup(_AM_SYSTEM_USERS_GROUPS, 'groups', false, $groups, 5, true);
        } else {
            //add each user groups
            foreach ($groups as $key => $group) {
                $group_select[] = new XoopsFormHidden('groups[' . $key . ']', $group);
            }
        }
        foreach ($group_select as $group) {
            $this->addElement($group);
            unset($group);
        }

        $this->addElement(new XoopsFormHidden("fct", "users"));
        $this->addElement(new XoopsFormHidden("op", "users_save"));
        $this->addElement(new XoopsFormButton("", "submit", _SUBMIT, "submit", 'btn primary formButton'));

        if (!empty($uid_value)) {
            $this->addElement(new XoopsFormHidden("uid", $uid_value));
        }
    }
}
