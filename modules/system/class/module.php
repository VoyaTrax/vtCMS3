<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author          Andricq Nicolas (AKA MusS)
 * @package         System
 * @version         $Id: module.php 10783 2013-01-13 13:23:32Z trabis $
 */

class SystemModule
{
    public $error = array();

    public $trace = array();

    protected $_list = array();

    protected $_mods = array();

    protected $config_delng = array();
    protected $template_delng = array();

    protected $config_old = array();

    protected $_reservedTables = array(
        'block_module_link',
        'cache_model',
        'config',
        'configoption',
        'groups',
        'groups_users_link',
        'group_permission',
        'imgset',
        'imgsetimg',
        'imgset_tplset_link',
        'modules',
        'newblocks',
        'online',
        'priv_msgs',
        'session',
        'tplfile',
        'tplset',
        'tplsource',
        'users',
    );

    public function __construct()
    {
        // Get main instance
        $xoops = Xoops::getInstance();
        $module_handler = $xoops->getHandlerModule();

        $this->_list = XoopsLists::getModulesList();

        $mods = $module_handler->getObjectsArray();
        foreach (array_keys($mods) as $i) {
            $this->_mods[] = $mods[$i]->getInfo('dirname');
        }
    }

    /**
     * @return array
     */
    public function getModuleList()
    {
        // Get main instance
        $xoops = Xoops::getInstance();
        $module_handler = $xoops->getHandlerModule();
        $moduleperm_handler = $xoops->getHandlerGroupperm();

        $criteria = new CriteriaCompo();
        $criteria->setSort('weight');
        // Get all installed modules
        $ret = $module_handler->getObjects($criteria, true);
        $list = array();
        foreach (array_keys($ret) as $i) {
            if (!$ret[$i]->getInfo('extension')) {
                if ($ret[$i]->getInfo('dirname') == 'system') {
                    $ret[$i]->setInfo('can_delete', false);
                    $ret[$i]->setInfo('can_disable', false);
                } else {
                    $ret[$i]->setInfo('can_delete', true);
                    $ret[$i]->setInfo('can_disable', true);
                }
                if (round($ret[$i]->getInfo('version'), 2) != $ret[$i]->getVar('version')) {
                    $ret[$i]->setInfo('warning_update', true);
                }
                if (XoopsLoad::fileExists(XOOPS_ROOT_PATH . '/modules/' . $ret[$i]->getVar('dirname') . '/icons/logo_small.png')) {
                    $ret[$i]->setInfo('logo_small', XOOPS_URL . '/modules/' . $ret[$i]->getVar('dirname') . '/icons/logo_small.png');
                } else {
                    $ret[$i]->setInfo('logo_small', XOOPS_URL . '/media/xoops/images/icons/16/default.png');
                }
                $ret[$i]->setInfo('version', round($ret[$i]->getVar('version') / 100, 2));
                $ret[$i]->setInfo('update', XoopsLocal::formatTimestamp($ret[$i]->getVar('last_update'), 's'));
                $ret[$i]->setInfo('link_admin', XOOPS_URL . '/modules/' . $ret[$i]->getVar('dirname') . '/' . $ret[$i]->getInfo('adminindex'));

                if ($ret[$i]->getVar('isactive')) {
                    $ret[$i]->setInfo('options', $ret[$i]->getAdminMenu());
                }

                $sadmin = $moduleperm_handler->checkRight('module_admin', $ret[$i]->getVar('mid'), $xoops->user->getGroups());
                if ($sadmin && ($ret[$i]->getVar('hasnotification') || is_array($ret[$i]->getInfo('config')) || is_array($ret[$i]->getInfo('comments')))) {
                    $ret[$i]->setInfo('link_pref', XOOPS_URL . '/modules/system/admin.php?fct=preferences&amp;op=showmod&amp;mod=' . $ret[$i]->getVar('mid'));
                }

                $list[] = $ret[$i];
            }
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getModuleInstall()
    {
        // Get main instance
        $xoops = Xoops::getInstance();
        $module_handler = $xoops->getHandlerModule();

        $ret = array();
        $i = 0;
        foreach ($this->_list as $file) {
            if (XoopsLoad::fileExists(XOOPS_ROOT_PATH . '/modules/' . $file . '/xoops_version.php')) {
                clearstatcache();
                $file = trim($file);
                if (!in_array($file, $this->_mods)) {
                    /* @var $module XoopsModule */
                    $module = $module_handler->create();
                    $module->loadInfo($file);
                    if (!$module->getInfo('extension')) {
                        $module->setInfo('mid', $i);
                        $module->setInfo('version', round($module->getInfo('version'), 2));
                        $ret[] = $module;
                        unset($module);
                        $i++;
                    }
                }
            }
        }
        return $ret;
    }

    /**
     * @param string $mod
     * @param bool   $force
     *
     * @return bool|XoopsModule|XoopsObject
     */
    public function install($mod = '', $force = false)
    {
        $queryFunc = (bool) $force ? "queryF" : "query";
        $xoops = Xoops::getInstance();
        $module_handler = $xoops->getHandlerModule();
        $mod = trim($mod);
        if ($module_handler->getCount(new Criteria('dirname', $mod)) == 0) {
            /* @var $module XoopsModule */
            $module = $module_handler->create();
            $module->loadInfoAsVar($mod);
            $module->setVar('weight', 1);
            $module->setVar('isactive', 1);
            $module->setVar('last_update', time());
            $install_script = $module->getInfo('onInstall');
            if ($install_script && trim($install_script) != '') {
                include_once $xoops->path('modules/' . $mod . '/' . trim($install_script));
            }
            $func = "xoops_module_pre_install_{$mod}";
            // If pre install function is defined, execute
            if (function_exists($func)) {
                $result = $func($module);
                if (!$result) {
                    $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILED_EXECUTE, $func);
                    $this->error = array_merge($this->error, $module->getErrors());
                    return false;
                } else {
                    $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_SUCESS, "<strong>{$func}</strong>");
                    $this->trace = array_merge($this->trace, $module->getMessages());
                }
            }
            // Add SQL module tables
            $created_tables = array();
            if (count($this->error) == 0) {
                $sql_file = $module->getInfo('sqlfile');
                if (is_array($sql_file) && !empty($sql_file[XOOPS_DB_TYPE])) {
                    $sql_file_path = XOOPS_ROOT_PATH . '/modules/' . $mod . '/' . $sql_file[XOOPS_DB_TYPE];
                    if (!XoopsLoad::fileExists($sql_file_path)) {
                        $this->error[] = sprintf(_AM_SYSTEM_MODULES_SQL_NOT_FOUND, "<strong>{$sql_file_path}</strong>");
                        return false;
                    } else {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_SQL_FOUND, "<strong>{$sql_file_path}</strong>");
                        $this->trace[] = _AM_SYSTEM_MODULES_CREATE_TABLES;

                        $sql_query = fread(fopen($sql_file_path, 'r'), filesize($sql_file_path));
                        $sql_query = trim($sql_query);
                        SqlUtility::splitMySqlFile($pieces, $sql_query);
                        foreach ($pieces as $piece) {
                            // [0] contains the prefixed query
                            // [4] contains unprefixed table name
                            $prefixed_query = SqlUtility::prefixQuery($piece, $xoops->db()->prefix());
                            if (!$prefixed_query) {
                                $this->error[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_SQL_NOT_VALID, "<strong>" . $piece . "</strong>") . '</span>';
                                break;
                            }
                            // check if the table name is reserved
                            if (!in_array($prefixed_query[4], $this->_reservedTables) || $mod == 'system') {
                                // not reserved, so try to create one
                                if (!$xoops->db()->$queryFunc($prefixed_query[0])) {
                                    $this->error[] = $xoops->db()->error();
                                    break;
                                } else {
                                    if (!in_array($prefixed_query[4], $created_tables)) {
                                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TABLE_CREATED, "<strong>" . $xoops->db()->prefix($prefixed_query[4]) . "</strong>");
                                        $created_tables[] = $prefixed_query[4];
                                    } else {
                                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_INSERT_DATA, "<strong>" . $xoops->db()->prefix($prefixed_query[4]) . "</strong>");
                                    }
                                }
                            } else {
                                // the table name is reserved, so halt the installation
                                $this->error[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TABLE_RESERVED, "<strong>" . $prefixed_query[4] . "</strong>");
                                break;
                            }
                        }
                        // if there was an error, delete the tables created so far, so the next installation will not fail
                        if (count($this->error) > 0) {
                            foreach ($created_tables as $table) {
                                $xoops->db()->$queryFunc("DROP TABLE " . $xoops->db()->prefix($table));
                            }
                            return false;
                        }
                    }
                }
            }
            // Save module info, blocks, templates and perms
            if (count($this->error) == 0) {
                if (!$module_handler->insertModule($module)) {
                    $this->error[] = sprintf(_AM_SYSTEM_MODULES_INSERT_DATA_FAILD, "<strong>" . $module->getVar('name') . "</strong>");
                    foreach ($created_tables as $ct) {
                        $xoops->db()->$queryFunc("DROP TABLE " . $xoops->db()->prefix($ct));
                    }
                    $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILINS, "<strong>" . $module->name() . "</strong>");
                    $this->error[] = _AM_SYSTEM_MODULES_ERRORSC;
                    unset($module);
                    unset($created_tables);
                    return false;
                } else {
                    unset($created_tables);
                    $this->trace[] = _AM_SYSTEM_MODULES_INSERT_DATA_DONE . sprintf(_AM_SYSTEM_MODULES_MODULEID, "<strong>" . $module->getVar('mid') . "</strong>");

                    // install Templates
                    $this->installTemplates($module, 'add');

                    $xoops->templateClearModuleCache($module->getVar('mid'));

                    // install blocks
                    $this->installBlocks($module, 'add');

                    // Install Configs
                    $this->installConfigs($module, 'add');
                }
                if ($module->getInfo('hasMain')) {
                    $groups = array(XOOPS_GROUP_ADMIN, XOOPS_GROUP_USERS, XOOPS_GROUP_ANONYMOUS);
                } else {
                    $groups = array(XOOPS_GROUP_ADMIN);
                }
                // retrieve all block ids for this module
                $block_handler = $xoops->getHandlerBlock();
                $blocks = $block_handler->getByModule($module->getVar('mid'), false);
                $this->trace[] = _AM_SYSTEM_MODULES_GROUP_SETTINGS_ADD;
                $gperm_handler = $xoops->getHandlerGroupperm();
                foreach ($groups as $mygroup) {
                    if ($gperm_handler->checkRight('module_admin', 0, $mygroup)) {
                        $mperm = $gperm_handler->create();
                        $mperm->setVar('gperm_groupid', $mygroup);
                        $mperm->setVar('gperm_itemid', $module->getVar('mid'));
                        $mperm->setVar('gperm_name', 'module_admin');
                        $mperm->setVar('gperm_modid', 1);
                        if (!$gperm_handler->insert($mperm)) {
                            $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_ACCESS_ADMIN_ADD_ERROR, "<strong>" . $mygroup . "</strong>") . '</span>';
                        } else {
                            $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_ACCESS_ADMIN_ADD, "<strong>" . $mygroup . "</strong>");
                        }
                        unset($mperm);
                    }
                    $mperm = $gperm_handler->create();
                    $mperm->setVar('gperm_groupid', $mygroup);
                    $mperm->setVar('gperm_itemid', $module->getVar('mid'));
                    $mperm->setVar('gperm_name', 'module_read');
                    $mperm->setVar('gperm_modid', 1);
                    if (!$gperm_handler->insert($mperm)) {
                        $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_ACCESS_USER_ADD_ERROR, "<strong>" . $mygroup . "</strong>") . '</span>';
                    } else {
                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_ACCESS_USER_ADD_ERROR, "<strong>" . $mygroup . "</strong>");
                    }
                    unset($mperm);
                    foreach ($blocks as $blc) {
                        $bperm = $gperm_handler->create();
                        $bperm->setVar('gperm_groupid', $mygroup);
                        $bperm->setVar('gperm_itemid', $blc);
                        $bperm->setVar('gperm_name', 'block_read');
                        $bperm->setVar('gperm_modid', 1);
                        if (!$gperm_handler->insert($bperm)) {
                            $this->trace[]['sub'] = '<span class="red">' . _AM_SYSTEM_MODULES_BLOCK_ACCESS_ERROR . ' Block ID: <strong>' . $blc . '</strong> Group ID: <strong>' . $mygroup . '</strong></span>';
                        } else {
                            $this->trace[]['sub'] = _AM_SYSTEM_MODULES_BLOCK_ACCESS . sprintf(_AM_SYSTEM_MODULES_BLOCK_ID, "<strong>" . $blc . "</strong>") . sprintf(_AM_SYSTEM_MODULES_GROUP_ID, "<strong>" . $mygroup . "</strong>");
                        }
                        unset($bperm);
                    }
                }
                unset($blocks);
                unset($groups);

                // execute module specific install script if any
                // If pre install function is defined, execute
                $func = "xoops_module_install_{$mod}";
                if (function_exists($func)) {
                    $result = $func($module);
                    if (!$result) {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_EXECUTE, $func);
                        $this->trace = array_merge($this->trace, $module->getErrors());
                    } else {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_SUCESS, "<strong>{$func}</strong>");
                        $this->trace = array_merge($this->trace, $module->getMessages());
                    }
                }

                $this->trace[] = sprintf(_AM_SYSTEM_MODULES_OKINS, '<strong>' . $module->getVar('name', 's') . '</strong>');
                unset($blocks);
                return $module;
            }
        } else {
            $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILINS, "<strong>" . $mod . "</strong>") . "&nbsp;" . _AM_SYSTEM_MODULES_ERRORSC;
            return false;
        }
    }

    /**
     * @param string $mod
     *
     * @return bool|XoopsModule
     */
    public function uninstall($mod = '')
    {
        $xoops = Xoops::getInstance();
        $module_handler = $xoops->getHandlerModule();
        $module = $module_handler->getByDirname($mod);
        $xoops->templateClearModuleCache($module->getVar('mid'));

        if ($module->getVar('dirname') == 'system') {
            $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILUNINS, "<strong>" . $module->getVar('name') . "</strong>") . "&nbsp;" . _AM_SYSTEM_MODULES_ERRORSC;
            $this->error[] = " - " . _AM_SYSTEM_MODULES_SYSNO;
            return false;
        } elseif ($module->getVar('dirname') == $xoops->getConfig('startpage')) {
            $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILUNINS, "<strong>" . $module->getVar('name') . "</strong>") . "&nbsp;" . _AM_SYSTEM_MODULES_ERRORSC;
            $this->error[] = " - " . _AM_SYSTEM_MODULES_STRTNO;
            return false;
        } else {
            // Load module specific install script if any
            $uninstall_script = $module->getInfo('onUninstall');
            if ($uninstall_script && trim($uninstall_script) != '') {
                include_once $xoops->path('modules/' . $mod . '/' . trim($uninstall_script));
            }
            $func = "xoops_module_pre_uninstall_{$mod}";
            // If pre uninstall function is defined, execute
            if (function_exists($func)) {
                $result = $func($module);
                if (!$result) {
                    $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILED_EXECUTE, $func);
                    $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILUNINS, "<strong>" . $module->getVar('name') . "</strong>") . "&nbsp;" . _AM_SYSTEM_MODULES_ERRORSC;
                    $this->error = array_merge($this->error, $module->getErrors());
                    return false;
                } else {
                    $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_SUCESS, "<strong>{$func}</strong>");
                    $this->trace = array_merge($this->trace, $module->getMessages());
                }
            }

            if (false === $module_handler->deleteModule($module)) {
                $this->error[] = sprintf(_AM_SYSTEM_MODULES_DELETE_ERROR, $module->getVar('name'));
                return false;
            } else {
                // delete templates
                $this->deleteTemplates($module);

                // Delete blocks and block template files
                $this->deleteBlocks($module);

                // Delete tables used by this module
                $modtables = $module->getInfo('tables');
                if ($modtables != false && is_array($modtables)) {
                    $this->trace[] = _AM_SYSTEM_MODULES_DELETE_MOD_TABLES;
                    foreach ($modtables as $table) {
                        // prevent deletion of reserved core tables!
                        if (!in_array($table, $this->_reservedTables)) {
                            $sql = 'DROP TABLE ' . $xoops->db()->prefix($table);
                            if (!$xoops->db()->query($sql)) {
                                $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TABLE_DROPPED_ERROR, "<strong>" . $xoops->db()->prefix($table) . "<strong>") . "</span>";
                            } else {
                                $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TABLE_DROPPED, "<strong>" . $xoops->db()->prefix($table) . "</strong>");
                            }
                        } else {
                            $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TABLE_DROPPED_FAILDED, "<strong>" . $xoops->db()->prefix($table) . "</strong>") . "</span>";
                        }
                    }
                }

                // delete permissions if any
                $gperm_handler = $xoops->getHandlerGroupperm();
                if (false === $gperm_handler->deleteByModule($module->getVar('mid'))) {
                    $this->trace[] = '<span class="red">' . _AM_SYSTEM_MODULES_GROUP_PERMS_DELETE_ERROR . '</span>';
                } else {
                    $this->trace[] = _AM_SYSTEM_MODULES_GROUP_PERMS_DELETED;
                }

                // delete module config options if any
                $this->deleteConfigs( $module );

                // execute module specific install script if any
                $func = 'xoops_module_uninstall_' . $mod;
                if (function_exists($func)) {
                    $result = $func($module);
                    if (!$result) {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_EXECUTE, $func);
                        $this->trace = array_merge($this->error, $module->getErrors());
                    } else {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_SUCESS, "<strong>{$func}</strong>");
                        $this->trace = array_merge($this->trace, $module->getMessages());
                    }
                }
                $this->trace[] = sprintf(_AM_SYSTEM_MODULES_OKUNINS, "<strong>" . $module->getVar('name') . "</strong>");
                XoopsPreload::getInstance()->triggerEvent('onModuleUninstall', array(&$module, &$this));
                return $module;
            }
        }
    }

    public function update($mod = '')
    {
        $xoops = Xoops::getInstance();
        $module_handler = $xoops->getHandlerModule();
        $block_handler = $xoops->getHandlerBlock();
        $module = $module_handler->getByDirname($mod);
        $xoops->templateClearModuleCache($module->getVar('mid'));
        // Save current version for use in the update function
        $prev_version = $module->getVar('version');
        // we dont want to change the module name set by admin
        $temp_name = $module->getVar('name');
        $module->loadInfoAsVar($module->getVar('dirname'));
        $module->setVar('name', $temp_name);
        $module->setVar('last_update', time());

        if (!$module_handler->insertModule($module)) {
            $this->error[] = sprintf(_AM_SYSTEM_MODULES_FAILUPD, "<strong>" . $module->getVar('name') . "</strong>");
            return false;
        } else {
            $this->trace[] = _AM_SYSTEM_MODULES_MODULE_DATA_UPDATE;
            // delete templates
            $this->deleteTemplates($module);

            // install templates
            $this->installTemplates($module, 'update');

            // install blocks
            $this->installBlocks($module, 'rebuild');

            // reset compile_id
            $xoops->tpl()->setCompileId();

            // first delete all config entries
            $this->deleteConfigs( $module );

            // Install Configs
            $this->installConfigs($module, 'update');

            // execute module specific update script if any
            $update_script = $module->getInfo('onUpdate');
            if (false != $update_script && trim($update_script) != '') {
                include_once $xoops->path('modules/' . $mod . '/' . trim($update_script));
                $func = 'xoops_module_update_' . $mod;
                if (function_exists($func)) {
                    $result = $func($module, $prev_version);
                    if (!$result) {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_EXECUTE, $func);
                        $this->trace = array_merge($this->error, $module->getErrors());
                    } else {
                        $this->trace[] = sprintf(_AM_SYSTEM_MODULES_FAILED_SUCESS, "<strong>{$func}</strong>");
                        $this->trace = array_merge($this->trace, $module->getMessages());
                    }
                }
            }
            $this->trace[] = sprintf(_AM_SYSTEM_MODULES_OKUPD, '<strong>' . $module->getVar('name', 's') . '</strong>');
            return $module;
        }
    }

    /**
     * @param string $dirname
     * @param string $template
     * @param string $type
     *
     * @return string
     */
    function getTemplate($dirname, $template, $type = '')
    {
        $xoops = Xoops::getInstance();
        $ret = '';
        switch ($type) {
            case 'blocks':
            case 'admin':
                $path = $xoops->path('modules/' . $dirname . '/templates/' . $type . '/' . $template);
                break;
            default:
                $path = $xoops->path('modules/' . $dirname . '/templates/' . $template);
                break;
        }
        if (!XoopsLoad::fileExists($path)) {
            return $ret;
        } else {
            $lines = file($path);
        }
        if (!$lines) {
            return $ret;
        }
        $count = count($lines);
        for ($i = 0; $i < $count; $i++) {
            $ret .= str_replace("\n", "\r\n", str_replace("\r\n", "\n", $lines[$i]));
        }
        return $ret;
    }

    function installTemplates($module, $action='add')
    {
        $xoops = Xoops::getInstance();
        $templates = $module->getInfo('templates');
        if (is_array($templates) && count($templates) > 0) {
            $this->trace[] = constant('_AM_SYSTEM_MODULES_TEMPLATES_' . strtoupper($action));
            $tplfile_handler = $xoops->getHandlerTplfile();
            foreach ($templates as $tpl) {
                $tpl['file'] = trim($tpl['file']);
                if (!in_array($tpl['file'], $this->template_delng)) {
                    $type = (isset($tpl['type']) ? $tpl['type'] : 'module');
                    $tpldata = $this->getTemplate($module->getVar('dirname'), $tpl['file'], $type);
                    $tplfile = $tplfile_handler->create();
                    $tplfile->setVar('tpl_refid', $module->getVar('mid'));
                    $tplfile->setVar('tpl_lastimported', 0);
                    $tplfile->setVar('tpl_lastmodified', time());

                    if (preg_match("/\.css$/i", $tpl['file'])) {
                        $tplfile->setVar('tpl_type', 'css');
                    } else {
                        $tplfile->setVar('tpl_type', $type);
                    }
                    $tplfile->setVar('tpl_source', $tpldata, true);
                    $tplfile->setVar('tpl_module', $module->getVar('dirname'));
                    $tplfile->setVar('tpl_tplset', 'default');
                    $tplfile->setVar('tpl_file', $tpl['file'], true);
                    $tplfile->setVar('tpl_desc', $tpl['description'], true);
                    if (!$tplfile_handler->insertTpl($tplfile)) {
                        $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_ADD_ERROR, "<strong>" . $tpl['file'] . "</strong>") . '</span>';
                    } else {
                        $newid = $tplfile->getVar('tpl_id');
                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TEMPLATE_INSERT_DATA, "<strong>" . $tpl['file'] . "</strong>");
                        if ($module->getVar('dirname') == 'system') {
                            if (!$xoops->templateTouch($newid)) {
                                $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE_ERROR, "<strong>" . $tpl['file'] . "</strong>") . '</span>';
                            } else {
                                $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE, "<strong>" . $tpl['file'] . "</strong>");
                            }
                        } else {
                            if ($xoops->config['template_set'] == 'default') {
                                if (!$xoops->templateTouch($newid)) {
                                    $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE_ERROR, "<strong>" . $tpl['file'] . "</strong>") . '</span>';
                                } else {
                                    $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE, "<strong>" . $tpl['file'] . "</strong>");
                                }
                            }
                        }
                    }
                    unset($tpldata);
                } else {
                    $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_DELETE_OLD_ERROR, "<strong>" . $tpl['file'] . "</strong>") . '</span>';
                }
            }
        }
    }

    function deleteTemplates($module)
    {
        $xoops = Xoops::getInstance();
        $tplfile_handler = $xoops->getHandlerTplfile();
        $templates = $tplfile_handler->find('default', 'module', $module->getVar('mid'));
        if (is_array($templates) && count($templates) > 0) {
            $this->trace[] = _AM_SYSTEM_MODULES_TEMPLATES_DELETE;
            // delete template file entry in db
            $dcount = count($templates);
            for ($i = 0; $i < $dcount; $i++) {
                if (!$tplfile_handler->deleteTpl($templates[$i])) {
                    $this->template_delng[] = $templates[$i]->getVar('tpl_file');
                }
            }
        }
    }

    function installBlocks($module, $action='add')
    {
        $xoops = Xoops::getInstance();
        $blocks = $module->getInfo('blocks');
        $this->trace[] = constant('_AM_SYSTEM_MODULES_BLOCKS_' . strtoupper($action));
        $block_handler = $xoops->getHandlerBlock();
        if (is_array($blocks) && count($blocks) > 0) {
            $tplfile_handler = $xoops->getHandlerTplfile();
            $showfuncs = array();
            $funcfiles = array();
            foreach ($blocks as $i => $block) {
                if (isset($block['show_func']) && $block['show_func'] != '' && isset($block['file']) && $block['file'] != '') {
                    $showfuncs[] = $block['show_func'];
                    $funcfiles[] = $block['file'];

                    $criteria = new CriteriaCompo();
                    $criteria->add(new Criteria('mid', $module->getVar('mid')));
                    $criteria->add(new Criteria('func_num', $i));

                    $new_block = false;
                    $block_obj = $block_handler->getObjects($criteria);
                    if (count($block_obj) == 0) {
                        $new_block = true;
                        $block_obj[0] = $block_handler->create();
                        $block_obj[0]->setVar('func_num', $i);
                        $block_obj[0]->setVar('mid', $module->getVar('mid'));
                        $block_obj[0]->setVar('name', addslashes($block['name']));
                        $block_obj[0]->setVar('title', addslashes($block['name']));
                        $block_obj[0]->setVar('side', 0);
                        $block_obj[0]->setVar('weight', 0);
                        $block_obj[0]->setVar('visible', 0);
                        $block_obj[0]->setVar('block_type', ($module->getVar('dirname') == 'system') ? 'S' : 'M');
                        $block_obj[0]->setVar('isactive', 1);
                        $block_obj[0]->setVar('content', '');
                        $block_obj[0]->setVar('c_type', 'H');
                        $block_obj[0]->setVar('dirname', $module->getVar('dirname'));
                        $block_obj[0]->setVar('options', isset($block['options']) ? $block['options'] : '');
                    }
                    $block_obj[0]->setVar('func_file', $block['file']);
                    $block_obj[0]->setVar('show_func', isset($block['show_func']) ? $block['show_func'] : '');
                    $block_obj[0]->setVar('edit_func', isset($block['edit_func']) ? $block['edit_func'] : '');
                    $template = $this->getTemplate($module->getVar('dirname'), $block['template'], 'blocks');
                    $block_obj[0]->setVar('template', !empty($template) ? $block['template'] : '');
                    $block_obj[0]->setVar('last_modified', time());

                    if (!$block_handler->insert($block_obj[0])) {
                        $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_UPDATE_ERROR, $block_obj[0]->getVar('name')) . '</span>';
                    } else {
                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_BLOCK_UPDATE, $block_obj[0]->getVar('name')) . sprintf(_AM_SYSTEM_MODULES_BLOCK_ID, "<strong>" . $block_obj[0]->getVar('bid') . "</strong>");

                        $blockmodulelink_handler = $xoops->getHandlerBlockmodulelink();
                        $blockmodulelink = $blockmodulelink_handler->create();
                        $blockmodulelink->setVar('block_id', $block_obj[0]->getVar('bid'));
                        $blockmodulelink->setVar('module_id', 0); //show on all pages
                        $blockmodulelink_handler->insert($blockmodulelink);

                        if ($template != '') {
                            $tplfile = $tplfile_handler->find('default', 'block', $block_obj[0]->getVar('bid'));
                            if (count($tplfile) == 0) {
                                $tplfile_new = $tplfile_handler->create();
                                $tplfile_new->setVar('tpl_module', $module->getVar('dirname'));
                                $tplfile_new->setVar('tpl_refid', $block_obj[0]->getVar('bid'));
                                $tplfile_new->setVar('tpl_tplset', 'default');
                                $tplfile_new->setVar('tpl_file', $block_obj[0]->getVar('template'), true);
                                $tplfile_new->setVar('tpl_type', 'block');
                            } else {
                                $tplfile_new = $tplfile[0];
                                $tplfile_new->setVars($tplfile_new->getValues());
                            }
                            $tplfile_new->setVar('tpl_source', $template, true);
                            $tplfile_new->setVar('tpl_desc', $block['description'], true);
                            $tplfile_new->setVar('tpl_lastmodified', time());
                            $tplfile_new->setVar('tpl_lastimported', 0);
                            if (!$tplfile_handler->insertTpl($tplfile_new)) {
                                $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_UPDATE_ERROR, "<strong>" . $block['template'] . "</strong>") . '</span>';
                            } else {
                                $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TEMPLATE_UPDATE, "<strong>" . $block['template'] . "</strong>");
                                if ($module->getVar('dirname') == 'system') {
                                    if (!$xoops->templateTouch($tplfile_new->getVar('tpl_id'))) {
                                        $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE_ERROR, "<strong>" . $block['template'] . "</strong>") . '</span>';
                                    } else {
                                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE, "<strong>" . $block['template'] . "</strong>");
                                    }
                                } else {
                                    if ($xoops->config['template_set'] == 'default') {
                                        if (!$xoops->templateTouch($tplfile_new->getVar('tpl_id'))) {
                                            $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE_ERROR, "<strong>" . $block['template'] . "</strong>") . '</span>';
                                        } else {
                                            $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_TEMPLATE_RECOMPILE, "<strong>" . $block['template'] . "</strong>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $blocks = $block_handler->getByModule($module->getVar('mid'));
        foreach ($blocks as $block) {
            /* @var $block XoopsBlock */
            if (!in_array($block->getVar('show_func'), $showfuncs) || !in_array($block->getVar('func_file'), $funcfiles)) {
                $sql = sprintf("DELETE FROM %s WHERE bid = %u", $xoops->db()->prefix('newblocks'), $block->getVar('bid'));
                if (!$xoops->db()->query($sql)) {
                    $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_BLOCK_DELETE_ERROR, "<strong>" . $block->getVar('name') . "</strong>") . sprintf(_AM_SYSTEM_MODULES_BLOCK_ID, "<strong>" . $block->getVar('bid') . "</strong>") . '</span>';
                } else {
                    $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_BLOCK_DELETE, '<strong>' . $block->getVar('name') . '</strong>') . '&nbsp;' . sprintf(_AM_SYSTEM_MODULES_BLOCK_ID, '<strong>' . $block->getVar('bid') . '</strong>');
                    if ($block->getVar('template') != '') {
                        $tplfiles = $tplfile_handler->find(null, 'block', $block->getVar('bid'));
                        if (is_array($tplfiles)) {
                            $btcount = count($tplfiles);
                            for ($k = 0; $k < $btcount; $k++) {
                                if (!$tplfile_handler->deleteTpl($tplfiles[$k])) {
                                    $this->trace[]['sub'] = '<span class="red">' . _AM_SYSTEM_MODULES_BLOCK_DEPRECATED_ERROR . '(ID: <strong>' . $tplfiles[$k]->getVar('tpl_id') . '</strong>)</span>';
                                } else {
                                    $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_BLOCK_DEPRECATED, "<strong>" . $tplfiles[$k]->getVar('tpl_file') . "</strong>");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function deleteBlocks($module)
    {
        $xoops = Xoops::getInstance();
        $block_handler = $xoops->getHandlerBlock();
        $blocks = $block_handler->getByModule($module->getVar('mid'));
        if (is_array($blocks) && count($blocks) > 0) {
            $tplfile_handler = $xoops->getHandlerTplfile();
            $bcount = count($blocks);
            $this->trace[] = _AM_SYSTEM_MODULES_BLOCKS_DELETE;
            for ($i = 0; $i < $bcount; $i++) {
                if (false === $block_handler->deleteBlock($blocks[$i])) {
                    $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_BLOCK_DELETE_ERROR, "<strong>" . $blocks[$i]->getVar('name') . "</strong>") . sprintf(_AM_SYSTEM_MODULES_BLOCK_ID, "<strong>" . $blocks[$i]->getVar('bid') . "</strong>") . '</span>';
                } else {
                    $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_BLOCK_DELETE, "<strong>" . $blocks[$i]->getVar('name') . "</strong>") . sprintf(_AM_SYSTEM_MODULES_BLOCK_ID, "<strong>" . $blocks[$i]->getVar('bid') . "</strong>");
                }
                if ($blocks[$i]->getVar('template') != '') {
                    $templates = $tplfile_handler->find(null, 'block', $blocks[$i]->getVar('bid'));
                    $btcount = count($templates);
                    if ($btcount > 0) {
                        for ($j = 0; $j < $btcount; $j++) {
                            if (!$tplfile_handler->delete($templates[$j])) {
                                $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_BLOCK_DELETE_TEMPLATE_ERROR, $templates[$j]->getVar('tpl_file')) . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_ID, "<strong>" . $templates[$j]->getVar('tpl_id') . "</strong>") . '</span>';
                            } else {
                                $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_BLOCK_DELETE_DATA, "<strong>" . $templates[$j]->getVar('tpl_file') . "</strong>") . sprintf(_AM_SYSTEM_MODULES_TEMPLATE_ID, "<strong>" . $templates[$j]->getVar('tpl_id') . "</strong>");
                            }
                        }
                    }
                    unset($templates);
                }
            }
        }
    }

    function deleteConfigs( $module )
    {
        $xoops = Xoops::getInstance();

        $config_handler = $xoops->getHandlerConfig();
        $configs = $config_handler->getConfigs(new Criteria('conf_modid', $module->getVar('mid')));
        if (is_array($configs) && count($configs) > 0) {
            $this->trace[] = _AM_SYSTEM_MODULES_MODULE_DATA_DELETE;
            $confcount = count($configs);
            for ($i = 0; $i < $confcount; $i++) {
                if (!$config_handler->deleteConfig($configs[$i])) {
                    $this->trace[]['sub'] = '<span class="red">' . _AM_SYSTEM_MODULES_CONFIG_DATA_DELETE_ERROR . sprintf(_AM_SYSTEM_MODULES_CONFIG_ID, "<strong>" . $configs[$i]->getvar('conf_id') . "</strong>") . '</span>';
                    // save the name of config failed to delete for later use
                    $this->config_delng[] = $configs[$i]->getvar('conf_name');
                } else {
                    $this->config_old[$configs[$i]->getvar('conf_name')]['value'] = $configs[$i]->getvar('conf_value', 'N');
                    $this->config_old[$configs[$i]->getvar('conf_name')]['formtype'] = $configs[$i]->getvar('conf_formtype');
                    $this->config_old[$configs[$i]->getvar('conf_name')]['valuetype'] = $configs[$i]->getvar('conf_valuetype');
                    $this->trace[]['sub'] = _AM_SYSTEM_MODULES_CONFIG_DATA_DELETE . sprintf(_AM_SYSTEM_MODULES_CONFIG_ID, "<strong>" . $configs[$i]->getVar('conf_id') . "</strong>");
                }
            }
        }
    }

    function installconfigs($module, $action='add')
    {
        $xoops = Xoops::getInstance();
        // now reinsert them with the new settings
        $configs = $module->getInfo('config');
        if (!is_array($configs)) {
            $configs = array();
        }

        XoopsPreload::getInstance()->triggerEvent('onModuleUpdateConfigs', array($module, &$configs));

        if (is_array($configs) && count($configs) > 0) {
            $this->trace[] = constant('_AM_SYSTEM_MODULES_MODULE_DATA_' . strtoupper($action));
            $config_handler = $xoops->getHandlerConfig();
            $order = 0;
            foreach ($configs as $config) {
                // only insert ones that have been deleted previously with success
                if (!in_array($config['name'], $this->config_delng)) {
                    $confobj = $config_handler->createConfig();
                    $confobj->setVar('conf_modid', $module->getVar('mid'));
                    $confobj->setVar('conf_catid', 0);
                    $confobj->setVar('conf_name', $config['name']);
                    $confobj->setVar('conf_title', $config['title'], true);
                    $confobj->setVar('conf_desc', $config['description'], true);
                    $confobj->setVar('conf_formtype', $config['formtype']);
                    $confobj->setVar('conf_valuetype', $config['valuetype']);
                    if (isset($this->config_old[$config['name']]['value']) && $this->config_old[$config['name']]['formtype'] == $config['formtype'] && $this->config_old[$config['name']]['valuetype'] == $config['valuetype']) {
                        // preserver the old value if any
                        // form type and value type must be the same
                        $confobj->setVar('conf_value', $this->config_old[$config['name']]['value'], true);
                    } else {
                        $confobj->setConfValueForInput($config['default'], true);
                        //$confobj->setVar('conf_value', $config['default'], true);
                    }
                    $confobj->setVar('conf_order', $order);
                    $confop_msgs = '';
                    if (isset($config['options']) && is_array($config['options'])) {
                        foreach ($config['options'] as $key => $value) {
                            $confop = $config_handler->createConfigOption();
                            $confop->setVar('confop_name', $key, true);
                            $confop->setVar('confop_value', $value, true);
                            $confobj->setConfOptions($confop);
                            $confop_msgs .= '<br />&nbsp;&nbsp;&nbsp;&nbsp; ' . _AM_SYSTEM_MODULES_CONFIG_ADD . _AM_SYSTEM_MODULES_NAME . ' <strong>' . (defined($key) ? constant($key) : $key) . '</strong> ' . _AM_SYSTEM_MODULES_VALUE . ' <strong>' . $value . '</strong> ';
                            unset($confop);
                        }
                    }
                    $order++;
                    if (false != $config_handler->insertConfig($confobj)) {
                        //$msgs[] = '&nbsp;&nbsp;Config <strong>'.$config['name'].'</strong> added to the database.'.$confop_msgs;
                        $this->trace[]['sub'] = sprintf(_AM_SYSTEM_MODULES_CONFIG_DATA_ADD, "<strong>" . $config['name'] . "</strong>") . $confop_msgs;
                    } else {
                        $this->trace[]['sub'] = '<span class="red">' . sprintf(_AM_SYSTEM_MODULES_CONFIG_DATA_ADD_ERROR, "<strong>" . $config['name'] . "</strong>") . '</span>';
                    }
                    unset($confobj);
                }
            }
            unset($configs);
        }
    }
}
