<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Blocks functions
 *
 * @copyright   The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license     GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author      Kazumi Ono (AKA onokazu)
 * @package     system
 * @version     $Id: login.php 10585 2012-12-28 01:54:48Z dugris $
 */

function b_system_login_show()
{
    $xoops = Xoops::getInstance();
    if (!$xoops->isUser()) {
        $block = array();
        $block['lang_username'] = _USERNAME;
        $block['unamevalue'] = "";
        $block['lang_password'] = _PASSWORD;
        $block['lang_login'] = _LOGIN;
        $block['lang_lostpass'] = _MB_SYSTEM_LPASS;
        $block['lang_registernow'] = _MB_SYSTEM_RNOW;
        //$block['lang_rememberme'] = _MB_SYSTEM_REMEMBERME;
        if ($xoops->getConfig('use_ssl') == 1 && $xoops->getConfig('sslloginlink') != '') {
            $block['sslloginlink'] = "<a href=\"javascript:openWithSelfMain('" . $xoops->getConfig('sslloginlink') . "', 'ssllogin', 300, 200);\">" . _MB_SYSTEM_SECURE . "</a>";
        } elseif ($xoops->getConfig('usercookie')) {
            $block['lang_rememberme'] = _MB_SYSTEM_REMEMBERME;
        }
        return $block;
    }
    return false;
}
