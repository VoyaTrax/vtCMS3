<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Template sets Manager
 *
 * @copyright   The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license     GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author      Kazumi Ono (AKA onokazu)
 * @package     system
 * @version     $Id: main.php 10677 2013-01-06 01:12:16Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

// Get main instance
$xoops = Xoops::getInstance();
$system = System::getInstance();
$system_breadcrumb = SystemBreadcrumb::getInstance();

// Check users rights
if (!$xoops->isUser() || !$xoops->isModule() || !$xoops->user->isAdmin($xoops->module->mid())) {
    exit(_NOPERM);
}

// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'default', 'string');

// Call Header
$xoops->header('system_templates.html');
// Define scripts
$xoops->theme()->addScript('media/jquery/jquery.js');
$xoops->theme()->addScript('media/jquery/ui/jquery.ui.js');
$xoops->theme()->addScript('media/jquery/plugins/jquery.easing.js');
$xoops->theme()->addScript('media/jquery/plugins/jqueryFileTree.js');
$xoops->theme()->addScript('modules/system/js/admin.js');
$xoops->theme()->addScript('modules/system/js/templates.js');
$xoops->theme()->addScript('modules/system/js/code_mirror/codemirror.js');
// Define Stylesheet
$xoops->theme()->addStylesheet('modules/system/css/admin.css');
$xoops->theme()->addStylesheet('modules/system/css/code_mirror/docs.css');
// Define Breadcrumb and tips
$system_breadcrumb->addLink(_AM_SYSTEM_TEMPLATES_NAV_MAIN, system_adminVersion('tplsets', 'adminpath'));

switch ($op) {
    //index
    default:

        // Define Breadcrumb and tips
        $admin_page = new XoopsModuleAdmin();
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_CPANEL, XOOPS_URL . '/admin.php', true);
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_TEMPLATES_NAV_MANAGER, $system->adminVersion('tplsets', 'adminpath'));
        $admin_page->renderBreadcrumb();
        $admin_page->addTips(_AM_SYSTEM_TEMPLATES_NAV_TIPS);
        $admin_page->renderTips();

        $xoops->tpl()->assign('index', true);

        $form = new XoopsThemeForm(_AM_SYSTEM_TEMPLATES_GENERATE, "form", 'admin.php?fct=tplsets', "post", true);

        $ele = new XoopsFormSelect(_AM_SYSTEM_TEMPLATES_SET, 'tplset', $xoops->getConfig('tplset'));
        $tplset_handler = $xoops->getHandlerTplset();
        $tplsetlist = $tplset_handler->getNameList();
        asort($tplsetlist);
        foreach ($tplsetlist as $key => $name) {
            $ele->addOption($key, $name);
        }
        $form->addElement($ele);
        $form->addElement(new XoopsFormSelectTheme(_AM_SYSTEM_TEMPLATES_SELECT_THEME, 'select_theme', 1, 5), true);
        $form->addElement(new XoopsFormRadioYN(_AM_SYSTEM_TEMPLATES_FORCE_GENERATED, 'force_generated', 0, _YES, _NO), true);

        $modules = new XoopsFormSelect(_AM_SYSTEM_TEMPLATES_SELECT_MODULES, 'select_modules');

        $module_handler = $xoops->getHandlerModule();
        $criteria = new CriteriaCompo(new Criteria('isactive', 1));
        $moduleslist = $module_handler->getNameList($criteria, true);
        $modules->addOption(0, _AM_SYSTEM_TEMPLATES_ALL_MODULES);
        $modules->addOptionArray($moduleslist);
        $form->addElement($modules, true);

        $form->addElement(new XoopsFormHidden("active_templates", "0"));
        $form->addElement(new XoopsFormHidden("active_modules", "0"));
        $form->addElement(new XoopsFormHidden("op", "tpls_generate_surcharge"));
        $form->addElement(new XoopsFormButton("", "submit", _SUBMIT, "submit"));
        $form->display();
        break;

    //generate surcharge
    case 'tpls_generate_surcharge':
        if (!$xoops->security()->check()) {
            $xoops->redirect('admin.php?fct=tplsets', 3, implode('<br />', $xoops->security()->getErrors()));
        }
        // Assign Breadcrumb menu
        $system_breadcrumb->addHelp(system_adminVersion('tplsets', 'help') . '#override');
        $system_breadcrumb->addLink(_AM_SYSTEM_TEMPLATES_NAV_FILE_GENERATED);
        $system_breadcrumb->render();

        if ($_REQUEST['select_modules'] == '0' || $_REQUEST['active_modules'] == '1') {
            //Generate modules
            if (isset($_REQUEST['select_theme']) && isset($_REQUEST['force_generated'])) {
                //on verifie si le dossier module existe
                $theme_surcharge = XOOPS_THEME_PATH . '/' . $_REQUEST['select_theme'] . '/modules';
                $indexFile = XOOPS_ROOT_PATH . "/modules/system/include/index.html";
                $verif_write = false;
                $text = '';

                if (!is_dir($theme_surcharge)) {
                    //Creation du dossier modules

                    if (!is_dir($theme_surcharge)) {
                        mkdir($theme_surcharge, 0777);
                    }
                    chmod($theme_surcharge, 0777);
                    copy($indexFile, $theme_surcharge . "/index.html");
                }

                $tplset = $system->cleanVars($POST, 'tplset', 'default', 'string');

                //on cr�e uniquement les templates qui n'existent pas
                $module_handler = $xoops->getHandlerModule();
                $tplset_handler = $xoops->getHandlerTplset();
                $tpltpl_handler = $xoops->getHandlerTplfile();

                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('tplset_name', $tplset));
                $tplsets_arr = $tplset_handler->getObjects();
                $tcount = $tplset_handler->getCount();
;
                $installed_mods = $tpltpl_handler->getModuleTplCount($tplset);

                //all templates or only one template
                if ($_REQUEST['active_templates'] == 0) {
                    foreach (array_keys($tplsets_arr) as $i) {
                        $tplsetname = $tplsets_arr[$i]->getVar('tplset_name');
                        $tplstats = $tpltpl_handler->getModuleTplCount($tplsetname);

                        if (count($tplstats) > 0) {
                            foreach ($tplstats as $moddir => $filecount) {
                                $module = $xoops->getModuleByDirname($moddir);
                                if (is_object($module)) {
                                    // create module folder
                                    if (!is_dir($theme_surcharge . '/' . $module->getVar('dirname'))) {
                                        mkdir($theme_surcharge . '/' . $module->getVar('dirname'), 0777);
                                        chmod($theme_surcharge . '/' . $module->getVar('dirname'), 0777);
                                        copy($indexFile, $theme_surcharge . '/' . $module->getVar('dirname') . '/index.html');
                                    }

                                    // create block folder
                                    if (!is_dir($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks')) {
                                        if (!is_dir($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks')) {
                                            mkdir($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks', 0777);
                                        }
                                        chmod($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks', 0777);
                                        copy($indexFile, $theme_surcharge . '/' . $module->getVar('dirname') . '/blocks' . '/index.html');
                                    }

                                    $class = "odd";
                                    $text .= '<table cellspacing="1" class="outer"><tr><th colspan="3" align="center">' . _AM_SYSTEM_TEMPLATES_MODULES . ucfirst($module->getVar('dirname')) . '</th></tr><tr><th align="center">' . _AM_SYSTEM_TEMPLATES_TYPES . '</th><th  align="center">' . _AM_SYSTEM_TEMPLATES_FILES . '</th><th>' . _AM_SYSTEM_TEMPLATES_STATUS . '</th></tr>';


                                    // create template
                                    $templates = $tpltpl_handler->find($tplsetname, 'module', null, $moddir);
                                    for ($j = 0; $j < count($templates); $j++) {
                                        $filename = $templates[$j]->getVar('tpl_file');
                                        if ($tplsetname == $tplset) {
                                            $physical_file = XOOPS_THEME_PATH . '/' . $_REQUEST['select_theme'] . '/modules/' . $moddir . '/' . $filename;

                                            $tplfile = $tpltpl_handler->get($templates[$j]->getVar('tpl_id'), true);

                                            if (is_object($tplfile)) {
                                                if (!XoopsLoad::fileExists($physical_file) || $_REQUEST['force_generated'] == 1) {
                                                    $open = fopen("" . $physical_file . "", "w+");
                                                    if (fwrite($open, "" . html_entity_decode($tplfile->getVar('tpl_source', 'E'), ENT_QUOTES))) {
                                                        $text .= '<tr class="' . $class . '"><td align="center">' . _AM_SYSTEM_TEMPLATES_TEMPLATES . '</td><td>' . $physical_file . '</td><td align="center">';
                                                        if (XoopsLoad::fileExists($physical_file)) {
                                                            $text .= '<img width="16" src="' . system_AdminIcons('success.png') . '" /></td></tr>';
                                                        } else {
                                                            $text .= '<img width="16" src="' . system_AdminIcons('cancel.png') . '" /></td></tr>';
                                                        }
                                                        $verif_write = true;
                                                    }
                                                    fclose($open);
                                                    $class = ($class == "even") ? "odd" : "even";
                                                }
                                            }
                                        }
                                    }

                                    // create block template
                                    $btemplates = $tpltpl_handler->find($tplsetname, 'block', null, $moddir);
                                    for ($k = 0; $k < count($btemplates); $k++) {
                                        $filename = $btemplates[$k]->getVar('tpl_file');
                                        if ($tplsetname == $tplset) {
                                            $physical_file = XOOPS_THEME_PATH . '/' . $_REQUEST['select_theme'] . '/modules/' . $moddir . '/blocks/' . $filename;
                                            $btplfile = $tpltpl_handler->get($btemplates[$k]->getVar('tpl_id'), true);

                                            if (is_object($btplfile)) {
                                                if (!XoopsLoad::fileExists($physical_file) || $_REQUEST['force_generated'] == 1) {
                                                    $open = fopen("" . $physical_file . "", "w+");
                                                    if (fwrite($open, "" . utf8_encode(html_entity_decode($btplfile->getVar('tpl_source', 'E'))) . "")) {
                                                        $text .= '<tr class="' . $class . '"><td align="center">' . _AM_SYSTEM_TEMPLATES_BLOCKS . '</td><td>' . $physical_file . '</td><td align="center">';
                                                        if (XoopsLoad::fileExists($physical_file)) {
                                                            $text .= '<img width="16" src="' . system_AdminIcons('success.png') . '" /></td></tr>';
                                                        } else {
                                                            $text .= '<img width="16" src="' . system_AdminIcons('cancel.png') . '" /></td></tr>';
                                                        }
                                                        $verif_write = true;
                                                    }
                                                    fclose($open);
                                                    $class = ($class == "even") ? "odd" : "even";
                                                }
                                            }
                                        }
                                    }
                                    $text .= '</table>';
                                }
                            }
                            unset($module);
                        }
                    }
                } else {
                    foreach (array_keys($tplsets_arr) as $i) {
                        $tplsetname = $tplsets_arr[$i]->getVar('tplset_name');
                        $tplstats = $tpltpl_handler->getModuleTplCount($tplsetname);

                        if (count($tplstats) > 0) {
                            $moddir = $_REQUEST['select_modules'];
                            $module = $xoops->getModuleByDirname($moddir);
                            if (is_object($module)) {
                                // create module folder
                                if (!is_dir($theme_surcharge . '/' . $module->getVar('dirname'))) {
                                    mkdir($theme_surcharge . '/' . $module->getVar('dirname'), 0777);
                                    chmod($theme_surcharge . '/' . $module->getVar('dirname'), 0777);
                                    copy($indexFile, $theme_surcharge . '/' . $module->getVar('dirname') . '/index.html');
                                }

                                // create block folder
                                if (!is_dir($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks')) {
                                    if (!is_dir($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks')) {
                                        mkdir($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks', 0777);
                                    }
                                    chmod($theme_surcharge . '/' . $module->getVar('dirname') . '/blocks', 0777);
                                    copy($indexFile, $theme_surcharge . '/' . $module->getVar('dirname') . '/blocks' . '/index.html');
                                }

                                $class = "odd";
                                $text .= '<table cellspacing="1" class="outer"><tr><th colspan="3" align="center">' . _AM_SYSTEM_TEMPLATES_MODULES . ucfirst($module->getVar('dirname')) . '</th></tr><tr><th align="center">' . _AM_SYSTEM_TEMPLATES_TYPES . '</th><th  align="center">' . _AM_SYSTEM_TEMPLATES_FILES . '</th><th>' . _AM_SYSTEM_TEMPLATES_STATUS . '</th></tr>';
                                $select_templates_modules = $_REQUEST['select_templates_modules'];
                                for ($l = 0; $l < count($_REQUEST['select_templates_modules']); $l++) {
                                    // create template
                                    $templates = $tpltpl_handler->find($tplsetname, 'module', null, $moddir);
                                    for ($j = 0; $j < count($templates); $j++) {
                                        $filename = $templates[$j]->getVar('tpl_file');
                                        if ($tplsetname == $tplset) {
                                            $physical_file = XOOPS_THEME_PATH . '/' . $_REQUEST['select_theme'] . '/modules/' . $moddir . '/' . $filename;

                                            $tplfile = $tpltpl_handler->get($templates[$j]->getVar('tpl_id'), true);

                                            if (is_object($tplfile)) {
                                                if (!XoopsLoad::fileExists($physical_file) || $_REQUEST['force_generated'] == 1) {
                                                    if ($select_templates_modules[$l] == $filename) {
                                                        $open = fopen("" . $physical_file . "", "w+");
                                                        if (fwrite($open, "" . html_entity_decode($tplfile->getVar('tpl_source', 'E'), ENT_QUOTES))) {
                                                            $text .= '<tr class="' . $class . '"><td align="center">' . _AM_SYSTEM_TEMPLATES_TEMPLATES . '</td><td>' . $physical_file . '</td><td align="center">';
                                                            if (XoopsLoad::fileExists($physical_file)) {
                                                                $text .= '<img width="16" src="' . system_AdminIcons('success.png') . '" /></td></tr>';
                                                            } else {
                                                                $text .= '<img width="16" src="' . system_AdminIcons('cancel.png') . '" /></td></tr>';
                                                            }
                                                            $verif_write = true;
                                                        }
                                                        fclose($open);
                                                    }
                                                    $class = ($class == "even") ? "odd" : "even";
                                                }
                                            }
                                        }
                                    }

                                    // create block template
                                    $btemplates = $tpltpl_handler->find($tplsetname, 'block', null, $moddir);
                                    for ($k = 0; $k < count($btemplates); $k++) {
                                        $filename = $btemplates[$k]->getVar('tpl_file');
                                        if ($tplsetname == $tplset) {
                                            $physical_file = XOOPS_THEME_PATH . '/' . $_REQUEST['select_theme'] . '/modules/' . $moddir . '/blocks/' . $filename;
                                            $btplfile = $tpltpl_handler->get($btemplates[$k]->getVar('tpl_id'), true);

                                            if (is_object($btplfile)) {
                                                if (!XoopsLoad::fileExists($physical_file) || $_REQUEST['force_generated'] == 1) {
                                                    if ($select_templates_modules[$l] == $filename) {
                                                        $open = fopen("" . $physical_file . "", "w+");
                                                        if (fwrite($open, "" . utf8_encode(html_entity_decode($btplfile->getVar('tpl_source', 'E'))) . "")) {
                                                            $text .= '<tr class="' . $class . '"><td align="center">' . _AM_SYSTEM_TEMPLATES_BLOCKS . '</td><td>' . $physical_file . '</td><td align="center">';
                                                            if (XoopsLoad::fileExists($physical_file)) {
                                                                $text .= '<img width="16" src="' . system_AdminIcons('success.png') . '" /></td></tr>';
                                                            } else {
                                                                $text .= '<img width="16" src="' . system_AdminIcons('cancel.png') . '" /></td></tr>';
                                                            }
                                                            $verif_write = true;
                                                        }
                                                        fclose($open);
                                                    }
                                                    $class = ($class == "even") ? "odd" : "even";
                                                }
                                            }
                                        }
                                    }
                                }
                                $text .= '</table>';
                            }
                            unset($module);
                        }
                    }
                }
                $xoops->tpl()->assign('infos', $text);
                $xoops->tpl()->assign('verif', $verif_write);
            } else {
                $xoops->redirect("admin.php?fct=tplsets", 2, _AM_SYSTEM_TEMPLATES_SAVE);
            }
        } else {
            // Generate one module
            $xoops->tpl()->assign('index', true);

            $tplset = $system->cleanVars($POST, 'tplset', 'default', 'string');

            $form = new XoopsThemeForm(_AM_SYSTEM_TEMPLATES_SELECT_TEMPLATES, "form", 'admin.php?fct=tplsets', "post", true);

            $tpltpl_handler = $xoops->getHandlerTplfile();
            $templates_arr = $tpltpl_handler->find($tplset, '', null, $_REQUEST['select_modules']);

            $modules = new XoopsFormSelect(_AM_SYSTEM_TEMPLATES_SELECT_TEMPLATES, 'select_templates_modules', null, 10, true);
            foreach (array_keys($templates_arr) as $i) {
                $modules->addOption($templates_arr[$i]->getVar('tpl_file'));
            }
            $form->addElement($modules);

            $form->addElement(new XoopsFormHidden("active_templates", "1"));
            $form->addElement(new XoopsFormHidden("force_generated", $_REQUEST['force_generated']));
            $form->addElement(new XoopsFormHidden("select_modules", $_REQUEST['select_modules']));
            $form->addElement(new XoopsFormHidden("active_modules", "1"));
            $form->addElement(new XoopsFormHidden("select_theme", $_REQUEST['select_theme']));
            $form->addElement(new XoopsFormHidden("op", "tpls_generate_surcharge"));
            $form->addElement(new XoopsFormButton("", "submit", _SUBMIT, "submit"));
            $xoops->tpl()->assign('form', $form->render());
        }
        break;

    // save
    case 'tpls_save':
        $path_file = $_REQUEST['path_file'];
        if (isset($path_file)) {
            // copy file
            $copy_file = $path_file;
            copy($copy_file, $path_file . '.back');
            // Save modif
            if (isset($_REQUEST['templates'])) {
                $open = fopen("" . $path_file . "", "w+");
                if (!fwrite($open, utf8_encode(stripslashes($_REQUEST['templates'])))) {
                    $xoops->redirect("admin.php?fct=tplsets", 2, _AM_SYSTEM_TEMPLATES_ERROR);
                }
                fclose($open);
            }
        }
        $xoops->redirect("admin.php?fct=tplsets", 2, _AM_SYSTEM_TEMPLATES_SAVE);
        break;
}
// Call Footer
$xoops->footer();
