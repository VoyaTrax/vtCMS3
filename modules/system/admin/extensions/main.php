<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Plugins Manager
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author          Andricq Nicolas (AKA MusS)
 * @package         system
 * @version         $Id: main.php 10419 2012-12-16 19:57:47Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

// Get main instance
$xoops = Xoops::getInstance();
$system = System::getInstance();

// Check users rights
if (!$xoops->isUser() || !$xoops->isModule() || !$xoops->user->isAdmin($xoops->module->mid())) {
    exit(_NOPERM);
}

if (isset($_POST)) {
    foreach ($_POST as $k => $v) {
        ${$k} = $v;
    }
}
$system->loadLanguage('modulesadmin','system');

// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'list', 'string');
$module = $system->cleanVars($_REQUEST, 'module', '', 'string');

if (in_array($op, array('install', 'update', 'uninstall'))) {
    if (!$xoops->security()->check()) {
        $op = 'list';
    }
}
$myts = MyTextsanitizer::getInstance();

switch ($op) {

    case 'list':
        // Call Header
        $xoops->header('system_extensions.html');
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        // Define scripts
        $xoops->theme()->addScript('media/jquery/plugins/jquery.jeditable.js');
        $xoops->theme()->addScript('modules/system/js/admin.js');
        $xoops->theme()->addScript('modules/system/js/module.js');
        // Define Breadcrumb and tips
        $admin_page = new XoopsModuleAdmin();
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_CPANEL, XOOPS_URL . '/admin.php', true);
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_ADMIN, $system->adminVersion('extensions', 'adminpath'));
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_MAIN);
        $admin_page->addTips(_AM_SYSTEM_EXTENSIONS_TIPS);
        $admin_page->renderBreadcrumb();
        $admin_page->renderTips();

        $system_extension = new SystemExtension();

        $extension = $system_extension->getExtensionList();

        $xoops->tpl()->assign('xoops', $xoops);
        $xoops->tpl()->assign('extension_list', $extension);
        // Call Footer
        $xoops->footer();
        break;

    case 'install':
        $module = $system->cleanVars($_POST, 'dirname', '', 'string');
        // Call Header
        $xoops->header('system_modules_logger.html');
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        // Define Breadcrumb and tips
        $admin_page = new XoopsModuleAdmin();
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_CPANEL, XOOPS_URL . '/admin.php', true);
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_ADMIN, $system->adminVersion('extensions', 'adminpath'));
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_INSTALL);
        $admin_page->renderBreadcrumb();

        $ret = array();
        $system_extension = new SystemExtension();
        $ret = $system_extension->install($module);
        if ($ret) {
            $xoops->tpl()->assign('install', 1);
            $xoops->tpl()->assign('module', $ret);
            $xoops->tpl()->assign('from_title', _AM_SYSTEM_EXTENSIONS_ADMIN);
            $xoops->tpl()->assign('from_link', $system->adminVersion('extensions', 'adminpath'));
            $xoops->tpl()->assign('title', _AM_SYSTEM_EXTENSIONS_INSTALL);
            $xoops->tpl()->assign('log', $system_extension->trace);
        } else {
            print_r($system_extension->error);
            //print_r($system_extension->trace);
        }
        //Set active modules in cache folder
        $xoops->setActiveModules();
        // Call Footer
        $xoops->footer();
        break;

    case 'uninstall':
        $mid = $system->cleanVars($_POST, 'mid', 0, 'int');
        $module_handler = $xoops->getHandlerModule();
        $module = $module_handler->getById($mid);
        // Call Header
        $xoops->header('system_modules_logger.html');
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        // Define Breadcrumb and tips
        $admin_page = new XoopsModuleAdmin();
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_CPANEL, XOOPS_URL . '/admin.php', true);
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_ADMIN, $system->adminVersion('extensions', 'adminpath'));
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_UNINSTALL);
        $admin_page->renderBreadcrumb();

        $ret = array();
        $system_extension = new SystemExtension();
        $ret = $system_extension->uninstall($module->getVar('dirname'));
        $xoops->tpl()->assign('module', $ret);
        if ($ret) {
            $xoops->tpl()->assign('from_title', _AM_SYSTEM_EXTENSIONS_ADMIN);
            $xoops->tpl()->assign('from_link', $system->adminVersion('extensions', 'adminpath'));
            $xoops->tpl()->assign('title', _AM_SYSTEM_EXTENSIONS_UNINSTALL);
            $xoops->tpl()->assign('log', $system_extension->trace);
        }
        $folder = array(1, 3);
        $system->CleanCache($folder);
         //Set active modules in cache folder
        $xoops->setActiveModules();
        // Call Footer
        $xoops->footer();
        break;

    case 'update':
        $mid = $system->cleanVars($_POST, 'mid', 0, 'int');
        $module_handler = $xoops->getHandlerModule();
        $block_handler = $xoops->getHandlerBlock();
        $module = $module_handler->getById($mid);
        // Call Header
        $xoops->header('system_modules_logger.html');
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        // Define Breadcrumb and tips
        $admin_page = new XoopsModuleAdmin();
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_CPANEL, XOOPS_URL . '/admin.php', true);
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_ADMIN, $system->adminVersion('extensions', 'adminpath'));
        $admin_page->addBreadcrumbLink(_AM_SYSTEM_EXTENSIONS_UPDATE);
        $admin_page->renderBreadcrumb();

        $ret = array();
        $system_extension = new SystemExtension();
        $ret = $system_extension->update($module->getVar('dirname'));
        $xoops->tpl()->assign('module', $ret);
        if ($ret) {
            $xoops->tpl()->assign('install', 1);
            $xoops->tpl()->assign('from_title', _AM_SYSTEM_EXTENSIONS_ADMIN);
            $xoops->tpl()->assign('from_link', $system->adminVersion('extensions', 'adminpath'));
            $xoops->tpl()->assign('title', _AM_SYSTEM_EXTENSIONS_UPDATE);
            $xoops->tpl()->assign('log', $system_extension->trace);
        }
        $folder = array(1, 3);
        $system->CleanCache($folder);
        //Set active modules in cache folder
        $xoops->setActiveModules();
        // Call Footer
        $xoops->footer();
        break;
}