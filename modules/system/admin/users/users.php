<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Users Manager
 *
 * @copyright   The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license     GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @author      Kazumi Ono (AKA onokazu)
 * @package     system
 * @version     $Id: users.php 10605 2012-12-29 14:19:09Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

// Get main instance
$xoops = Xoops::getInstance();

// Check users rights
if (!$xoops->isUser() || !$xoops->isModule() || !$xoops->user->isAdmin($xoops->module->mid())) {
    exit(_NOPERM);
}

//  Check is active
if (!$xoops->getModuleConfig('active_users', 'system')) {
    $xoops->redirect('admin.php', 2, _AM_SYSTEM_NOTACTIVE);
}

/*********************************************************/
/* Users Functions                                       */
/*********************************************************/
/**
 * @param int $uid
 * @param string $type
 * @return void
 */
function synchronize($uid, $type)
{
    $xoops = Xoops::getInstance();

    switch ($type) {
        case 'user':
            $total_posts = 0;
            /* @var $plugin SystemPluginInterface */
            $plugins = Xoops_Module_Plugin::getPlugins();
            foreach ($plugins as $plugin) {
                if ($res = $plugin->userPosts($uid)){
                    $total_posts += $res;
                }
            }

            $sql = "UPDATE " . $xoops->db()->prefix("users") . " SET posts = '" . (int) $total_posts . "' WHERE uid = '" . $uid . "'";
            if (!$xoops->db()->queryF($sql)) {
                $xoops->redirect("admin.php?fct=users", 1, _AM_SYSTEM_USERS_CNUUSER);
            }
            break;

        case 'all users':
            $sql = "SELECT uid FROM " . $xoops->db()->prefix("users") . "";
            if (!$result = $xoops->db()->query($sql)) {
                $xoops->redirect("admin.php?fct=users", 1, sprintf(_AM_SYSTEM_USERS_CNGUSERID, $uid));
            }

            while ($data = $xoops->db()->fetchArray($result)) {
                synchronize($data['uid'], "user");
            }
            break;
    }
}