<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Extended User Profile
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         profile
 * @since           2.3.0
 * @author          Jan Pedersen
 * @author          Taiwen Jiang <phppp@users.sourceforge.net>
 * @version         $Id: activate.php 10408 2012-12-16 18:43:15Z trabis $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'header.php';
$xoops = Xoops::getInstance();
$xoops->loadLanguage('admin');

$xoops->header();
if (!empty($_GET['id']) && !empty($_GET['actkey'])) {
    $id = intval($_GET['id']);
    $actkey = trim($_GET['actkey']);
    if (empty($id)) {
        $xoops->redirect(XOOPS_URL, 1, '');
        exit();
    }
    $member_handler = $xoops->getHandlerMember();
    $thisuser = $member_handler->getUser($id);
    if (!is_object($thisuser)) {
        $xoops->redirect(XOOPS_URL, 1, '');
    }
    if ($thisuser->getVar('actkey') != $actkey) {
        $xoops->redirect(XOOPS_URL . '/', 5, _US_ACTKEYNOT);
    } else {
        if ($thisuser->getVar('level') > 0) {
            $xoops->redirect(XOOPS_URL . '/modules/' . $xoops->module->getVar('dirname', 'n'). '/index.php', 5, _US_ACONTACT, false);
        } else {
            if (false != $member_handler->activateUser($thisuser)) {
                $xoops->getConfigs();
                if ($xoops->getConfig('activation_type') == 2) {
                    $myts = MyTextSanitizer::getInstance();
                    $xoopsMailer = $xoops->getMailer();
                    $xoopsMailer->useMail();
                    $xoopsMailer->setTemplate('activated.tpl');
                    $xoopsMailer->assign('SITENAME', $xoops->getConfig('sitename'));
                    $xoopsMailer->assign('ADMINMAIL', $xoops->getConfig('adminmail'));
                    $xoopsMailer->assign('SITEURL', XOOPS_URL . "/");
                    $xoopsMailer->setToUsers($thisuser);
                    $xoopsMailer->setFromEmail($xoops->getConfig('adminmail'));
                    $xoopsMailer->setFromName($xoops->getConfig('sitename'));
                    $xoopsMailer->setSubject(sprintf(_US_YOURACCOUNT, $xoops->getConfig('sitename')));
                    $xoops->footer();
                    if (!$xoopsMailer->send()) {
                        printf(_US_ACTVMAILNG, $thisuser->getVar('uname') );
                    } else {
                        printf(_US_ACTVMAILOK, $thisuser->getVar('uname') );
                    }
                    include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'footer.php';
                } else {
                    $xoops->redirect(XOOPS_URL . '/user.php', 5, _US_ACTLOGIN, false);
                }
            } else {
                $xoops->redirect(XOOPS_URL . '/index.php', 5, 'Activation failed!');
            }
        }
    }
// Not implemented yet: re-send activiation code
} else if (!empty($_REQUEST['email']) && $xoops->getConfig('activation_type') != 0) {
    $myts = MyTextSanitizer::getInstance();
    $member_handler = $xoops->getHandlerMember();
    $getuser = $member_handler->getUsers(new Criteria('email', $myts->addSlashes(trim($_REQUEST['email']))));
    if (count($getuser) == 0) {
        $xoops->redirect(XOOPS_URL, 2, _US_SORRYNOTFOUND);
    }
    /* @var XoopsUser $getuser */
    $getuser = $getuser[0];
    if ($getuser->isActive()) {
        $xoops->redirect(XOOPS_URL, 2, sprintf(_US_ACONTACT, $getuser->getVar('email')));
    }
    $xoopsMailer = $xoops->getMailer();
    $xoopsMailer->useMail();
    $xoopsMailer->setTemplate('register.tpl');
    $xoopsMailer->assign('SITENAME', $xoops->getConfig('sitename'));
    $xoopsMailer->assign('ADMINMAIL', $xoops->getConfig('adminmail'));
    $xoopsMailer->assign('SITEURL', XOOPS_URL . "/");
    $xoopsMailer->setToUsers($getuser[0]);
    $xoopsMailer->setFromEmail($xoops->getConfig('adminmail'));
    $xoopsMailer->setFromName($xoops->getConfig('sitename'));
    $xoopsMailer->setSubject(sprintf(_US_USERKEYFOR, $getuser->getVar('uname') ));
    if (!$xoopsMailer->send()) {
        echo _US_YOURREGMAILNG;
    } else {
        echo _US_YOURREGISTERED;
    }
} else {
    $form = new XoopsThemeForm('', 'form', 'activate.php');
    $form->addElement(new XoopsFormText(_US_EMAIL, 'email', 25, 255) );
    $form->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit') );
    $form->display();
}

$xoops->appendConfig('profile_breadcrumbs', array('title' => _PROFILE_MA_REGISTER));
include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'footer.php';