<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Extended User Profile
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         profile
 * @since           2.3.0
 * @author          Jan Pedersen
 * @author          Taiwen Jiang <phppp@users.sourceforge.net>
 * @version         $Id: lostpass.php 10328 2012-12-07 00:56:07Z trabis $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'header.php';
$xoops = Xoops::getInstance();
$email = isset($_GET['email']) ? trim($_GET['email']) : '';
$email = isset($_POST['email']) ? trim($_POST['email']) : $email;

$xoops->loadLanguage('user');

if ($email == '') {
    $xoops->redirect("user.php", 2, _US_SORRYNOTFOUND, false);
}

$myts = MyTextSanitizer::getInstance();
$member_handler = $xoops->getHandlerMember();
/* @var $user XoopsUser */
list($user) = $member_handler->getUsers(new Criteria('email', $myts->addSlashes($email)));

if (empty($user)) {
    $msg = _US_SORRYNOTFOUND;
    $xoops->redirect("user.php", 2, $msg, false);
} else {
    $code = isset($_GET['code']) ? trim($_GET['code']) : '';
    $areyou = substr($user->getVar("pass"), 0, 5);
    if ($code != '' && $areyou == $code) {
        $newpass = $xoops->makePass();
        $xoopsMailer = $xoops->getMailer();
        $xoopsMailer->useMail();
        $xoopsMailer->setTemplate("lostpass2.tpl");
        $xoopsMailer->assign("SITENAME", $xoops->getConfig('sitename'));
        $xoopsMailer->assign("ADMINMAIL", $xoops->getConfig('adminmail'));
        $xoopsMailer->assign("SITEURL", XOOPS_URL . "/");
        $xoopsMailer->assign("IP", $_SERVER['REMOTE_ADDR']);
        $xoopsMailer->assign("NEWPWD", $newpass);
        $xoopsMailer->setToUsers($user);
        $xoopsMailer->setFromEmail($xoops->getConfig('adminmail'));
        $xoopsMailer->setFromName($xoops->getConfig('sitename'));
        $xoopsMailer->setSubject(sprintf(_US_NEWPWDREQ, XOOPS_URL));
        if (!$xoopsMailer->send()) {
            echo $xoopsMailer->getErrors();
        }

        // Next step: add the new password to the database
        $sql = sprintf("UPDATE %s SET pass = '%s' WHERE uid = %u", $xoops->db()->prefix("users"), md5($newpass), $user->getVar('uid'));
        if (!$xoops->db()->queryF($sql)) {
            $xoops->header();
            echo _US_MAILPWDNG;
            include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'footer.php';
        }
        $xoops->redirect("user.php", 3, sprintf(_US_PWDMAILED, $user->getVar("uname")), false);
        // If no Code, send it
    } else {
        $xoopsMailer = $xoops->getMailer();
        $xoopsMailer->useMail();
        $xoopsMailer->setTemplate("lostpass1.tpl");
        $xoopsMailer->assign("SITENAME", $xoops->getConfig('sitename'));
        $xoopsMailer->assign("ADMINMAIL", $xoops->getConfig('adminmail'));
        $xoopsMailer->assign("SITEURL", XOOPS_URL . "/");
        $xoopsMailer->assign("IP", $_SERVER['REMOTE_ADDR']);
        $xoopsMailer->assign("NEWPWD_LINK", XOOPS_URL . "/modules/profile/lostpass.php?email={$email}&code=" . $areyou);
        $xoopsMailer->setToUsers($user);
        $xoopsMailer->setFromEmail($xoops->getConfig('adminmail'));
        $xoopsMailer->setFromName($xoops->getConfig('sitename'));
        $xoopsMailer->setSubject(sprintf(_US_NEWPWDREQ, $xoops->getConfig('sitename')));
        include $xoops->path('header.php');
        if (!$xoopsMailer->send()) {
            echo $xoopsMailer->getErrors();
        }
        echo "<h4>";
        printf(_US_CONFMAIL, $user->getVar('uname'));
        echo "</h4>";
        include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'footer.php';
    }
}