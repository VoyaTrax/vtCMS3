<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Extended User Profile
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         profile
 * @since           2.3.0
 * @author          Jan Pedersen
 * @author          Taiwen Jiang <phppp@users.sourceforge.net>
 * @version         $Id: edituser.php 10764 2013-01-11 19:25:11Z trabis $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'header.php';
$xoops = Xoops::getInstance();
$xoops->loadLanguage('user');

// If not a user, redirect
if (!$xoops->isUser()) {
    $xoops->redirect(XOOPS_URL, 3, _US_NOEDITRIGHT);
}

$myts = MyTextSanitizer::getInstance();
$op = isset($_REQUEST['op']) ? $_REQUEST['op'] : 'editprofile';
$xoops->getConfigs();

if ($op == 'save') {
    if (!$xoops->security()->check()) {
        $xoops->redirect(XOOPS_URL . "/modules/" . $xoops->module->getVar('dirname', 'n') . "/", 3, _US_NOEDITRIGHT . "<br />" . implode('<br />', $xoops->security()->getErrors()));
        exit();
    }
    $uid = $xoops->user->getVar('uid');
    $errors = array();
    $edituser = $xoops->user;
    if ($xoops->user->isAdmin()) {
        $edituser->setVar('uname', trim($_POST['uname']));
        $edituser->setVar('email', trim($_POST['email']));
    }
    $stop = XoopsUserUtility::validate($edituser);

    if (!empty($stop)) {
        $op = 'editprofile';
    } else {

        // Dynamic fields
        /* @var $profile_handler ProfileProfileHandler */
        $profile_handler = $xoops->getModuleHandler('profile');
        // Get fields
        $fields = $profile_handler->loadFields();
        // Get ids of fields that can be edited
        $gperm_handler = $xoops->getHandlerGroupperm();
        $editable_fields = $gperm_handler->getItemIds('profile_edit', $xoops->user->getGroups(), $xoops->module->getVar('mid'));

        if (!$profile = $profile_handler->getProfile($edituser->getVar('uid'))) {
            $profile = $profile_handler->create();
            $profile->setVar('profile_id', $edituser->getVar('uid'));
        }

        /* @var ProfileField $field */
        foreach ($fields as $field) {
            $fieldname = $field->getVar('field_name');
            if (in_array($field->getVar('field_id'), $editable_fields) && isset($_REQUEST[$fieldname])) {
                $value = $field->getValueForSave($_REQUEST[$fieldname]);
                if (in_array($fieldname, $profile_handler->getUserVars())) {
                    $edituser->setVar($fieldname, $value);
                } else {
                    $profile->setVar($fieldname, $value);
                }
            }
        }
        if (!$member_handler->insertUser($edituser)) {
            $stop = $edituser->getHtmlErrors();
            $op = 'editprofile';
        } else {
            $profile->setVar('profile_id', $edituser->getVar('uid'));
            $profile_handler->insert($profile);
            unset($_SESSION['xoopsUserTheme']);
            $xoops->redirect(XOOPS_URL . '/modules/' . $xoops->module->getVar('dirname', 'n') . '/userinfo.php?uid=' . $edituser->getVar('uid'), 2, _US_PROFUPDATED);
        }
    }
}


if ($op == 'editprofile') {
    $xoops->header('profile_editprofile.html');
    include_once dirname(__FILE__) . '/include/forms.php';
    $form = profile_getUserForm($xoops->user);
    $form->assign($xoops->tpl());
    if (!empty($stop)) {
        $xoops->tpl()->assign('stop', $stop);
    }

    $xoops->appendConfig('profile_breadcrumbs', array('title' => _US_EDITPROFILE));
}

if ($op == 'avatarform') {
    if (!$xoops->isActiveModule('avatars')) {
        $xoops->redirect('index.php', 3, _NOPERM);
    }
    $helper = Avatars::getInstance();
    $xoops->header('profile_avatar.html');
    $xoops->appendConfig('profile_breadcrumbs', array('title' => _US_MYAVATAR));

    $oldavatar = $xoops->user->getVar('user_avatar');
    if (!empty($oldavatar) && $oldavatar != 'blank.gif') {
        $xoops->tpl()->assign('old_avatar', XOOPS_UPLOAD_URL . '/' . $oldavatar);
    }
    if ($xoops->getModuleConfig('avatars_allowupload', 'avatars') == 1 && $xoops->user->getVar('posts') >= $xoops->getModuleConfig('avatars_postsrequired', 'avatars')) {
        $form = new XoopsThemeForm(_US_UPLOADMYAVATAR, 'uploadavatar', XOOPS_URL . '/modules/' . $xoops->module->getVar('dirname', 'n') . '/edituser.php', 'post', true);
        $form->setExtra('enctype="multipart/form-data"');
        $form->addElement(new XoopsFormLabel(_US_MAXPIXEL, $xoops->getModuleConfig('avatars_imagewidth', 'avatars') . ' x ' . $xoops->getModuleConfig('avatars_imageheight', 'avatars')));
        $form->addElement(new XoopsFormLabel(_US_MAXIMGSZ, $xoops->getModuleConfig('avatars_imagefilesize', 'avatars')));
        $form->addElement(new XoopsFormFile(_US_SELFILE, 'avatarfile', $xoops->getModuleConfig('avatars_imagefilesize', 'avatars')), true);
        $form->addElement(new XoopsFormHidden('op', 'avatarupload'));
        $form->addElement(new XoopsFormHidden('uid', $xoops->user->getVar('uid')));
        $form->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
        $form->assign($xoops->tpl());
    }
    $avatar_handler = $helper->getHandlerAvatar();
    $form2 = new XoopsThemeForm(_US_CHOOSEAVT, 'chooseavatar', XOOPS_URL . '/modules/' . $xoops->module->getVar('dirname', 'n') . '/edituser.php', 'post', true);
    $avatar_select = new XoopsFormSelect('', 'user_avatar', $xoops->user->getVar('user_avatar'));
    $avatar_list = $avatar_handler->getListByType('S', true);
    $avatar_selected = $xoops->user->getVar("user_avatar", "E");
    $avatar_selected = in_array($avatar_selected, array_keys($avatar_list)) ? $avatar_selected : "blank.gif";
    $avatar_select->addOptionArray($avatar_list);
    $avatar_select->setExtra("onchange='showImgSelected(\"avatar\", \"user_avatar\", \"uploads\", \"\", \"" . XOOPS_URL . "\")'");
    $avatar_tray = new XoopsFormElementTray(_US_AVATAR, '&nbsp;');
    $avatar_tray->addElement($avatar_select);
    $avatar_tray->addElement(new XoopsFormLabel('', "<a href=\"javascript:openWithSelfMain('" . XOOPS_URL . "/modules/avatars/popup.php','avatars',600,400);\">" . _LIST . "</a><br />"));
    $avatar_tray->addElement(new XoopsFormLabel('', "<br /><img src='" . XOOPS_UPLOAD_URL . "/" . $avatar_selected . "' name='avatar' id='avatar' alt='' />"));
    $form2->addElement($avatar_tray);
    $form2->addElement(new XoopsFormHidden('uid', $xoops->user->getVar('uid')));
    $form2->addElement(new XoopsFormHidden('op', 'avatarchoose'));
    $form2->addElement(new XoopsFormButton('', 'submit2', _SUBMIT, 'submit'));
    $form2->assign($xoops->tpl());
}

if ($op == 'avatarupload') {
    if (!$xoops->isActiveModule('avatars')) {
        $xoops->redirect('index.php', 3, _NOPERM);
    }
    if (!$xoops->security()->check()) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT . "<br />" . implode('<br />', $xoops->security()->getErrors()));
    }
    $helper = Avatars::getInstance();
    $xoops_upload_file = array();
    $uid = 0;
    if (!empty($_POST['xoops_upload_file']) && is_array($_POST['xoops_upload_file'])) {
        $xoops_upload_file = $_POST['xoops_upload_file'];
    }
    if (!empty($_POST['uid'])) {
        $uid = intval($_POST['uid']);
    }
    if (empty($uid) || $xoops->user->getVar('uid') != $uid) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
    }
    if ( $xoops->getModuleConfig('avatars_allowupload', 'avatars') == 1 && $xoops->user->getVar('posts') >= $xoops->getModuleConfig('avatars_postsrequired', 'avatars') ) {
        $uploader = new XoopsMediaUploader(XOOPS_UPLOAD_PATH . '/avatars', array(
                                                                                'image/gif', 'image/jpeg',
                                                                                'image/pjpeg', 'image/x-png',
                                                                                'image/png'
                                                                           ), $xoops->getModuleConfig('avatars_imagefilesize', 'avatars'), $xoops->getModuleConfig('avatars_imagewidth', 'avatars'), $xoops->getModuleConfig('avatars_imageheight', 'avatars'));
        if ($uploader->fetchMedia($_POST['xoops_upload_file'][0])) {
            $uploader->setPrefix('cavt');
            if ($uploader->upload()) {
                $avatar_handler = $helper->getHandlerAvatar();
                $avatar = $avatar_handler->create();
                $avatar->setVar('avatar_file', 'avatars/' . $uploader->getSavedFileName());
                $avatar->setVar('avatar_name', $xoops->user->getVar('uname'));
                $avatar->setVar('avatar_mimetype', $uploader->getMediaType());
                $avatar->setVar('avatar_display', 1);
                $avatar->setVar('avatar_type', 'C');
                if (!$avatar_handler->insert($avatar)) {
                    @unlink($uploader->getSavedDestination());
                } else {
                    $oldavatar = $xoops->user->getVar('user_avatar');
                    if (!empty($oldavatar) && false !== strpos(strtolower($oldavatar), "cavt")) {
                        $avatars = $avatar_handler->getObjects(new Criteria('avatar_file', $oldavatar));
                        if (!empty($avatars) && count($avatars) == 1 && is_object($avatars[0])) {
                            $avatar_handler->delete($avatars[0]);
                            $oldavatar_path = realpath(XOOPS_UPLOAD_PATH . '/' . $oldavatar);
                            if (0 === strpos($oldavatar_path, XOOPS_UPLOAD_PATH) && is_file($oldavatar_path)) {
                                unlink($oldavatar_path);
                            }
                        }
                    }
                    $sql = sprintf("UPDATE %s SET user_avatar = %s WHERE uid = %u", $xoops->db()->prefix('users'), $xoops->db()->quoteString('avatars/' . $uploader->getSavedFileName()), $xoops->user->getVar('uid'));
                    $xoops->db()->query($sql);
                    $avatar_handler->addUser($avatar->getVar('avatar_id'), $xoops->user->getVar('uid'));
                    $xoops->redirect('userinfo.php?t=' . time() . '&amp;uid=' . $xoops->user->getVar('uid'), 3, _US_PROFUPDATED);
                }
            }
        }
        $xoops->redirect("edituser.php?op=avatarform", 3, $uploader->getErrors());
    }
    $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
    exit();
}

if ($op == 'avatarchoose') {
    if (!$xoops->isActiveModule('avatars')) {
        $xoops->redirect('index.php', 3, _NOPERM);
    }
    if (!$xoops->security()->check()) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT . "<br />" . implode('<br />', $xoops->security()->getErrors()));
    }
    $helper = Avatars::getInstance();
    $uid = 0;
    if (!empty($_POST['uid'])) {
        $uid = intval($_POST['uid']);
    }
    if (empty($uid) || $xoops->user->getVar('uid') != $uid) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
    }
    $user_avatar = '';
    $avatar_handler = $helper->getHandlerAvatar();
    if (!empty($_POST['user_avatar'])) {
        $user_avatar = $myts->addSlashes(trim($_POST['user_avatar']));
        $criteria_avatar = new CriteriaCompo(new Criteria('avatar_file', $user_avatar));
        $criteria_avatar->add(new Criteria('avatar_type', "S"));
        $avatars = $avatar_handler->getObjects($criteria_avatar);
        if (!is_array($avatars) || !count($avatars)) {
            $user_avatar = 'avatars/blank.gif';
        }
        unset($avatars, $criteria_avatar);
    }
    $user_avatarpath = realpath(XOOPS_UPLOAD_PATH . '/' . $user_avatar);
    if (0 === strpos($user_avatarpath, realpath(XOOPS_UPLOAD_PATH)) && is_file($user_avatarpath)) {
        $oldavatar = $xoops->user->getVar('user_avatar');
        $xoops->user->setVar('user_avatar', $user_avatar);
        $member_handler = $xoops->getHandlerMember();
        if (!$member_handler->insertUser($xoops->user)) {
            $xoops->header();
            echo $xoops->user->getHtmlErrors();
            $xoops->footer();
        }
        if ($oldavatar && preg_match("/^cavt/", strtolower(substr($oldavatar, 8)))) {
            $avatars = $avatar_handler->getObjects(new Criteria('avatar_file', $oldavatar));
            if (!empty($avatars) && count($avatars) == 1 && is_object($avatars[0])) {
                $avatar_handler->delete($avatars[0]);
                $oldavatar_path = realpath(XOOPS_UPLOAD_PATH . '/' . $oldavatar);
                if (0 === strpos($oldavatar_path, realpath(XOOPS_UPLOAD_PATH)) && is_file($oldavatar_path)) {
                    unlink($oldavatar_path);
                }
            }
        }
        if ($user_avatar != 'avatars/blank.gif') {
            $avatars = $avatar_handler->getObjects(new Criteria('avatar_file', $user_avatar));
            if (is_object($avatars[0])) {
                /* @var XoopsAvatar $avatar */
                $avatar = $avatars[0];
                $avatar_handler->addUser($avatar->getVar('avatar_id'), $xoops->user->getVar('uid'));
            }
        }
    }
    $xoops->redirect('userinfo.php?uid=' . $uid, 0, _US_PROFUPDATED);
}
include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'footer.php';