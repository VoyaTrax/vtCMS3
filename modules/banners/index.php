<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id$
 */

include dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'mainfile.php';

$xoops = Xoops::getInstance();
XoopsLoad::load('system', 'system');
// Get main instance
$system = System::getInstance();
// Get banners handler
$banner_Handler = $xoops->getModuleHandler('banner');
$client_Handler = $xoops->getModuleHandler('bannerclient');
// Get member handler
$member_handler = $xoops->getHandlerMember();
// Parameters
$nb_banners = $xoops->getModuleConfig('banners_pager');
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'list', 'string');

switch ($op) {

    case 'list':
    default:
        $access = false;
        $admin = false;
        if ($xoops->isUser()) {
            $uid = $xoops->user->getVar('uid');
        } else {
            $uid = 0;
        }
        if ($uid == 0) {
            $access = false;
        }
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('uid', $uid));
        $client_count = $client_Handler->getCount($criteria);
        if ($client_count != 0) {
            $access = true;
        }
        if ($xoops->userIsAdmin) {
            $access = true;
            $admin = true;
        }
        if ($access == false) {
            $xoops->redirect(XOOPS_URL, 2, _NOPERM);
        }
        // Get start pager
        $start = $system->cleanVars($_REQUEST, 'start', 0, 'int');
        $startF = $system->cleanVars($_REQUEST, 'startF', 0, 'int');
        // Call header
        $xoops->header('banners_client.html');
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        $xoops->theme()->addStylesheet('media/jquery/ui/' . $xoops->getModuleConfig('jquery_theme', 'system') . '/ui.all.css');
        // Define scripts
        $xoops->theme()->addScript($xoops->url('/media/jquery/jquery.js'));
        $xoops->theme()->addScript($xoops->url('/media/jquery/ui/jquery.ui.js'));
        $xoops->theme()->addScript('modules/system/js/admin.js');

        // Display banner
        if ($admin == false) {
            $client_arr = $client_Handler->getall($criteria);
            foreach (array_keys($client_arr) as $i) {
                $cid[] = $client_arr[$i]->getVar("cid");
            }
        }

        // Display banner
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('status', 0, '!='));
        if ($admin == false) {
            $criteria->add(new Criteria('cid', '(' . implode(',', $cid) . ')','IN'));
        }
        $criteria->setSort("cid");
        $criteria->setOrder("ASC");
        $criteria->setStart($start);
        $criteria->setLimit($nb_banners);

        $banner_count = $banner_Handler->getCount($criteria);
        $banner_arr = $banner_Handler->getall($criteria);

        $xoops->tpl()->assign('banner_count', $banner_count);

        if ($banner_count > 0) {
            foreach (array_keys($banner_arr) as $i) {
                $imptotal = $banner_arr[$i]->getVar("imptotal");
                $impmade = $banner_arr[$i]->getVar("impmade");
                $imageurl = $banner_arr[$i]->getVar("imageurl");
                $clicks = $banner_arr[$i]->getVar("clicks");
                $htmlbanner = $banner_arr[$i]->getVar("htmlbanner");
                $htmlcode = $banner_arr[$i]->getVar("htmlcode");
                $client = $client_Handler->get($banner_arr[$i]->getVar("cid"));
                if (is_object($client)) {
                    $client_name = $client->getVar("name");
                    $client_uid = $client->getVar("uid");
                } else {
                    $client_name = '';
                    $client_uid = 0;
                }
                if ($impmade == 0) {
                    $percent = 0;
                } else {
                    $percent = substr(100 * $clicks / $impmade, 0, 5);
                }
                if ($imptotal == 0) {
                    $left = "" . _AM_BANNERS_BANNERS_UNLIMIT . "";
                } else {
                    $left = $imptotal - $impmade;
                }
                $img = '';
                if ($htmlbanner) {
                    $img .= html_entity_decode($htmlcode);
                } else {
                    $img = '';
                    if (stristr($imageurl, '.swf')) {
                        $img .= '<object type="application/x-shockwave-flash" width="468" height="60" data="' . $imageurl . '" style="z-index:100;">' . '<param name="movie" value="' . $imageurl . '" />' . '<param name="wmode" value="opaque" />' . '</object>';
                    } else {
                        $img .= '<img src="' . $imageurl . '" alt="" />';
                    }
                }

                $banner['bid'] = $banner_arr[$i]->getVar("bid");
                $banner['impmade'] = $impmade;
                $banner['clicks'] = $clicks;
                $banner['left'] = $left;
                $banner['percent'] = $percent;
                $banner['imageurl'] = $img;
                $banner['name'] = $client_name;
                $banner['uid'] = $client_uid;
                if ($banner_arr[$i]->getVar("clickurl") == '') {
                    $banner['clickurl'] = '#';
                } else {
                    $banner['clickurl'] = $banner_arr[$i]->getVar("clickurl");
                }
                $xoops->tpl()->append_by_ref('banner', $banner);
                $xoops->tpl()->append_by_ref('popup_banner', $banner);
                unset($banner);
            }
        }
        // Display Page Navigation
        if ($banner_count > $nb_banners) {
            $nav = new XoopsPageNav($banner_count, $nb_banners, $start, 'start', 'startF=' . $startF);
            $xoops->tpl()->assign('nav_menu_banner', $nav->renderNav(4));
        }
        // Display Finished Banners
        // Criteria
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('status', 0));
        if ($admin == false) {
            $criteria->add(new Criteria('cid', '(' . implode(',', $cid) . ')','IN'));
        }
        $criteria->setSort("datestart");
        $criteria->setOrder("DESC");
        $criteria->setStart($startF);
        $criteria->setLimit($nb_banners);

        $banner_finish_count = $banner_Handler->getCount($criteria);
        $banner_finish_arr = $banner_Handler->getall($criteria);

        $xoops->tpl()->assign('banner_finish_count', $banner_finish_count);

        if ($banner_finish_count > 0) {
            foreach (array_keys($banner_finish_arr) as $i) {
                $bid = $banner_finish_arr[$i]->getVar("bid");
                $imageurl = $banner_finish_arr[$i]->getVar("imageurl");
                $htmlbanner = $banner_finish_arr[$i]->getVar("htmlbanner");
                $htmlcode = $banner_finish_arr[$i]->getVar("htmlcode");
                $impressions = $banner_finish_arr[$i]->getVar("impmade");
                $clicks = $banner_finish_arr[$i]->getVar("clicks");
                $client = $client_Handler->get($banner_finish_arr[$i]->getVar("cid"));
                if (is_object($client)) {
                    $client_name = $client->getVar("name");
                    $client_uid = $client->getVar("uid");
                } else {
                    $client_name = '';
                    $client_uid = 0;
                }
                if ($impressions != 0) {
                    $percent = substr(100 * $clicks / $impressions, 0, 5);
                } else {
                    $percent = 0;
                }
                $img = '';
                if ($htmlbanner) {
                    $img .= html_entity_decode($htmlcode);
                } else {
                    $img = '<div id="xo-bannerfix">';
                    if (stristr($imageurl, '.swf')) {
                        $img .= '<object type="application/x-shockwave-flash" width="468" height="60" data="' . $imageurl . '" style="z-index:100;">' . '<param name="movie" value="' . $imageurl . '" />' . '<param name="wmode" value="opaque" />' . '</object>';
                    } else {
                        $img .= '<img src="' . $imageurl . '" alt="" />';
                    }

                    $img .= '</div>';
                }
                $banner_finish['bid'] = $bid;
                $banner_finish['impressions'] = $impressions;
                $banner_finish['clicks'] = $clicks;
                $banner_finish['percent'] = $percent;
                $banner_finish['imageurl'] = $img;
                $banner_finish['datestart'] = XoopsLocal::formatTimestamp($banner_finish_arr[$i]->getVar("datestart"), "m");
                $banner_finish['dateend'] = XoopsLocal::formatTimestamp($banner_finish_arr[$i]->getVar("dateend"), "m");
                $banner_finish['clickurl'] = $banner_finish_arr[$i]->getVar("clickurl");
                $banner_finish['name'] = $client_name;
                $banner_finish['uid'] = $client_uid;
                $xoops->tpl()->append_by_ref('banner_finish', $banner_finish);
                $xoops->tpl()->append_by_ref('popup_banner_finish', $banner_finish);
                unset($banner_finish);
            }
        }
        // Display Page Navigation
        if ($banner_finish_count > $nb_banners) {
            $nav = new XoopsPageNav($banner_finish_count, $nb_banners, $startF, 'startF', 'start=' . $start);
            $xoops->tpl()->assign('nav_menu_bannerF', $nav->renderNav(4));
        }
        $xoops->footer();
        break;

    case 'edit':
        $access = false;
        $admin = false;
        if ($xoops->isUser()) {
            $uid = $xoops->user->getVar('uid');
        } else {
            $uid = 0;
        }
        if ($uid == 0) {
            $access = false;
        }
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('uid', $uid));
        $client_count = $client_Handler->getCount($criteria);
        if ($client_count != 0) {
            $access = true;
        }
        if ($xoops->userIsAdmin) {
            $access = true;
            $admin = true;
        }
        if ($access == false) {
            $xoops->redirect(XOOPS_URL, 2, _NOPERM);
        }
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            // Call header
            $xoops->header('banners_client.html');
            $obj = $banner_Handler->get($bid);
            $form = new XoopsThemeForm(_AM_BANNERS_CLIENTS_EDIT, 'form', 'index.php', 'post', true);
            $form->addElement(new XoopsFormText( _AM_BANNERS_BANNERS_CLICKURL, 'clickurl', 80, 255, $obj->getVar('clickurl') ), false );
            $form->addElement(new XoopsFormHidden('op', 'save'));
            $form->addElement(new XoopsFormHidden('bid', $obj->getVar('bid')));
            $form->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
            $xoops->tpl()->assign('form', $form->render());
            $xoops->footer();
        } else {
            $xoops->redirect(XOOPS_URL, 1, _MD_BANNERS_INDEX_DBERROR);
        }
        break;

    case 'save':
        $access = true;
        $admin = false;
        if ($xoops->isUser()) {
            $uid = $xoops->user->getVar('uid');
        } else {
            $uid = 0;
        }
        if ($uid == 0) {
            $access = false;
        }
        if ($xoops->userIsAdmin) {
            $access = true;
            $admin = true;
        }
        if ($access == false) {
            $xoops->redirect(XOOPS_URL, 2, _NOPERM);
        }
        if (!$xoops->security()->check()) {
            $xoops->redirect("index.php", 3, implode(",", $xoops->security()->getErrors()));
        }
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            $obj = $banner_Handler->get($bid);
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('cid', $obj->getVar("cid")));
            $client_arr = $client_Handler->getall($criteria);
            foreach (array_keys($client_arr) as $i) {
                if ($admin == false) {
                    if ($client_arr[$i]->getVar("uid") != $uid){
                        $xoops->redirect(XOOPS_URL, 2, _NOPERM);
                    }
                }
            }
            $obj->setVar("clickurl", $_POST["clickurl"]);
            if ($banner_Handler->insert($obj)) {
                $xoops->redirect("index.php", 2, _AM_BANNERS_DBUPDATED);
            }
            $xoops->error($obj->getHtmlErrors());
        } else {
            $xoops->redirect(XOOPS_URL, 1, _MD_BANNERS_INDEX_NO_ID);
        }
        break;

    case 'EmailStats':
        $access = true;
        $admin = false;
        if ($xoops->isUser()) {
            $uid = $xoops->user->getVar('uid');
        } else {
            $uid = 0;
        }
        if ($uid == 0) {
            $access = false;
        }
        if ($xoops->userIsAdmin) {
            $access = true;
            $admin = true;
        }
        if ($access == false) {
            $xoops->redirect(XOOPS_URL, 2, _NOPERM);
        }
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            $banner = $banner_Handler->get($bid);
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('cid', $banner->getVar("cid")));
            $client_arr = $client_Handler->getall($criteria);
            foreach (array_keys($client_arr) as $i) {
                if ($admin == false) {
                    if ($client_arr[$i]->getVar("uid") != $uid){
                        $xoops->redirect(XOOPS_URL, 2, _NOPERM);
                    }
                }
                $client_uid = $client_arr[$i]->getVar("uid");
                $client_name = $client_arr[$i]->getVar("name");
            }
            $user = $member_handler->getUser($client_arr[$i]->getVar("uid"));
            $email = $user->getVar("email", 'n');
            if ($email != ''){
                if ($banner->getVar('impmade') == 0) {
                    $percent = 0;
                } else {
                    $percent = substr(100 * $banner->getVar('clicks') / $banner->getVar('impmade'), 0, 5);
                }
                if ($banner->getVar('imptotal') == 0) {
                    $left = _AM_BANNERS_BANNERS_UNLIMIT;
                    $banner->setVar('imptotal', _AM_BANNERS_BANNERS_UNLIMIT);
                } else {
                    $left = $banner->getVar('imptotal') - $banner->getVar('impmade');
                }
                $date = date("F jS Y, h:iA.");
                $subject = sprintf(_MD_BANNERS_INDEX_MAIL_SUBJECT, $xoops->getConfig('sitename'));
                $message = sprintf(_MD_BANNERS_INDEX_MAIL_MESSAGE, $xoops->getConfig('sitename'), $client_name, $bid, $banner->getVar('imageurl'), $banner->getVar('clickurl'), $banner->getVar('imptotal'), $banner->getVar('impmade'), $left, $banner->getVar('clicks'), $percent, $date);
                $xoopsMailer = $xoops->getMailer();
                $xoopsMailer->useMail();
                $xoopsMailer->setToEmails($email);
                $xoopsMailer->setFromEmail($xoops->getConfig('adminmail'));
                $xoopsMailer->setFromName($xoops->getConfig('sitename'));
                $xoopsMailer->setSubject($subject);
                $xoopsMailer->setBody($message);
                if (!$xoopsMailer->send()) {
                    $xoops->redirect("index.php", 2, sprintf(_MAIL_SENDMAILNG, $email));
                }
                $xoops->redirect("index.php", 2, _MD_BANNERS_INDEX_MAIL_OK);
            } else {
                $xoops->redirect("index.php", 2, _MD_BANNERS_INDEX_NOMAIL);
            }
        } else {
            $xoops->redirect(XOOPS_URL, 1, _MD_BANNERS_INDEX_NO_ID);
        }
        break;

    case 'click':
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            $banner = $xoops->getHandlerBanner()->get($bid);
            if ($banner) {
                if ($xoops->security()->checkReferer()) {
                    $banner->setVar('clicks', $banner->getVar('clicks') + 1);
                    $banner_Handler->insert($banner);
                    header('Location: ' . $banner->getVar('clickurl'));
                    exit();
                } else {
                    //No valid referer found so some javascript error or direct access found
                    echo _MD_BANNERS_INDEX_NO_REFERER;
                }
            }
        }
        $xoops->redirect(XOOPS_URL, 3, _MD_BANNERS_INDEX_NO_ID);
        break;
}