<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id$
 */

function xoops_module_install_banners(&$module)
{
    $xoops = Xoops::getInstance();
    // update client
    $sql = "SHOW COLUMNS FROM " . $xoops->db()->prefix("bannerclient");
    $result = $xoops->db()->queryF($sql);
    if (($rows = $xoops->db()->getRowsNum($result)) == 7) {
        $sql = "SELECT * FROM " . $xoops->db()->prefix("bannerclient");
        $result = $xoops->db()->query($sql);
        while ($myrow = $xoops->db()->fetchArray($result)) {
            $extrainfo = $myrow['contact'] . ' - ' . $myrow['email'] . ' - ' . $myrow['login'] . ' - ' . $myrow['passwd'] . ' - ' . $myrow['extrainfo'];
            $sql = "UPDATE `" . $xoops->db()->prefix("bannerclient") . "` SET `extrainfo` = '" .  $extrainfo . "' WHERE `cid` = " . $myrow['cid'];
            $xoops->db()->queryF($sql);
        }
        $sql = "ALTER TABLE " . $xoops->db()->prefix("bannerclient") . " DROP contact, DROP email, DROP login, DROP passwd";
        $xoops->db()->queryF($sql);
        $sql = "ALTER TABLE " . $xoops->db()->prefix("bannerclient") . " ADD `uid` MEDIUMINT( 8 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `cid`";
        $xoops->db()->queryF($sql);
    }
    // update banner
    $sql = "SHOW COLUMNS FROM " . $xoops->db()->prefix("banner");
    $result = $xoops->db()->queryF($sql);
    if (($rows = $xoops->db()->getRowsNum($result)) == 10) {
        $sql = "ALTER TABLE " . $xoops->db()->prefix("banner") . " CHANGE `date` `datestart` INT( 10 ) NOT NULL DEFAULT '0'";
        $xoops->db()->queryF($sql);
        $sql = "ALTER TABLE " . $xoops->db()->prefix("banner") . " ADD `dateend` INT( 10 ) NOT NULL DEFAULT '0' AFTER `datestart`";
        $xoops->db()->queryF($sql);
        $sql = "ALTER TABLE " . $xoops->db()->prefix("banner") . " ADD `status` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `htmlcode`";
        $xoops->db()->queryF($sql);
    }
    // update bannerfinish
    $sql = "SHOW COLUMNS FROM " . $xoops->db()->prefix("bannerfinish");
    $result = $xoops->db()->queryF($sql);
    if (($rows = $xoops->db()->getRowsNum($result)) == 6) {
        $sql = "SELECT * FROM " . $xoops->db()->prefix("bannerfinish");
        $result = $xoops->db()->query($sql);
        while ($myrow = $xoops->db()->fetchArray($result)) {
            $sql = "INSERT INTO `" . $xoops->db()->prefix("banner") . "` (`cid`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `datestart`, `dateend`, `htmlbanner`, `htmlcode`, `status`) VALUES (" . $myrow['cid'] . ", 0, " . $myrow['impressions'] . ", " . $myrow['clicks'] . ", 0, '', " . $myrow['datestart'] . ", " . $myrow['dateend'] . ", 0, '', 0)";
            $xoops->db()->queryF($sql);
        }
        $sql = "DROP TABLE " . $xoops->db()->prefix("bannerfinish");
        $xoops->db()->queryF($sql);
    }

    // delete banners and my_ip
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'banners'";
    $xoops->db()->queryF($sql);
    $sql = "DELETE FROM " . $xoops->db()->prefix("config") . " WHERE `conf_name` = 'my_ip'";
    $xoops->db()->queryF($sql);

    // change 'imptotal'
    $sql = "ALTER TABLE " . $xoops->db()->prefix("banner") . " CHANGE `imptotal` `imptotal` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0'";
    $xoops->db()->queryF($sql);


    // move file
    // Get banners handler
    $banner_Handler = $xoops->getModuleHandler('banner','banners');
    $banner_arr = $banner_Handler->getall();
    foreach (array_keys($banner_arr) as $i) {
        $namefile = substr_replace($banner_arr[$i]->getVar('imageurl'),'',0,strlen(XOOPS_URL . '/images/banners/'));
        $pathfile_image =  XOOPS_ROOT_PATH . '/images/banners/' . $namefile;
        $pathfile_upload =  XOOPS_ROOT_PATH . '/uploads/banners/' . $namefile;
        if (is_file($pathfile_image)){
            copy($pathfile_image, $pathfile_upload);
            unlink($pathfile_image);
            $obj = $banner_Handler->get($banner_arr[$i]->getVar('bid'));
            $obj->setVar("imageurl", XOOPS_UPLOAD_URL . '/banners/' . $namefile);
            $banner_Handler->insert($obj);
        }
    }

    // data for table 'banner'
    $sql = "INSERT INTO `" . $xoops->db()->prefix("banner") . "` (`cid`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `datestart`, `dateend`, `htmlbanner`, `htmlcode`, `status`) VALUES (1, 0, 1, 0, '" . XOOPS_URL . "/uploads/banners/xoops_flashbanner2.swf', 'http://www.xoops.org/', " . time() . ", 0, 0, '', 1)";
    $xoops->db()->queryF($sql);
    $sql = "INSERT INTO `" . $xoops->db()->prefix("banner") . "` (`cid`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `datestart`, `dateend`, `htmlbanner`, `htmlcode`, `status`) VALUES (1, 0, 1, 0, '" . XOOPS_URL . "/uploads/banners/xoops_banner_2.gif', 'http://www.xoops.org/', " . time() . ", 0, 0, '', 1)";
    $xoops->db()->queryF($sql);
    $sql = "INSERT INTO `" . $xoops->db()->prefix("banner") . "` (`cid`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `datestart`, `dateend`, `htmlbanner`, `htmlcode`, `status`) VALUES (1, 0, 1, 0, '" . XOOPS_URL . "/uploads/banners/banner.swf', 'http://www.xoops.org/', " . time() . ", 0, 0, '', 1)";
    $xoops->db()->queryF($sql);
    return true;
}