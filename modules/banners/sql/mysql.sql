#
# Table structure for table `banner`
#

CREATE TABLE banner (
  bid smallint(5) unsigned NOT NULL auto_increment,
  cid tinyint(3) unsigned NOT NULL default '0',
  imptotal int(10) unsigned NOT NULL default '0',
  impmade mediumint(8) unsigned NOT NULL default '0',
  clicks mediumint(8) unsigned NOT NULL default '0',
  imageurl varchar(255) NOT NULL default '',
  clickurl varchar(255) NOT NULL default '',
  datestart int(10) NOT NULL default '0',
  dateend int(10) NOT NULL default '0',
  htmlbanner tinyint(1) NOT NULL default '0',
  htmlcode text,
  status tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (bid),
  KEY idxbannercid (cid),
  KEY idxbannerbidcid (bid,cid)
) ENGINE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `bannerclient`
#

CREATE TABLE bannerclient (
  cid smallint(5) unsigned NOT NULL auto_increment,
  uid mediumint(8) unsigned NOT NULL default '0',
  name varchar(60) NOT NULL default '',
  extrainfo text,
  PRIMARY KEY  (cid),
  KEY name (name)
) ENGINE=MyISAM;
# --------------------------------------------------------

#
# Dumping data for table `bannerclient`
#

INSERT INTO bannerclient VALUES (1,0, 'XOOPS', 'XOOPS Dev Team');