<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id$
 */
include dirname(__FILE__) . '/header.php';
// Get main instance
$system = System::getInstance();
$xoops = Xoops::getInstance();
// Parameters
$nb_banners = $xoops->getModuleConfig('banners_pager');
$mimetypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png', 'image/png', 'application/x-shockwave-flash');
$upload_size = 500000;
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'default', 'string');
// Get banners handler
$banner_Handler = $xoops->getModuleHandler('banner');
$client_Handler = $xoops->getModuleHandler('bannerclient');
// Call header
$xoops->header('banners_admin_banners.html');

// Get start pager
$start = $system->cleanVars($_REQUEST, 'start', 0, 'int');
$startF = $system->cleanVars($_REQUEST, 'startF', 0, 'int');

$admin_page = new XoopsModuleAdmin();
$admin_page->renderNavigation('banners.php');

switch ($op) {

    case 'list':
    default:
        // Define Stylesheet
        $xoops->theme()->addStylesheet('media/jquery/ui/' . $xoops->getModuleConfig('jquery_theme', 'system') . '/ui.all.css');
        // Define scripts
        $xoops->theme()->addScript($xoops->url('/media/jquery/ui/jquery.ui.js'));
        $xoops->theme()->addScript('modules/system/js/admin.js');

        $admin_page->addTips(_AM_BANNERS_TIPS_BANNERS);
        $admin_page->addItemButton(_AM_BANNERS_BANNERS_ADD, 'banners.php?op=new', 'add');
        $admin_page->renderTips();
        if ($client_Handler->getCount() == 0){
            $xoops->error(_AM_BANNERS_BANNERS_ERROR_NOCLIENT);
        } else {
            $admin_page->renderButton();
        }
        // Display banner
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('status', 0, '!='));
        $criteria->setSort("datestart");
        $criteria->setOrder("DESC");
        $criteria->setStart($start);
        $criteria->setLimit($nb_banners);

        $banner_count = $banner_Handler->getCount($criteria);
        $banner_arr = $banner_Handler->getall($criteria);

        $xoops->tpl()->assign('banner_count', $banner_count);

        if ($banner_count > 0) {
            foreach (array_keys($banner_arr) as $i) {
                $imptotal = $banner_arr[$i]->getVar("imptotal");
                $impmade = $banner_arr[$i]->getVar("impmade");
                $imageurl = $banner_arr[$i]->getVar("imageurl");
                $clicks = $banner_arr[$i]->getVar("clicks");
                $htmlbanner = $banner_arr[$i]->getVar("htmlbanner");
                $htmlcode = $banner_arr[$i]->getVar("htmlcode");
                $name_client = $client_Handler->get($banner_arr[$i]->getVar("cid"));
                $name = '';
                if (is_object($name_client)) {
                    $name = $name_client->getVar("name");
                }

                if ($impmade == 0) {
                    $percent = 0;
                } else {
                    $percent = substr(100 * $clicks / $impmade, 0, 5);
                }
                if ($imptotal == 0) {
                    $left = "" . _AM_BANNERS_BANNERS_UNLIMIT . "";
                } else {
                    $left = $imptotal - $impmade;
                }
                $img = '';
                if ($htmlbanner) {
                    $img .= html_entity_decode($htmlcode);
                } else {
                    if (stristr($imageurl, '.swf')) {
                        $img .= '<object type="application/x-shockwave-flash" width="468" height="60" data="' . $imageurl . '" style="z-index:100;">' . '<param name="movie" value="' . $imageurl . '" />' . '<param name="wmode" value="opaque" />' . '</object>';
                    } else {
                        $img .= '<img src="' . $imageurl . '" alt="" />';
                    }
                }

                $banner['bid'] = $banner_arr[$i]->getVar("bid");
                $banner['impmade'] = $impmade;
                $banner['clicks'] = $clicks;
                $banner['left'] = $left;
                $banner['percent'] = $percent;
                $banner['imageurl'] = $img;
                $banner['name'] = $name;
                $xoops->tpl()->append_by_ref('banner', $banner);
                $xoops->tpl()->append_by_ref('popup_banner', $banner);
                unset($banner);
            }
        }
        // Display Page Navigation
        if ($banner_count > $nb_banners) {
            $nav = new XoopsPageNav($banner_count, $nb_banners, $start, 'start', 'startF=' . $startF);
            $xoops->tpl()->assign('nav_menu_banner', $nav->renderNav(4));
        }
        // Display Finished Banners
        // Criteria
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('status', 0));
        $criteria->setSort("datestart");
        $criteria->setOrder("DESC");
        $criteria->setStart($startF);
        $criteria->setLimit($nb_banners);

        $banner_finish_count = $banner_Handler->getCount($criteria);
        $banner_finish_arr = $banner_Handler->getall($criteria);

        $xoops->tpl()->assign('banner_finish_count', $banner_finish_count);

        if ($banner_finish_count > 0) {
            foreach (array_keys($banner_finish_arr) as $i) {
                $bid = $banner_finish_arr[$i]->getVar("bid");
                $imageurl = $banner_finish_arr[$i]->getVar("imageurl");
                $htmlbanner = $banner_finish_arr[$i]->getVar("htmlbanner");
                $htmlcode = $banner_finish_arr[$i]->getVar("htmlcode");
                $impressions = $banner_finish_arr[$i]->getVar("impmade");
                $clicks = $banner_finish_arr[$i]->getVar("clicks");
                if ($impressions != 0) {
                    $percent = substr(100 * $clicks / $impressions, 0, 5);
                } else {
                    $percent = 0;
                }
                $img = '';
                if ($htmlbanner) {
                    $img .= html_entity_decode($htmlcode);
                } else {
                    $img = '<div id="xo-bannerfix">';
                    if (stristr($imageurl, '.swf')) {
                        $img .= '<object type="application/x-shockwave-flash" width="468" height="60" data="' . $imageurl . '" style="z-index:100;">' . '<param name="movie" value="' . $imageurl . '" />' . '<param name="wmode" value="opaque" />' . '</object>';
                    } else {
                        $img .= '<img src="' . $imageurl . '" alt="" />';
                    }

                    $img .= '</div>';
                }
                $banner_finish['bid'] = $bid;
                $banner_finish['impressions'] = $impressions;
                $banner_finish['clicks'] = $clicks;
                $banner_finish['percent'] = $percent;
                $banner_finish['imageurl'] = $img;
                $banner_finish['datestart'] = XoopsLocal::formatTimestamp($banner_finish_arr[$i]->getVar("datestart"), "m");
                $banner_finish['dateend'] = XoopsLocal::formatTimestamp($banner_finish_arr[$i]->getVar("dateend"), "m");
                $name_client = $client_Handler->get($banner_finish_arr[$i]->getVar("cid"));
                $name = '';
                if (is_object($name_client)) {
                    $name = $name_client->getVar("name");
                }
                $banner_finish['name'] = $name;
                $xoops->tpl()->append_by_ref('banner_finish', $banner_finish);
                $xoops->tpl()->append_by_ref('popup_banner_finish', $banner_finish);
                unset($banner_finish);
            }
        }
        // Display Page Navigation
        if ($banner_finish_count > $nb_banners) {
            $nav = new XoopsPageNav($banner_finish_count, $nb_banners, $startF, 'startF', 'start=' . $start);
            $xoops->tpl()->assign('nav_menu_bannerF', $nav->renderNav(4));
        }
        break;

    case 'new':
        $admin_page->addTips(sprintf(_AM_BANNERS_TIPS_BANNERS_FORM1, implode(', ', $mimetypes)) . sprintf(_AM_BANNERS_TIPS_BANNERS_FORM2, $upload_size / 1000));
        $admin_page->addItemButton(_AM_BANNERS_BANNERS_LIST, 'banners.php', 'application-view-detail');
        $admin_page->renderTips();
        $admin_page->renderButton();
        $obj = $banner_Handler->create();
        $form = $xoops->getModuleForm($obj, 'banner');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'edit':
        $admin_page->addTips(sprintf(_AM_BANNERS_TIPS_BANNERS_FORM1, implode(', ', $mimetypes)) . sprintf(_AM_BANNERS_TIPS_BANNERS_FORM2, $upload_size / 1000));
        $admin_page->addItemButton(_AM_BANNERS_BANNERS_LIST, 'banners.php', 'application-view-detail');
        $admin_page->renderTips();
        $admin_page->renderButton();
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            $obj = $banner_Handler->get($bid);
            $form = $xoops->getModuleForm($obj, 'banner');
            $xoops->tpl()->assign('form', $form->render());
        } else {
            $xoops->redirect('banners.php', 1, _AM_SYSTEM_DBERROR);
        }
        break;

    case 'save':
        if (!$xoops->security()->check()) {
            $xoops->redirect("banners.php", 3, implode(",", $xoops->security()->getErrors()));
        }
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            $obj = $banner_Handler->get($bid);
        } else {
            $obj = $banner_Handler->create();
            $obj->setVar("datestart", time());
            $obj->setVar("dateend", 0);
            $obj->setVar("status", 1);
        }

        $obj->setVar("cid", $_POST["cid"]);
        $obj->setVar("imptotal", $_POST["imptotal"]);
        $obj->setVar("clickurl", $_POST["clickurl"]);
        $obj->setVar("htmlbanner", $system->cleanVars($_POST, 'htmlbanner', 0, 'int'));
        $obj->setVar("htmlcode", $_POST["htmlcode"]);

        $uploader_banners_img = new XoopsMediaUploader(XOOPS_UPLOAD_PATH . '/banners', $mimetypes, $upload_size, null, null);

        if ($uploader_banners_img->fetchMedia("banners_imageurl")) {
            $uploader_banners_img->setPrefix("banner");
            $uploader_banners_img->fetchMedia("banners_imageurl");
            if (!$uploader_banners_img->upload()) {
                $errors = $uploader_banners_img->getErrors();
                $xoops->redirect("javascript:history.go(-1)", 3, $errors);
            } else {
                $obj->setVar("imageurl", XOOPS_UPLOAD_URL . '/banners/' . $uploader_banners_img->getSavedFileName());
            }
        } else {
            if ($_POST["banners_imageurl"] == 'blank.gif') {
                $obj->setVar("imageurl", $_POST["imageurl"]);
            } else {
                $obj->setVar("imageurl", XOOPS_UPLOAD_URL . '/banners/' . $_POST["banners_imageurl"]);
            }
        }

        if ($banner_Handler->insert($obj)) {
            $xoops->redirect("banners.php", 2, _AM_BANNERS_DBUPDATED);
        }
        $xoops->error($obj->getHtmlErrors());
        $form = $xoops->getModuleForm($obj, 'banner');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'delete':
        $admin_page->addItemButton(_AM_BANNERS_BANNERS_ADD, 'banners.php?op=new', 'add');
        $admin_page->addItemButton(_AM_BANNERS_BANNERS_LIST, 'banners.php', 'application-view-detail');
        $admin_page->renderButton();
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        if ($bid > 0) {
            $obj = $banner_Handler->get($bid);
            if (isset($_POST["ok"]) && $_POST["ok"] == 1) {
                if (!$xoops->security()->check()) {
                    $xoops->redirect("banners.php", 3, implode(",", $xoops->security()->getErrors()));
                }
                $namefile = substr_replace($obj->getVar('imageurl'),'',0,strlen(XOOPS_URL . '/uploads/banners/'));
                $urlfile =  XOOPS_ROOT_PATH . '/uploads/banners/' . $namefile;
                if ($banner_Handler->delete($obj)) {
                    // delete banner
                    if (is_file($urlfile)){
                        chmod($urlfile, 0777);
                        unlink($urlfile);
                    }
                    $xoops->redirect("banners.php", 2, _AM_BANNERS_DBUPDATED);
                } else {
                    $xoops->error($obj->getHtmlErrors());
                }
            } else {
                // Define Stylesheet
                $xoops->theme()->addStylesheet('modules/system/css/admin.css');
                $img = '';
                $imageurl = $obj->getVar("imageurl");
                if ($obj->getVar("htmlbanner")) {
                    $img .= html_entity_decode($obj->getVar("htmlcode"));
                } else {
                    if (strtolower(substr($imageurl, strrpos($imageurl, "."))) == ".swf") {
                        $img .= "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/ swflash.cab#version=6,0,40,0\" width=\"468\" height=\"60\">";
                        $img .= "<param name=movie value=\"$imageurl\">";
                        $img .= "<embed src=\"$imageurl\" pluginspage=\"http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash\"  type=\"application/x-shockwave-flash\" width=\"468\" height=\"60\">";
                        $img .= "</embed>";
                        $img .= "</object>";
                    } else {
                        $img .= "<img src='" . $imageurl . "' alt='' />";
                    }
                }
                $xoops->confirm(array("ok" => 1, "bid" => $bid, "op" => "delete"), 'banners.php', sprintf(_AM_BANNERS_BANNERS_SUREDEL). '<br \>' . $img . '<br \>');
            }
        } else {
            $xoops->redirect('banners.php', 1, _AM_SYSTEM_DBERROR);
        }
        break;

    case 'reload':
        $bid = $system->cleanVars($_REQUEST, 'bid', 0, 'int');
        $obj = $banner_Handler->get($bid);
        $obj->setVar("datestart", time());
        $obj->setVar("dateend", 0);
        $obj->setVar("imptotal", 0);
        $obj->setVar("impmade", 0);
        $obj->setVar("clicks", 0);
        $obj->setVar("status", 1);
        if ($banner_Handler->insert($obj)) {
            $xoops->redirect("banners.php", 2, _AM_BANNERS_DBUPDATED);
        }
        $xoops->error($obj->getHtmlErrors());
        break;
}
$xoops->footer();