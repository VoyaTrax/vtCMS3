<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: clients.php 10328 2012-12-07 00:56:07Z trabis $
 */
include dirname(__FILE__) . '/header.php';

// Get main instance
$system = System::getInstance();
$xoops = Xoops::getInstance();

// Parameters
$nb_clients = $xoops->getModuleConfig('banners_clientspager');
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'default', 'string');
// Get banners handler
$banner_Handler = $xoops->getModuleHandler('banner');
$client_Handler = $xoops->getModuleHandler('bannerclient');
// Get member handler
$member_handler = $xoops->getHandlerMember();
// Call header
$xoops->header('banners_admin_clients.html');
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'list', 'string');
// Get start pager
$start = $system->cleanVars($_REQUEST, 'start', 0, 'int');

$admin_page = new XoopsModuleAdmin();
$admin_page->renderNavigation('clients.php');

switch ($op) {

    case 'list':
    default:
        // Define Stylesheet
        $xoops->theme()->addStylesheet('media/jquery/ui/' . $xoops->getModuleConfig('jquery_theme', 'system') . '/ui.all.css');
        // Define scripts
        $xoops->theme()->addScript($xoops->url('/media/jquery/ui/jquery.ui.js'));
        $xoops->theme()->addScript('modules/system/js/admin.js');

        $admin_page->addTips(_AM_BANNERS_TIPS_CLIENTS);
        $admin_page->addItemButton(_AM_BANNERS_CLIENTS_ADD, 'clients.php?op=new', 'add');
        $admin_page->renderTips();
        $admin_page->renderButton();

        // Display client
        $criteria = new CriteriaCompo();
        $criteria->setSort("name");
        $criteria->setOrder("ASC");
        $criteria->setStart($start);
        $criteria->setLimit($nb_clients);

        $client_count = $client_Handler->getCount($criteria);
        $client_arr = $client_Handler->getall($criteria);

        $xoops->tpl()->assign('client_count', $client_count);

        if ($client_count > 0) {
            foreach (array_keys($client_arr) as $i) {
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('cid', $client_arr[$i]->getVar("cid"), '='));
                $banner_active = $banner_Handler->getCount($criteria);
                $client['cid'] = $client_arr[$i]->getVar("cid");
                $client['uid'] = $client_arr[$i]->getVar("uid");
                $client['banner_active'] = $banner_active;
                if ( $client_arr[$i]->getVar("uid") == 0) {
                    $client['uname'] = '/';
                    $client['email'] = '/';
                }else{
                    $user = $member_handler->getUser($client_arr[$i]->getVar("uid"));
                    $client['uname'] = $user->getVar("uname");
                    $client['email'] = $user->getVar("email");
                    $client['avatar'] = ($user->getVar("user_avatar") == 'blank.gif')
                        ? XOOPS_URL . 'modules/system/images/icons/default/anonymous.png'
                        : XOOPS_URL . '/uploads/' . $user->getVar("user_avatar");
                    $client['url'] = $user->getVar("url");
                }
                $client['name'] = $client_arr[$i]->getVar("name");
                $client['extrainfo'] = $client_arr[$i]->getVar("extrainfo");
                $xoops->tpl()->append_by_ref('client', $client);
                $xoops->tpl()->append_by_ref('client_banner', $client);
                unset($client);
            }
        }
        // Display Page Navigation
        if ($client_count > $nb_clients) {
            $nav = new XoopsPageNav($client_count, $nb_clients, $start, 'start');
            $xoops->tpl()->assign('nav_menu', $nav->renderNav(4));
        }
        break;

    case 'new':
        $admin_page->addTips(_AM_BANNERS_TIPS_CLIENTS_ADDEDIT);
        $admin_page->addItemButton(_AM_BANNERS_CLIENTS_LIST, 'clients.php', 'application-view-detail');
        $admin_page->renderTips();
        $admin_page->renderButton();
        $obj = $client_Handler->create();
        $form = $xoops->getModuleForm($obj, 'bannerclient');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'edit':
        $admin_page->addTips(_AM_BANNERS_TIPS_CLIENTS_ADDEDIT);
        $admin_page->addItemButton(_AM_BANNERS_CLIENTS_LIST, 'clients.php', 'application-view-detail');
        $admin_page->renderTips();
        $admin_page->renderButton();
        $cid = $system->cleanVars($_REQUEST, 'cid', 0, 'int');
        if ($cid > 0) {
            $obj = $client_Handler->get($cid);
            $form = $xoops->getModuleForm($obj, 'bannerclient');
            $xoops->tpl()->assign('form', $form->render());
        } else {
            $xoops->redirect('clients.php', 1, _AM_SYSTEM_DBERROR);
        }
        break;

    case 'save':
        if (!$xoops->security()->check()) {
            $xoops->redirect("clients.php", 3, implode(",", $xoops->security()->getErrors()));
        }
        $cid = $system->cleanVars($_REQUEST, 'cid', 0, 'int');
        if ($cid > 0) {
            $obj = $client_Handler->get($cid);
        } else {
            $obj = $client_Handler->create();
        }
        $obj->setVar("name", $_POST["name"]);
        if ($_POST["user"] == 'Y'){
            $obj->setVar("uid", $_POST["uid"]);
        } else {
            $obj->setVar("uid", 0);
        }
        $obj->setVar("extrainfo", $_POST["extrainfo"]);
        if ($client_Handler->insert($obj)) {
            $xoops->redirect("clients.php", 2, _AM_BANNERS_DBUPDATED);
        }
        $xoops->error($obj->getHtmlErrors());
        $form = $xoops->getModuleForm($obj, 'bannerclient');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'delete':
        $admin_page->addItemButton(_AM_BANNERS_CLIENTS_ADD, 'clients.php?op=new', 'add');
        $admin_page->addItemButton(_AM_BANNERS_CLIENTS_LIST, 'clients.php', 'application-view-detail');
        $admin_page->renderButton();
        $cid = $system->cleanVars($_REQUEST, 'cid', 0, 'int');
        if ($cid > 0) {
            $obj = $client_Handler->get($cid);
            if (isset($_POST["ok"]) && $_POST["ok"] == 1) {
                if (!$xoops->security()->check()) {
                    $xoops->redirect("clients.php", 3, implode(",", $xoops->security()->getErrors()));
                }
                if ($client_Handler->delete($obj)) {
                    // Delete client banners
                    $banner_arr = $banner_Handler->getall(new Criteria('cid', $cid));
                    foreach (array_keys($banner_arr) as $i) {
                        $obj = $banner_Handler->get($banner_arr[$i]->getVar('bid'));
                        $namefile = substr_replace($banner_arr[$i]->getVar('imageurl'),'',0,strlen(XOOPS_URL . '/uploads/banners/'));
                        $urlfile =  XOOPS_ROOT_PATH . '/uploads/banners/' . $namefile;
                        if ($banner_Handler->delete($obj)) {
                            // delete banner
                            if (is_file($urlfile)){
                                chmod($urlfile, 0777);
                                unlink($urlfile);
                            }
                        } else {
                            $xoops->error($obj->getHtmlErrors());
                        }
                    }
                    $xoops->redirect("clients.php", 2, _AM_BANNERS_DBUPDATED);
                } else {
                    $xoops->error($obj->getHtmlErrors());
                }
            } else {
                // Define Stylesheet
                $xoops->theme()->addStylesheet('modules/system/css/admin.css');
                $xoops->confirm(array("ok" => 1, "cid" => $cid, "op" => "delete"), 'clients.php', sprintf(_AM_BANNERS_CLIENTS_SUREDEL, $obj->getVar("name")) . '<br />');
            }
        } else {
            $xoops->redirect('clients.php', 1, _AM_SYSTEM_DBERROR);
        }
        break;
}
$xoops->footer();