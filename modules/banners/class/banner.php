<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: banner.php 9676 2012-06-19 21:05:06Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class BannersBanner extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('bid', XOBJ_DTYPE_INT, null, false, 5);
        $this->initVar('cid', XOBJ_DTYPE_INT, null, false, 3);
        $this->initVar('imptotal', XOBJ_DTYPE_INT, null, false, 8);
        $this->initVar('impmade', XOBJ_DTYPE_INT, null, false, 8);
        $this->initVar('clicks', XOBJ_DTYPE_INT, null, false, 8);
        $this->initVar('imageurl', XOBJ_DTYPE_TXTBOX, null, false);
        $this->initVar('clickurl', XOBJ_DTYPE_TXTBOX, null, false);
        $this->initVar('datestart', XOBJ_DTYPE_INT, null, false, 10);
        $this->initVar('dateend', XOBJ_DTYPE_INT, null, false, 10);
        $this->initVar('htmlbanner', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('htmlcode', XOBJ_DTYPE_TXTBOX, null, false);
        $this->initVar('status', XOBJ_DTYPE_INT, null, false, 1);
    }
}

class BannersBannerHandler extends XoopsPersistableObjectHandler
{
    /**
     * @param null|XoopsDatabase $db
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'banner', 'bannersbanner', 'bid', 'imageurl');
    }
}