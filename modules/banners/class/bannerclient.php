<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: bannerclient.php 9676 2012-06-19 21:05:06Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('XOOPS root path not defined');

class BannersBannerclient extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('cid', XOBJ_DTYPE_INT, null, false, 5);
        $this->initVar('uid', XOBJ_DTYPE_INT, 0, true);
        $this->initVar('name', XOBJ_DTYPE_TXTBOX, null, false);
        $this->initVar('extrainfo', XOBJ_DTYPE_TXTAREA, null, false);
    }
}

class BannersBannerclientHandler extends XoopsPersistableObjectHandler
{
    /**
     * @param null|XoopsDatabase $db
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'bannerclient', 'bannersbannerclient', 'cid', 'name');
    }
}