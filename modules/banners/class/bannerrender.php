<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * banners module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         banners
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: bannerrender.php 10328 2012-12-07 00:56:07Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class BannerRender
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Display banner
     *
     * @param $nb_banner
     * @param $align
     * @param $client
     * @param $ids
     *
     * @return string
     */
    public function displayBanner($nb_banner = 1, $align = 'H', $client = array(), $ids = '')
    {
        $xoops = Xoops::getInstance();
        if ($xoops->isActiveModule('banners')) {
            // Get banners handler
            $banner_Handler = $xoops->getModuleHandler('banner', 'banners');
            // Display banner
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('status', 0, '!='));
            $criteria->setSort('RAND()');
            if (!empty($client)) {
                if (!in_array(0, $client)) {
                    $criteria->add(new Criteria('cid', '(' . implode(',', $client) . ')', 'IN'));
                }
            }
            if ($ids == '') {
                $criteria->setLimit($nb_banner);
                $criteria->setStart(0);
            } else {
                $criteria->add(new Criteria('bid', '(' . $ids . ')', 'IN'));
            }
            $banner_arr = $banner_Handler->getall($criteria);
            $numrows = count($banner_arr);
            $bannerobject = '';
            if ($numrows > 0) {
                foreach (array_keys($banner_arr) as $i) {
                    $imptotal = $banner_arr[$i]->getVar("imptotal");
                    $impmade = $banner_arr[$i]->getVar("impmade");
                    $htmlbanner = $banner_arr[$i]->getVar("htmlbanner");
                    $htmlcode = $banner_arr[$i]->getVar("htmlcode");
                    $imageurl = $banner_arr[$i]->getVar("imageurl");
                    $bid = $banner_arr[$i]->getVar("bid");
                    $clickurl = $banner_arr[$i]->getVar("clickurl");
                    /**
                     * Print the banner
                     */
                    if ($htmlbanner) {
                        $bannerobject .= $htmlcode;
                    } else {
                        if (stristr($imageurl, '.swf')) {
                            $bannerobject .= '<a href="' . XOOPS_URL . '/modules/banners/index.php?op=click&amp;bid=' . $bid . '" rel="external" title="' . $clickurl . '"></a>' . '<object type="application/x-shockwave-flash" width="468" height="60" data="' . $imageurl . '" style="z-index:100;">' . '<param name="movie" value="' . $imageurl . '" />' . '<param name="wmode" value="opaque" />' . '</object>';
                        } else {
                            $bannerobject .= '<a href="' . XOOPS_URL . '/modules/banners/index.php?op=click&amp;bid=' . $bid . '" rel="external" title="' . $clickurl . '"><img src="' . $imageurl . '" alt="' . $clickurl . '" /></a>';
                        }
                    }
                    if ($align == 'V') {
                        $bannerobject .= '<br /><br />';
                    } else {
                        $bannerobject .= '&nbsp;';
                    }
                    if ($xoops->getModuleConfig('banners_myip', 'banners') == $xoops->getEnv('REMOTE_ADDR')) {
                        // EMPTY
                    } else {
                        /**
                         * Check if this impression is the last one
                         */
                        $impmade = $impmade + 1;
                        if ($imptotal > 0 && $impmade >= $imptotal) {
                            $xoops->db()->queryF(sprintf('UPDATE %s SET status = %u, dateend = %u WHERE bid = %u', $xoops->db()->prefix('banner'), 0, time(), $bid));
                        } else {
                            $xoops->db()->queryF(sprintf('UPDATE %s SET impmade = %u WHERE bid = %u', $xoops->db()->prefix('banner'), $impmade, $bid));
                        }
                    }
                }
                return $bannerobject;
            }
        }
    }
}