<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU public license
 * @author          trabis <lusopoemas@gmail.com>
 * @version         $Id: install.php 10538 2012-12-24 16:57:18Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

function xoops_module_install_notifications(&$module)
{
    $xoops = Xoops::getInstance();
    $sql = "SHOW COLUMNS FROM " . $xoops->db()->prefix("xoopsnotifications");
    $result = $xoops->db()->queryF($sql);
    if (($rows = $xoops->db()->getRowsNum($result)) == 7) {
        $sql = "SELECT * FROM " . $xoops->db()->prefix("xoopsnotifications");
        $result = $xoops->db()->query($sql);
        while ($myrow = $xoops->db()->fetchArray($result)) {
            $sql = "INSERT INTO `" . $xoops->db()->prefix("notifications") . "` (`id`, `modid`, `itemid`, `category`, `event`, `uid`, `mode`) VALUES (" . $myrow['not_id'] . ", " . $myrow['not_modid'] . ", " . $myrow['not_itemid'] . ", " . $myrow['not_category'] . ", " . $myrow['not_event'] . ", " . $myrow['not_uid'] . ", " . $myrow['not_mode'] . ")";
            $xoops->db()->queryF($sql);
        }
        //Don't drop old table for now
        //$sql = "DROP TABLE " . $xoops->db()->prefix("xoopsnotifications");
        //$xoops->db()->queryF($sql);
    }
    return true;
}