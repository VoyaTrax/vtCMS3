<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: index.php 10567 2012-12-26 20:39:00Z mageg $
 */

include_once 'header.php';
$xoops = Xoops::getInstance();
$xoops->header('page_index.html');
// Parameters
$nb_content = $xoops->getModuleConfig('page_userpager');
// Get handler
$content_Handler = $xoops->getModuleHandler('page_content');
// add module css
$xoTheme->addStylesheet( $xoops->url('/modules/' . $xoops->module->getVar('dirname', 'n') . '/css/styles.css'), null );
// Get start pager
$start = $system->cleanVars($_REQUEST, 'start', 0, 'int');

// Criteria
$criteria = new CriteriaCompo();
$criteria->add(new Criteria('content_status', 0, '!='));
$criteria->add(new Criteria('content_maindisplay', 0, '!='));
$criteria->setSort('content_weight ASC, content_title');
$criteria->setOrder('ASC');
$criteria->setStart($start);
$criteria->setLimit($nb_content);
$content_count = $content_Handler->getCount($criteria);
$content_arr = $content_Handler->getAll($criteria);
// Assign Template variables
$xoops->tpl()->assign('content_count', $content_count);
$keywords = '';
if ($content_count > 0) {
    //Cleaning the content of $content, they are assign by blocks and mess the output
    $xoops->tpl()->assign('content', array());
    foreach (array_keys($content_arr) as $i) {
        $content_id = $content_arr[$i]->getVar('content_id');
        $content['id'] = $content_id;
        $content['title'] = $content_arr[$i]->getVar('content_title');
        $content['shorttext'] = $content_arr[$i]->getVar('content_shorttext');
        $content['authorid'] = $content_arr[$i]->getVar('content_author');
        $content['author'] = XoopsUser::getUnameFromId($content_arr[$i]->getVar('content_author'));
        $content['date'] = XoopsLocal::formatTimestamp($content_arr[$i]->getVar('content_create'), $xoops->getModuleConfig('page_dateformat'));
        $content['time'] = XoopsLocal::formatTimestamp($content_arr[$i]->getVar('content_create'), $xoops->getModuleConfig('page_timeformat'));
        $xoops->tpl()->append_by_ref('content', $content);
        $keywords .= $content_arr[$i]->getVar('content_title') . ',';
        unset($content);
    }
    // Display Page Navigation
    if ($content_count > $nb_content) {
        $nav = new XoopsPageNav($content_count, $nb_content, $start, 'start');
        $xoops->tpl()->assign('nav_menu', $nav->renderNav(4));
    }
} else {
    $xoops->tpl()->assign('error_message', _AM_PAGE_CONTENT_ERROR_NOCONTENT);
}
// r�f�rencement
//description
$xoTheme->addMeta('meta', 'description', strip_tags($xoops->module->name()));
//keywords
$keywords = substr($keywords,0,-1);
$xoTheme->addMeta('meta', 'keywords', $keywords);

$xoops->footer();