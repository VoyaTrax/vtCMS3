<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: header.php 10321 2012-12-05 19:25:07Z trabis $
 */

include dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'mainfile.php';
include_once dirname(__FILE__). DIRECTORY_SEPARATOR . 'class'. DIRECTORY_SEPARATOR . 'page.php';

$xoops = Xoops::getInstance();
XoopsLoad::load('system', 'system');
$xoops->loadLanguage('admin', $xoops->module->getVar('dirname', 'e'));
// Get main instance
$system = System::getInstance();

//permission
$gperm_handler = $xoops->getHandler('groupperm');
if ($xoops->isUser()) {
    $groups = $xoops->user->getGroups();
} else {
    $groups = XOOPS_GROUP_ANONYMOUS;
}