<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: admin.php 10724 2013-01-09 19:45:15Z mageg $
 */
//generic
define("_AM_PAGE_ACTION","Action");
define("_AM_PAGE_VIEW","View");
define("_AM_PAGE_DELETE","Delete");
define("_AM_PAGE_EDIT","Edit");
define("_AM_PAGE_DBUPDATED","Database updated successfully!");
define("_AM_PAGE_OFF","Enabled");
define("_AM_PAGE_ON","Deactivated");
define("_AM_PAGE_CLONE","Clone");
define("_AM_PAGE_SUREDEL","Are you sure you want to delete : <span style='color : Red'>%s</span>");

//index.php
define("_AM_PAGE_INDEX_TOTALCONTENT","There are %s contents in our database");
define("_AM_PAGE_INDEX_TOTALDISPLAY","There are %s visible contents");
define("_AM_PAGE_INDEX_TOTALNOTDISPLAY","There are %s contents not visible");

//content.php
define("_AM_PAGE_CONTENT_ADD","Add a new content");
define("_AM_PAGE_CONTENT_EDIT","Edit a content");
define("_AM_PAGE_CONTENT_LIST","List of contents");
define("_AM_PAGE_CONTENT_ERROR_NOCONTENT","There are no contents");
define("_AM_PAGE_CONTENT_TITLE","Title");
define("_AM_PAGE_CONTENT_SHORTTEXT","Short text");
define("_AM_PAGE_CONTENT_TEXT","Text");
define("_AM_PAGE_CONTENT_TEXT_DSC","Main content of the page");
//define("_AM_PAGE_CONTENT_CATEGORY","Category");
//define("_AM_PAGE_CONTENT_PAGE","Related article");
//define("_AM_PAGE_CONTENT_PAGEID","Or ID");
//define("_AM_PAGE_CONTENT_PAGEID_DSC","Put id...");
define("_AM_PAGE_CONTENT_AUTHOR","Posted by");
define("_AM_PAGE_CONTENT_MKEYWORD","Metas keyword");
define("_AM_PAGE_CONTENT_MKEYWORD_DSC","Metas keyword separated by a comma");
define("_AM_PAGE_CONTENT_MDESCRIPTION","Metas description");
define("_AM_PAGE_CONTENT_OPTIONS","Display options");
define("_AM_PAGE_CONTENT_OPTIONS_DSC","Choose which information will be displayed");
define("_AM_PAGE_CONTENT_DOPDF","PDF icon");
define("_AM_PAGE_CONTENT_DOPRINT","Print icon");
define("_AM_PAGE_CONTENT_DOSOCIAL","Social networks");
define("_AM_PAGE_CONTENT_DOAUTHOR","Author");
define("_AM_PAGE_CONTENT_DOMAIL","Mail icon");
define("_AM_PAGE_CONTENT_DODATE","Date");
define("_AM_PAGE_CONTENT_DOHITS","Hits");
define("_AM_PAGE_CONTENT_DORATING","Rating and vote count");
define("_AM_PAGE_CONTENT_DOCOMS","Comments");
define("_AM_PAGE_CONTENT_DONCOMS","Comments count");
define("_AM_PAGE_CONTENT_DOTITLE","Title");
define("_AM_PAGE_CONTENT_DONOTIFICATIONS","Notifications");
define("_AM_PAGE_CONTENT_ID","ID");
define("_AM_PAGE_CONTENT_STATUS","Active");
define("_AM_PAGE_CONTENT_HITS","Hits");
define("_AM_PAGE_CONTENT_RATING","Rating");
define("_AM_PAGE_CONTENT_DATEUPDATE","Update the date");
define("_AM_PAGE_CONTENT_DATEUPDATE_NO","No");
define("_AM_PAGE_CONTENT_DATEUPDATE_YES","Yes");
define("_AM_PAGE_CONTENT_PERMISSION","Select groups that can view this content");
define("_AM_PAGE_CONTENT_COPY","[copy]");
define("_AM_PAGE_CONTENT_WEIGHT","Weight");
define("_AM_PAGE_CONTENT_ERROR_WEIGHT","You need a positive integer");
define("_AM_PAGE_CONTENT_MAINDISPLAY","Content displayed on the home page");
define("_AM_PAGE_CONTENT_MAINDISPLAY_SHORT","Home page displayed");

//related.php
define("_AM_PAGE_RELATED_ADD","Add a new related content");
define("_AM_PAGE_RELATED_EDIT","Edit a related content");
define("_AM_PAGE_RELATED_NAME","Group name");
define("_AM_PAGE_RELATED_NAVIGATION","Navigation type");
define("_AM_PAGE_RELATED_NAVIGATION_OPTION1","Arrow");
define("_AM_PAGE_RELATED_NAVIGATION_OPTION2","Arrow with menu");
define("_AM_PAGE_RELATED_NAVIGATION_OPTION3","Arrow with title content");
define("_AM_PAGE_RELATED_NAVIGATION_OPTION4","Menu");
define("_AM_PAGE_RELATED_NAVIGATION_OPTION5","Title content");
define("_AM_PAGE_RELATED_MENU","Menu");
define("_AM_PAGE_RELATED_MENU_DSC","The menu is a list with the names of related content");
define("_AM_PAGE_RELATED_ERROR_NORELATED","There are no related content");
define("_AM_PAGE_RELATED_ERROR_NOFREECONTENT","There are no free content");
define("_AM_PAGE_RELATED_MAIN","Main content");
define("_AM_PAGE_RELATED_SECONDARY","Secondary content");
define("_AM_PAGE_RELATED_MAIN_DSC",".....");
define("_AM_PAGE_RELATED_DELMAIN","The following groups of related content will be destroyed:");

//permissions.php
define("_AM_PAGE_PERMISSIONS_GLOBAL","Global permissions");
define("_AM_PAGE_PERMISSIONS_GLOBAL_DSC","Select groups that can");
define("_AM_PAGE_PERMISSIONS_GLOBAL_RATE","Rate content");
define("_AM_PAGE_PERMISSIONS_VIEW","View permissions");
//define("_AM_PAGE_PERMISSIONS_VIEW_DSC","Choose group than can view content");

//tips
define("_AM_PAGE_CONTENT_TIPS","<ul><li>Add, update, copy or delete content</li></ul>");
define("_AM_PAGE_RELATED_TIPS","<ul><li>This section allows you to create links between pages together</li></ul>");