<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: modinfo.php 10527 2012-12-23 15:31:52Z mageg $
 */

// Info module
define("_MI_PAGE_NAME", "Page");
define("_MI_PAGE_DESC", "Module for creating pages");
// Menu
define("_MI_PAGE_INDEX", "Index");
define("_MI_PAGE_CONTENT", "Content");
define("_MI_PAGE_RELATED", "Related content");
define("_MI_PAGE_PERMISSIONS", "Permissions");
define("_MI_PAGE_ABOUT", "About");
// Blocks
define("_MI_PAGE_BLOCKS_RECENT","Recent contents");
define("_MI_PAGE_BLOCKS_RECENTDSC","Display recent contents");
define("_MI_PAGE_BLOCKS_HITS","Top contents");
define("_MI_PAGE_BLOCKS_HITSDSC","Display top contents");
define("_MI_PAGE_BLOCKS_RATING","Top rated contents");
define("_MI_PAGE_BLOCKS_RATINGDSC","Display top rated contents");
define("_MI_PAGE_BLOCKS_RANDOM","Random contents");
define("_MI_PAGE_BLOCKS_RANDOMDSC","Display contents randomly");
define("_MI_PAGE_BLOCKS_ID", "ID contents");
define("_MI_PAGE_BLOCKS_IDDSC", "Display contents by ID");

// Preferences
define("_MI_PAGE_PREFERENCE_EDITOR", "Editor");
define("_MI_PAGE_PREFERENCE_ADMINPAGER", "Number contents to display per page in admin page");
define("_MI_PAGE_PREFERENCE_USERPAGER", "Number contents to display per page in user page");
define("_MI_PAGE_PREFERENCE_DATEFORMAT", "Date format");
define("_MI_PAGE_PREFERENCE_TIMEFORMAT", "time format");

// Notifications
define("_MI_PAGE_NOTIFICATION_GLOBAL", "Global Contents");
define("_MI_PAGE_NOTIFICATION_GLOBAL_DSC", "Notification options that apply to all contents.");
define("_MI_PAGE_NOTIFICATION_ITEM", "Content");
define("_MI_PAGE_NOTIFICATION_ITEM_DSC", "Notification options that apply to the current article.");
define("_MI_PAGE_NOTIFICATION_GLOBAL_NEWCONTENT", "New content published");
define("_MI_PAGE_NOTIFICATION_GLOBAL_NEWCONTENT_CAP", "Notify me when any new content is published.");
define("_MI_PAGE_NOTIFICATION_GLOBAL_NEWCONTENT_DSC", "Receive notification when any new content is published.");
define("_MI_PAGE_NOTIFICATION_GLOBAL_NEWCONTENT_SBJ", "[{X_SITENAME}] {X_MODULE} auto-notify : New content published");

