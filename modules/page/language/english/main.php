<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: main.php 10446 2012-12-18 19:57:26Z mageg $
 */
//generic


//index.php
define("_MD_PAGE_INDEX_MORE", "more details");

//viewpage.php
define("_MD_PAGE_VIEWPAGE_NOPAGE","This page does not exist in our database");
define("_MD_PAGE_VIEWPAGE_POSTEDBY","Posted by");
define("_MD_PAGE_VIEWPAGE_ON","on");
define("_MD_PAGE_VIEWPAGE_HITS","(%s reads)");
define("_MD_PAGE_VIEWPAGE_COMMENTS","comments");
define("_MD_PAGE_VIEWPAGE_RATING","Rating");
define("_MD_PAGE_VIEWPAGE_VOTE","votes");
define("_MD_PAGE_VIEWPAGE_SUMMARY","Summary");
define("_MD_PAGE_VIEWPAGE_PRINT","Printer page");
define("_MD_PAGE_VIEWPAGE_PDF","PDF page");

// rating.php
define("_MD_PAGE_RATING_DONOTVOTE","Do not vote for your own files.");
define("_MD_PAGE_RATING_VOTEONCE","Please do not vote for the same resource more than once.");
define("_MD_PAGE_RATING_CONFIRM","Are you sure you want to give the note %s for page '%s'?");
define("_MD_PAGE_RATING_THANKS","Thank you for your vote");

// print.php
define("_MD_PAGE_PRINT","Print");
define("_MD_PAGE_PRINT_COMES","This article comes from");
define("_MD_PAGE_PRINT_URL","The URL for this page is: ");