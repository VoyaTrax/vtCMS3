<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: blocks.php 10574 2012-12-27 20:46:10Z mageg $
 */
define("_MB_PAGE_DISPLAY","Display");
define("_MB_PAGE_CONTENTS","contents");
define("_MB_PAGE_TLENGTH","Title Length");
define("_MB_PAGE_ALLCONTENT","Display all de content");
define("_MB_PAGE_CONTENTDISPLAY","Content to display");