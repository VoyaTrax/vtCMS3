<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page_blocks.php 10618 2013-01-01 14:16:08Z mageg $
 */
function page_blocks_show($options) {
    $xoops = Xoops::getInstance();
    // Get handler
    $content_Handler = $xoops->getModuleHandler('page_content', 'page');
    $xoops->loadLanguage('main', 'page');
    $content = array();
    $xoops->theme()->addStylesheet( $xoops->url('/modules/page/css/styles.css'), null );
    $criteria = new CriteriaCompo();
    $criteria->add(new Criteria('content_status', 0, '!='));

    switch ($options[0]) {
        case 'random':
            $criteria->setSort('RAND()');
            break;
        case 'rating':
            $criteria->setSort('content_rating');
            $criteria->setOrder('DESC');
            break;
        case 'hits':
            $criteria->setSort('content_hits');
            $criteria->setOrder('DESC');
            break;
        case 'recent':
            $criteria->setSort('content_create');
            $criteria->setOrder('DESC');
            break;
    }

    if ($options[0] != 'id') {
        $criteria->setLimit($options[1]);
        $content_arr = $content_Handler->getAll($criteria);
        foreach (array_keys($content_arr) as $i) {
            $content_id = $content_arr[$i]->getVar('content_id');
            $content[$i]['id'] = $content_id;
            $content[$i]['title'] = strlen($content_arr[$i]->getVar('content_title')) > $options[2] ? substr($content_arr[$i]->getVar('content_title'), 0, ($options[2])) . '...' : $content_arr[$i]->getVar('content_title');
            $content[$i]['shorttext'] = $content_arr[$i]->getVar('content_shorttext');
            if ($options[3] == true){
                $content[$i]['text'] = $content_arr[$i]->getVar('content_text');
            }
            $content[$i]['authorid'] = $content_arr[$i]->getVar('content_author');
            $content[$i]['author'] = XoopsUser::getUnameFromId($content_arr[$i]->getVar('content_author'));
            $content[$i]['date'] = XoopsLocal::formatTimestamp($content_arr[$i]->getVar('content_create'), $xoops->getModuleConfig('page_dateformat', 'page'));
            $content[$i]['time'] = XoopsLocal::formatTimestamp($content_arr[$i]->getVar('content_create'), $xoops->getModuleConfig('page_timeformat', 'page'));
            $content[$i]['hits'] = sprintf(_MD_PAGE_VIEWPAGE_HITS, $content_arr[$i]->getVar('content_hits'));
            $content[$i]['rating'] = number_format($content_arr[$i]->getVar('content_rating'), 1);
            $content[$i]['votes'] = $content_arr[$i]->getVar('content_votes');
        }
    } else {
        $content_id = $options[1];
        $view_content = $content_Handler->get($content_id);
        // Test if the page exist
        if (count($view_content) == 0 || $view_content->getVar('content_status') == 0){
            $content = array();
        } else {
            $request = Xoops_Request::getInstance();
            $url = $request->getUrl();
            // add module css
            $xoops->theme()->addStylesheet( $xoops->url('/modules/page/css/styles.css'), null );
            $xoops->theme()->addStylesheet( $xoops->url('/modules/page/css/rating.css'), null );
            // Javascript
            $xoops->theme()->addScript( XOOPS_URL . '/modules/page/js/rating.js' );
            $xoops->theme()->addScript( XOOPS_URL . '/modules/page/js/MetaData.js' );
            $xoops->theme()->addScript( XOOPS_URL . '/modules/page/js/rating.pack.js' );
            //Rating form
            $content_rating = $view_content->getVar('content_rating');
            $rating_form = '<form id="rating" action="' . XOOPS_URL . '/modules/page/rating.php">';
            for($i=0.5; $i <= 6;){
                if ($content_rating >= ($i - 0.25) && $content_rating < ($i + 0.25)){
                    $rating_form .= '<input name="rating" type="radio" class="star_rating {split:2}" value="' . $i . '" checked="checked"/>';
                }else{
                    $rating_form .= '<input name="rating" type="radio" class="star_rating {split:2}" value="' . $i . '"/>';
                }
                $i += 0.5;
            }
            $rating_form .= '<input name="content_id" type="hidden" id="content_id" value="' . $content_id .'" />';
            $rating_form .= '<input name="extra_url" type="hidden" id="extra_url" value="' . $url .'" />';
            $rating_form .= '</form>';
            $content['rating_form'] = $rating_form;

            // content
            $content['content_id'] = $content_id;
            $content['content_title'] = $view_content->getVar('content_title');
            $content['content_shorttext'] = $view_content->getVar('content_shorttext');
            $content['content_text'] = $view_content->getVar('content_text');
            $content['content_author'] = XoopsUser::getUnameFromId($view_content->getVar('content_author'));
            $content['content_authorid'] = $view_content->getVar('content_author');
            $content['content_hits'] = sprintf(_MD_PAGE_VIEWPAGE_HITS, $view_content->getVar('content_hits'));
            $content['content_date'] = XoopsLocal::formatTimestamp($view_content->getVar('content_create'), $xoops->getModuleConfig('page_dateformat', 'page'));
            $content['content_time'] = XoopsLocal::formatTimestamp($view_content->getVar('content_create'), $xoops->getModuleConfig('page_timeformat', 'page'));
            $content['content_comments'] = $view_content->getVar('content_comments');
            $content['content_rating'] = number_format($view_content->getVar('content_rating'), 1);
            $content['content_votes'] = $view_content->getVar('content_votes');
            $content['content_dopdf'] = $view_content->getVar('content_dopdf');
            $content['content_doprint'] = $view_content->getVar('content_doprint');
            $content['content_dosocial'] = $view_content->getVar('content_dosocial');
            $content['content_doauthor'] = $view_content->getVar('content_doauthor');
            $content['content_dodate'] = $view_content->getVar('content_dodate');
            $content['content_domail'] = $view_content->getVar('content_domail');
            $content['content_dohits'] = $view_content->getVar('content_dohits');
            $content['content_dorating'] = $view_content->getVar('content_dorating');
            $content['content_dotitle'] = $view_content->getVar('content_dotitle');
            $content['content_dosocial'] = $view_content->getVar('content_dosocial');
            $content['content_donotifications'] = $view_content->getVar('content_donotifications');
            $content['content_docoms'] = $view_content->getVar('content_docoms');
            if ($view_content->getVar('content_docoms') == false){
                $ncoms = false;
            } elseif ($view_content->getVar('content_doncoms') == false){
                $ncoms = false;
            } else {
                $ncoms = true;
            }
            $content['content_doncoms'] = $ncoms;
            if ($view_content->getVar('content_dotitle') == false && $view_content->getVar('content_dorating') == false) {
                $header = false;
            } else {
                $header = true;
            }
            $content['content_doheader'] = $header;
            if ($view_content->getVar('content_doauthor') == false && $view_content->getVar('content_dodate') == false && $view_content->getVar('content_dohits') == false && $ncoms == false) {
                $footer = false;
            } else {
                $footer = true;
            }
            $content['content_dofooter'] = $footer;

            include_once XOOPS_ROOT_PATH. '/modules/page/class/page.php';
            $page = new Page();

            $related = $page->menu_related($content_id);

            if ($related['domenu'] ) {
                $content['summary'] = $related['summary'];
            }
            if ($related['navigation'] != '' ) {
                $content['navigation'] = $related['navigation'];
                $content['related'] = $related;
            }
        }
    }
    return $content;
}

function page_blocks_edit($options) {
    $form = '';
    if ($options[0] != 'id') {
        $form .= _MB_PAGE_DISPLAY . "&nbsp;\n";
        $form .= "<input type=\"hidden\" name=\"options[0]\" value=\"" . $options[0] . "\" />\n";
        $form .= "<input class=\"span1\" name=\"options[1]\" size=\"5\" maxlength=\"255\" value=\"" . $options[1] . "\" type=\"text\" pattern=\"^\d+$\" />&nbsp;" . _MB_PAGE_CONTENTS . "<br />\n";
        $form .= _MB_PAGE_TLENGTH . "&nbsp;<input class=\"span1\" name=\"options[2]\" size=\"5\" maxlength=\"255\" value=\"" . $options[2] . "\" type=\"text\" pattern=\"^\d+$\" /><br />\n";
        if ($options[3] == false){
            $checked_yes = '';
            $checked_no = 'checked="checked"';
        }else{
            $checked_yes = 'checked="checked"';
            $checked_no = '';
        }
        $form .= _MB_PAGE_ALLCONTENT . ": <input name=\"options[3]\" value=\"1\" type=\"radio\" " . $checked_yes . "/>" . _YES . "&nbsp;\n";
        $form .= "<input name=\"options[3]\" value=\"0\" type=\"radio\" " . $checked_no . "/>" . _NO . "<br />\n";
    } else {
        $xoops = Xoops::getInstance();
        $content_Handler = $xoops->getModuleHandler('page_content', 'page');
        $form .= _MB_PAGE_CONTENTDISPLAY . "&nbsp;\n";
        $form .= "<input type=\"hidden\" name=\"options[0]\" value=\"" . $options[0] . "\" />\n";
        $form .= "<select class=\"span2\" name=\"options[1]\">\n";
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('content_status', 0, '!='));
        $criteria->setSort('content_title');
        $criteria->setOrder('ASC');
        $content_arr = $content_Handler->getAll($criteria);
        foreach (array_keys($content_arr) as $i) {
            if ($content_arr[$i]->getVar('content_id') == $options[1]){
                $form .= "<option value=\"" . $content_arr[$i]->getVar('content_id') . "\" selected=\"selected\">" . $content_arr[$i]->getVar('content_title') . "</option>\n";
            } else {
                $form .= "<option value=\"" . $content_arr[$i]->getVar('content_id') . "\">" . $content_arr[$i]->getVar('content_title') . "</option>\n";
            }
        }
        $form .= "</select>\n";
    }
    return $form;
}