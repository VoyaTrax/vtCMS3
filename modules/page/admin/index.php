<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: index.php 10546 2012-12-25 09:54:00Z mageg $
 */
include dirname(__FILE__) . '/header.php';
// heaser
$xoops = Xoops::getInstance();
$xoops->header();
// Get page handler
$content_Handler = $xoops->getModuleHandler('page_content');

// content not display
$criteria = new CriteriaCompo();
$criteria->add(new Criteria('content_status', 0));
$content_notdisplay = $content_Handler->getCount($criteria);
// content display
$criteria = new CriteriaCompo();
$criteria->add(new Criteria('content_status', 0, '!='));
$content_display = $content_Handler->getCount($criteria);

// folder path
//$folder_path = XOOPS_ROOT_PATH . '/uploads/page';

$admin_page = new XoopsModuleAdmin();
$admin_page->displayNavigation('index.php');

// content
$admin_page->addInfoBox(_MI_PAGE_CONTENT, 'content');
$admin_page->addInfoBoxLine(sprintf(_AM_PAGE_INDEX_TOTALCONTENT, $content_notdisplay + $content_display), 'content');
$admin_page->addInfoBoxLine(sprintf(_AM_PAGE_INDEX_TOTALDISPLAY, '<span class="green">' . $content_display . '</span>'), 'content');
$admin_page->addInfoBoxLine(sprintf(_AM_PAGE_INDEX_TOTALNOTDISPLAY, '<span class="red">' . $content_notdisplay . '</span>'), 'content');

// extension

$admin_page->addConfigBoxLine(array('comments', 'warning'), 'extension');
$admin_page->addConfigBoxLine(array('notifications', 'warning'), 'extension');
$admin_page->addConfigBoxLine(array('pdf', 'warning'), 'extension');
$admin_page->addConfigBoxLine(array('xoosocialnetwork', 'warning'), 'extension');

$admin_page->displayIndex();
$xoops->footer();