<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: content.php 10567 2012-12-26 20:39:00Z mageg $
 */
include dirname(__FILE__) . '/header.php';
// Get main instance
$system = System::getInstance();
$xoops = Xoops::getInstance();
// Parameters
$nb_content = $xoops->getModuleConfig('page_adminpager');
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'default', 'string');
// Get handler
$content_Handler = $xoops->getModuleHandler('page_content');
$related_Handler = $xoops->getModuleHandler('page_related');
$gperm_handler = $xoops->getHandler('groupperm');
// Call header
$xoops->header('page_admin_content.html');
// Get start pager
$start = $system->cleanVars($_REQUEST, 'start', 0, 'int');

$admin_page = new XoopsModuleAdmin();
$admin_page->renderNavigation('content.php');

switch ($op) {

    case 'list':
    default:
        // Add Scripts
        $xoops->theme()->addScript('media/xoops/xoops.js');
        $admin_page->addTips(_AM_PAGE_CONTENT_TIPS);
        $admin_page->addItemButton(_AM_PAGE_CONTENT_ADD, 'content.php?op=new', 'add');
        $admin_page->renderTips();
        $admin_page->renderButton();

        // Criteria
        $criteria = new CriteriaCompo();
        $criteria->setSort('content_weight ASC, content_title');
        $criteria->setOrder('ASC');
        $criteria->setStart($start);
        $criteria->setLimit($nb_content);
        $content_count = $content_Handler->getCount($criteria);
        $content_arr = $content_Handler->getAll($criteria);
        // Assign Template variables
        $xoops->tpl()->assign('content_count', $content_count);
        if ($content_count > 0) {
            foreach (array_keys($content_arr) as $i) {
                $content_id = $content_arr[$i]->getVar('content_id');
                $content['id'] = $content_id;
                $content['title'] = $content_arr[$i]->getVar('content_title');
                $content['maindisplay'] = $content_arr[$i]->getVar('content_maindisplay');
                $content['weight'] = $content_arr[$i]->getVar('content_weight');
                $content['status'] = $content_arr[$i]->getVar('content_status');
                $content['hits'] = $content_arr[$i]->getVar('content_hits');
                $content['rating'] = number_format($content_arr[$i]->getVar('content_rating'), 1);
                $xoops->tpl()->append_by_ref('content', $content);
                unset($content);
            }
            // Display Page Navigation
            if ($content_count > $nb_content) {
                $nav = new XoopsPageNav($content_count, $nb_content, $start, 'start');
                $xoops->tpl()->assign('nav_menu', $nav->renderNav(4, 'small'));
            }
        } else {
            $xoops->tpl()->assign('error_message', _AM_PAGE_CONTENT_ERROR_NOCONTENT);
        }
        break;

    case 'new':
        $admin_page->addItemButton(_AM_PAGE_CONTENT_LIST, 'content.php', 'application-view-detail');
        $admin_page->renderButton();
        $obj = $content_Handler->create();
        $form = $xoops->getModuleForm($obj, 'page_content');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'edit':
        $admin_page->addItemButton(_AM_PAGE_CONTENT_LIST, 'content.php', 'application-view-detail');
        $admin_page->addItemButton(_AM_PAGE_CONTENT_ADD, 'content.php?op=new', 'add');
        $admin_page->renderButton();
        // Create form
        $obj = $content_Handler->get($system->cleanVars($_REQUEST, 'id', 0, 'int'));
        $form = $xoops->getModuleForm($obj, 'page_content');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'save':
        if (!$xoops->security()->check()) {
            $xoops->redirect("content.php", 3, implode(",", $xoops->security()->getErrors()));
        }
        $content_id = $system->cleanVars($_REQUEST, 'content_id', 0, 'int');
        if ($content_id > 0) {
            $obj = $content_Handler->get($content_id);
            if ($_POST["date_update"] == 'Y'){
                $obj->setVar("content_create", strtotime($_POST["content_create"]));
            }
        } else {
            $obj = $content_Handler->create();
            $obj->setVar("content_create", time());
        }
        $error_message = '';
        $error = false;

        $obj->setVar("content_title", $_POST["content_title"]);
        $obj->setVar("content_shorttext", $_POST["content_shorttext"]);
        $obj->setVar("content_text", $_POST["content_text"]);
        $obj->setVar("content_mkeyword", $_POST["content_mkeyword"]);
        $obj->setVar("content_mdescription", $_POST["content_mdescription"]);
        $obj->setVar("content_author", $_POST["content_author"]);
        $obj->setVar("content_status", $_POST["content_status"]);
        $obj->setVar("content_maindisplay", $_POST["content_maindisplay"]);
        $obj->setVar("content_dopdf", 0);
        $obj->setVar("content_doprint", 0);
        $obj->setVar("content_domail", 0);
        $obj->setVar("content_doauthor", 0);
        $obj->setVar("content_dodate", 0);
        $obj->setVar("content_dohits", 0);
        $obj->setVar("content_dorating", 0);
        $obj->setVar("content_doncoms", 0);
        $obj->setVar("content_docoms", 0);
        $obj->setVar("content_dosocial", 0);
        $obj->setVar("content_dotitle", 0);
        $obj->setVar("content_donotifications", 0);
        if (isset($_POST["content_option"])) {
            $content_option = $_POST["content_option"];
            foreach (array_keys($content_option) as $i) {
                switch ($content_option[$i]) {
                    case 'pdf':
                        $obj->setVar("content_dopdf", 1);
                        break;
                    case 'print':
                        $obj->setVar("content_doprint", 1);
                        break;
                    case 'mail':
                        $obj->setVar("content_domail", 1);
                        break;
                    case 'author':
                        $obj->setVar("content_doauthor", 1);
                        break;
                    case 'date':
                        $obj->setVar("content_dodate", 1);
                        break;
                    case 'hits':
                        $obj->setVar("content_dohits", 1);
                        break;
                    case 'rating':
                        $obj->setVar("content_dorating", 1);
                        break;
                    case 'coms':
                        $obj->setVar("content_docoms", 1);
                        break;
                    case 'ncoms':
                        $obj->setVar("content_doncoms", 1);
                        break;
                    case 'social':
                        $obj->setVar("content_dosocial", 1);
                        break;
                    case 'title':
                        $obj->setVar("content_dotitle", 1);
                        break;
                    case 'notifications':
                        $obj->setVar("content_donotifications", 1);
                        break;
                }
            }
        }
        if (preg_match('/^\d+$/', $_POST["content_weight"]) == false){
            $error = true;
            $error_message .= _AM_PAGE_CONTENT_ERROR_WEIGHT . '<br />';
            $obj->setVar("content_weight", '');
        } else {
            $obj->setVar("content_weight", $_POST["content_weight"]);
        }
        if ($error == true){
            $xoops->tpl()->assign('error_message', $error_message);
        } else {
            if ($content_Handler->insert($obj)) {
                $newcontent_id = $obj->get_new_id();
                $perm_id = isset($_REQUEST['content_id']) ? $content_id : $newcontent_id;
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('gperm_itemid', $perm_id, '='));
                $criteria->add(new Criteria('gperm_modid', $xoops->module->getVar('mid'),'='));
                $criteria->add(new Criteria('gperm_name', 'page_view_item', '='));
                $gperm_handler->deleteAll($criteria);
                //permissions view
                if(isset($_POST['groups_view_item'])) {
                    foreach($_POST['groups_view_item'] as $onegroup_id) {
                        $gperm_handler->addRight('page_view_item', $perm_id, $onegroup_id, $xoops->module->getVar('mid'));
                    }
                }
                //notifications
                if ($content_id == 0) {
                    if ($xoops->isActiveModule('notifications')) {
                        $notification_handler = Notifications::getInstance()->getNotificationHandler();
                        $tags = array();
                        $tags['MODULE_NAME'] = 'page';
                        $tags['ITEM_NAME'] = $_POST["content_title"];
                        $tags['ITEM_URL'] = XOOPS_URL . '/modules/page/viewpage.php?id=' . $newcontent_id;
                        $notification_handler->triggerEvent('global', 0, 'newcontent', $tags);
                        $notification_handler->triggerEvent('item', $newcontent_id, 'newcontent', $tags);
                    }
                }
                $xoops->redirect("content.php", 2, _AM_PAGE_DBUPDATED);
            }
            $xoops->error($obj->getHtmlErrors());
        }
        $form = $xoops->getModuleForm($obj, 'page_content');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'delete':
        $admin_page->addItemButton(_AM_PAGE_CONTENT_LIST, 'content.php', 'application-view-detail');
        $admin_page->addItemButton(_AM_PAGE_CONTENT_ADD, 'content.php?op=new', 'add');
        $admin_page->renderButton();
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        $content_id = $system->cleanVars($_REQUEST, 'id', 0, 'int');
        $obj = $content_Handler->get($content_id);
        if (isset($_POST["ok"]) && $_POST["ok"] == 1) {
            if (!$xoops->security()->check()) {
                $xoops->redirect("content.php", 3, implode(",", $xoops->security()->getErrors()));
            }
            // Deleting the content
            if ($content_Handler->delete($obj)) {
                // deleting permissions
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('gperm_itemid', $content_id, '='));
                $criteria->add(new Criteria('gperm_modid', $xoops->module->getVar('mid'),'='));
                $criteria->add(new Criteria('gperm_name', 'page_view_item', '='));
                $gperm_handler->deleteAll($criteria);
                // deleting secondary
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('related_group', 0, '!='));
                $criteria->add(new Criteria('related_contentid', $content_id));
                $related_Handler->deleteAll($criteria);
                // deleting main and secondary
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('related_group', 0));
                $criteria->add(new Criteria('related_contentid', $content_id));
                $related_arr = $related_Handler->getAll($criteria);
                if (count($related_arr) > 0) {
                    foreach (array_keys($related_arr) as $i) {
                        $obj_related = $related_Handler->get($related_arr[$i]->getVar("related_id"));
                        $related_Handler->delete($obj_related);
                        $criteria = new CriteriaCompo();
                        $criteria->add(new Criteria('related_group', $related_arr[$i]->getVar("related_id")));
                        $related_Handler->deleteAll($criteria);
                    }
                }
                $xoops->redirect("content.php", 2, _AM_PAGE_DBUPDATED);
            } else {
                $xoops->error($obj->getHtmlErrors());
            }
        } else {
            // deleting main and secondary
            $related_name = '';
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('related_group', 0));
            $criteria->add(new Criteria('related_contentid', $content_id));
            $related_arr = $related_Handler->getAll($criteria);
            if (count($related_arr) > 0) {
                $related_name .= '<br />' . _AM_PAGE_RELATED_DELMAIN . '<br />';
                foreach (array_keys($related_arr) as $i) {
                    $related_name .= '<span style="color : Red">';
                    $related_name .= $related_arr[$i]->getVar("related_name");
                    $related_name .= '</span>';
                    $related_name .= '<br />';
                }
            }
            $xoops->confirm(array("ok" => 1, "id" => $content_id, "op" => "delete"), 'content.php', sprintf(_AM_PAGE_SUREDEL, $obj->getvar('content_title')) . '<br />' . $related_name);
        }
        break;

    case 'update_status':
        $content_id = $system->cleanVars($_POST, 'content_id', 0, 'int');
        if ($content_id > 0) {
            $obj = $content_Handler->get($content_id);
            $old = $obj->getVar('content_status');
            $obj->setVar('content_status', !$old);
            if ($content_Handler->insert($obj)) {
                exit;
            }
            echo $obj->getHtmlErrors();
        }
        break;

    case 'update_display':
        $content_id = $system->cleanVars($_POST, 'content_id', 0, 'int');
        if ($content_id > 0) {
            $obj = $content_Handler->get($content_id);
            $old = $obj->getVar('content_maindisplay');
            $obj->setVar('content_maindisplay', !$old);
            if ($content_Handler->insert($obj)) {
                exit;
            }
            echo $obj->getHtmlErrors();
        }
        break;

    case 'clone':
        $content_id = $system->cleanVars($_REQUEST, 'id', 0, 'int');
        $content = $content_Handler->get($content_id);
        $obj = $content_Handler->create();
        $obj->setVar("content_title", _AM_PAGE_CONTENT_COPY . $content->getVar("content_title"));
        $obj->setVar("content_weight", 0);
        $obj->setVar("content_create", time());
        $obj->setVar("content_shorttext", $content->getVar("content_shorttext"));
        $obj->setVar("content_text", $content->getVar("content_text"));
        $obj->setVar("content_mkeyword", $content->getVar("content_mkeyword"));
        $obj->setVar("content_mdescription", $content->getVar("content_mdescription"));
        $obj->setVar("content_author", $content->getVar("content_author"));
        $obj->setVar("content_status", $content->getVar("content_status"));
        $obj->setVar("content_maindisplay", $content->getVar("content_maindisplay"));
        $obj->setVar("content_dopdf", $content->getVar("content_dopdf"));
        $obj->setVar("content_doprint", $content->getVar("content_doprint"));
        $obj->setVar("content_domail", $content->getVar("content_domail"));
        $obj->setVar("content_doauthor", $content->getVar("content_doauthor"));
        $obj->setVar("content_dodate", $content->getVar("content_dodate"));
        $obj->setVar("content_dohits", $content->getVar("content_dohits"));
        $obj->setVar("content_dorating", $content->getVar("content_dorating"));
        $obj->setVar("content_docoms", $content->getVar("content_docoms"));
        $obj->setVar("content_doncoms", $content->getVar("content_doncoms"));
        $obj->setVar("content_dosocial", $content->getVar("content_dosocial"));
        $obj->setVar("content_dotitle", $content->getVar("content_dotitle"));
        $obj->setVar("content_donotifications", $content->getVar("content_donotifications"));
        if ($content_Handler->insert($obj)) {
            $newcontent_id = $obj->get_new_id();
            $gperm_handler = $xoops->getHandler('groupperm');
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('gperm_itemid', $content_id, '='));
            $criteria->add(new Criteria('gperm_modid', $xoops->module->getVar('mid'),'='));
            $criteria->add(new Criteria('gperm_name', 'page_view_item', '='));
            $gperm_arr = $gperm_handler->getall($criteria);
            //permissions view
            foreach (array_keys($gperm_arr) as $i) {
                $gperm_handler->addRight('page_view_item', $newcontent_id, $gperm_arr[$i]->getVar("gperm_groupid"), $xoopsModule->getVar('mid'));
            }
            $xoops->redirect("content.php", 2, _AM_PAGE_DBUPDATED);
        }
        $xoops->error($obj->getHtmlErrors());

        break;
}
$xoops->footer();