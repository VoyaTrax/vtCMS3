<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: related.php 10328 2012-12-07 00:56:07Z trabis $
 */
include dirname(__FILE__) . '/header.php';
// Get main instance
$system = System::getInstance();
$xoops = Xoops::getInstance();
// Parameters
$nb_related = $xoops->getModuleConfig('page_adminpager');
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'default', 'string');
// Get handler
$related_Handler = $xoops->getModuleHandler('page_related');
$content_Handler = $xoops->getModuleHandler('page_content');
// Call header
$xoops->header('page_admin_related.html');
// Get start pager
$start = $system->cleanVars($_REQUEST, 'start', 0, 'int');

$admin_page = new XoopsModuleAdmin();
$admin_page->renderNavigation('related.php');

switch ($op) {

    case 'list':
    default:
        // Define Stylesheet
        $xoops->theme()->addStylesheet('media/jquery/ui/' . $xoops->getModuleConfig('jquery_theme', 'system') . '/ui.all.css');
        // Define scripts
        $xoops->theme()->addScript('media/xoops/xoops.js');
        $xoops->theme()->addScript($xoops->url('/media/jquery/ui/jquery.ui.js'));
        $xoops->theme()->addScript('modules/system/js/admin.js');

        $admin_page->addTips(_AM_PAGE_RELATED_TIPS);
        $admin_page->addItemButton(_AM_PAGE_RELATED_ADD, 'related.php?op=new', 'add');
        $admin_page->renderTips();
        $admin_page->renderButton();

        //joint
        $criteria = new CriteriaCompo();
        $criteria->setSort('related_name');
        $criteria->setOrder('ASC');
        $criteria->setStart($start);
        $criteria->setLimit($nb_related);
        $criteria->add(new Criteria('related_name', '/', '!='));
        $related_Handler->table_link = $related_Handler->db->prefix('page_content');
        $related_Handler->field_link = 'content_id'; // champ de la table en jointure
        $related_Handler->field_object = 'related_contentid'; // champ de la table courante
        $related_arr = $related_Handler->getByLink($criteria);
        $related_count = $related_Handler->getCount($criteria);
        // Assign Template variables
        $xoops->tpl()->assign('related_count', $related_count);
        if ($related_count > 0) {
            foreach (array_keys($related_arr) as $i) {
                $related_id = $related_arr[$i]->getVar("related_id");
                $related['id'] = $related_id;
                $related['name'] = $related_arr[$i]->getVar("related_name");
                $related['main_name'] = $related_arr[$i]->getVar("content_title");
                $related['main_contentid'] = $related_arr[$i]->getVar("related_contentid");
                $related['domenu'] = $related_arr[$i]->getVar("related_domenu");
                switch ($related_arr[$i]->getVar("related_navigation")) {
                    case 1:
                    default:
                        $related['navigation'] = _AM_PAGE_RELATED_NAVIGATION_OPTION1;
                        break;
                    case 2:
                        $related['navigation'] = _AM_PAGE_RELATED_NAVIGATION_OPTION2;
                        break;
                    case 3:
                        $related['navigation'] = _AM_PAGE_RELATED_NAVIGATION_OPTION3;
                        break;
                    case 4:
                        $related['navigation'] = _AM_PAGE_RELATED_NAVIGATION_OPTION4;
                        break;
                    case 5:
                        $related['navigation'] = _AM_PAGE_RELATED_NAVIGATION_OPTION5;
                        break;
                }
                //joint
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('related_group', $related_arr[$i]->getVar("related_contentid")));
                $criteria->setSort('related_weight');
                $criteria->setOrder('ASC');
                $related_Handler->table_link = $related_Handler->db->prefix('page_content');
                $related_Handler->field_link = 'content_id'; // champ de la table en jointure
                $related_Handler->field_object = 'related_contentid'; // champ de la table courante
                $secondary_arr = $related_Handler->getByLink($criteria);
                $related['secondary'] = '';
                if (count($secondary_arr) != 0) {

                        $related['secondary'] .= '<ul>';
                        foreach (array_keys($secondary_arr) as $j) {
                            if ($secondary_arr[$j]->getVar("related_name") == '/') {
                                $related['secondary'] .= '<li><a href="content.php?op=edit&amp;id=' . $secondary_arr[$j]->getVar("related_contentid") . '" title="' . _AM_PAGE_EDIT . '">' . $secondary_arr[$j]->getVar("content_title") . '</a></li>';
                            }
                        }
                        $related['secondary'] .= '</ul>';
                }
                $xoops->tpl()->append_by_ref('related', $related);
                $xoops->tpl()->append_by_ref('popup_related', $related);
                unset($related);
            }
            // Display Page Navigation
            if ($related_count > $nb_related) {
                $nav = new XoopsPageNav($related_count, $nb_related, $start, 'start');
                $xoops->tpl()->assign('nav_menu', $nav->renderNav(4));
            }
        } else {
            $xoops->tpl()->assign('error_message', _AM_PAGE_RELATED_ERROR_NORELATED);
        }
        break;

    case 'new':
        if ($related_Handler->getCount() == $content_Handler->getCount()) {
            $xoops->tpl()->assign('error_message', _AM_PAGE_RELATED_ERROR_NOFREECONTENT);
        } else {
            $admin_page->addItemButton(_AM_PAGE_CONTENT_LIST, 'related.php', 'application-view-detail');
            $admin_page->renderButton();
            $obj = $related_Handler->create();
            $form = $xoops->getModuleForm($obj, 'page_related');
            $xoops->tpl()->assign('form', $form->render());
        }
        break;

    case 'edit':
        $admin_page->addItemButton(_AM_PAGE_CONTENT_LIST, 'related.php', 'application-view-detail');
        $admin_page->addItemButton(_AM_PAGE_CONTENT_ADD, 'related.php?op=new', 'add');
        $admin_page->renderButton();
        // Create form
        $obj = $related_Handler->get($system->cleanVars($_REQUEST, 'id', 0, 'int'));
        $form = $xoops->getModuleForm($obj, 'page_related');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'save':
        if (!$xoops->security()->check()) {
            $xoops->redirect("related.php", 3, implode(",", $xoops->security()->getErrors()));
        }
        $related_id = $system->cleanVars($_REQUEST, 'related_id', 0, 'int');
        if ($related_id > 0) {
            $obj = $related_Handler->get($related_id);
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('related_group', $obj->getVar('related_group')));
            $criteria->add(new Criteria('related_name', '/'));
            $related_Handler->deleteAll($criteria);
        } else {
            $obj = $related_Handler->create();
        }
        //main
        $obj->setVar("related_name", $_POST["related_name"]);
        $obj->setVar("related_group", $_POST["related_main"]);
        $obj->setVar("related_contentid", $_POST["related_main"]);
        $obj->setVar("related_domenu", $_POST["related_domenu"]);
        $obj->setVar("related_navigation", $_POST["related_navigation"]);
        if ($related_Handler->insert($obj)) {
            //secondary
            $secondary_max = 10;
            for ($i = 1; $i <= $secondary_max; $i++) {
                if ($_POST["related_secondary" . $i] != 0 && $_POST["related_secondary" . $i] != $_POST["related_main"]) {
                    $obj = $related_Handler->create();
                    $obj->setVar("related_name", "/");
                    $obj->setVar("related_group", $_POST["related_main"]);
                    $obj->setVar("related_contentid", $_POST["related_secondary" . $i]);
                    $obj->setVar("related_domenu", $_POST["related_domenu"]);
                    $obj->setVar("related_navigation", $_POST["related_navigation"]);
                    $obj->setVar("related_weight", $i);
                    $related_Handler->insert($obj);
                    unset($obj);
                }
            }
            $xoops->redirect("related.php", 2, _AM_PAGE_DBUPDATED);
        } else {
            $xoops->error($obj->getHtmlErrors());
        }
        $form = $xoops->getModuleForm($obj, 'page_related');
        $xoops->tpl()->assign('form', $form->render());
        break;

    case 'delete':
        $admin_page->addItemButton(_AM_PAGE_CONTENT_LIST, 'related.php', 'application-view-detail');
        $admin_page->addItemButton(_AM_PAGE_CONTENT_ADD, 'related.php?op=new', 'add');
        $admin_page->renderButton();
        // Define Stylesheet
        $xoops->theme()->addStylesheet('modules/system/css/admin.css');
        $related_id = $system->cleanVars($_REQUEST, 'id', 0, 'int');
        $obj = $related_Handler->get($related_id);
        if (isset($_POST["ok"]) && $_POST["ok"] == 1) {
            if (!$xoops->security()->check()) {
                $xoops->redirect("related.php", 3, implode(",", $xoops->security()->getErrors()));
            }
            // Deleting the related
            if ($related_Handler->delete($obj)) {
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('related_group', $related_id));
                $related_Handler->deleteAll($criteria);
                $xoops->redirect("related.php", 2, _AM_PAGE_DBUPDATED);
            } else {
                $xoops->error($obj->getHtmlErrors());
            }
        } else {
            $xoops->confirm(array("ok" => 1, "id" => $related_id, "op" => "delete"), 'related.php', sprintf(_AM_PAGE_SUREDEL, $obj->getvar('related_name') . '<br />'));
        }
        break;

    case 'update_status':
        $related_id = $system->cleanVars($_POST, 'related_id', 0, 'int');
        if ($related_id > 0) {
            $obj = $related_Handler->get($related_id);
            $old = $obj->getVar('related_domenu');
            $obj->setVar('related_domenu', !$old);
            if ($related_Handler->insert($obj)) {
                exit;
            }
            echo $obj->getHtmlErrors();
        }
        break;

    case 'view':
        $related_id = $system->cleanVars($_POST, 'related_id', 0, 'int');
        if ($related_id > 0) {
            $obj = $related_Handler->get($related_id);
            $old = $obj->getVar('related_domenu');
            $obj->setVar('related_domenu', !$old);
            if ($related_Handler->insert($obj)) {
                exit;
            }
            echo $obj->getHtmlErrors();
        }
        break;
}
$xoops->footer();