<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: permissions.php 10527 2012-12-23 15:31:52Z mageg $
 */
include dirname(__FILE__) . '/header.php';
// Get main instance
$system = System::getInstance();
$xoops = Xoops::getInstance();
// Parameters
$nb_content = $xoops->getModuleConfig('page_adminpager');
// Get Action type
$op = $system->cleanVars($_REQUEST, 'op', 'global', 'string');
// Get handler
$content_Handler = $xoops->getModuleHandler('page_content');
$gperm_handler = $xoops->getHandler('groupperm');
// Call header
$xoops->header('page_admin_permissions.html');

// Get start pager
$start = $system->cleanVars($_REQUEST, 'start', 0, 'int');

$admin_page = new XoopsModuleAdmin();
$admin_page->renderNavigation('permissions.php');

$opform = new XoopsSimpleForm('', 'opform', 'permissions.php', 'get');
$op_select = new XoopsFormSelect("", 'op', $op);
$op_select->setExtra('onchange="document.forms.opform.submit()"');
$op_select->addOption('global', _AM_PAGE_PERMISSIONS_GLOBAL);
$op_select->addOption('view', _AM_PAGE_PERMISSIONS_VIEW);
$opform->addElement($op_select);
$xoops->tpl()->assign('form', $opform->render());

$module_id = $xoops->module->getVar('mid');

switch ($op) {

    case 'global':
        default:
        $global_perm_array = array('4' => _AM_PAGE_PERMISSIONS_GLOBAL_RATE);
        $form = new XoopsGroupPermForm(_AM_PAGE_PERMISSIONS_GLOBAL, $module_id, "page_global", _AM_PAGE_PERMISSIONS_GLOBAL_DSC, 'admin/permissions.php', true);
        foreach( $global_perm_array as $perm_id => $perm_name ) {
            $form->addItem($perm_id , $perm_name) ;
        }
        $form->display();
        break;

    case 'view':
        // View permission

        // Add Scripts
        $xoops->theme()->addScript('media/xoops/xoops.js');
        // Criteria
        $criteria = new CriteriaCompo();
        $criteria->setSort('content_weight ASC, content_title');
        $criteria->setOrder('ASC');
        $criteria->setStart($start);
        $criteria->setLimit($nb_content);
        $content_count = $content_Handler->getCount($criteria);
        $content_arr = $content_Handler->getAll($criteria);
        // Assign Template variables
        $xoops->tpl()->assign('content_count', $content_count);
        if ($content_count > 0) {
            $member_handler = $xoops->getHandler('member');
            $group_list = $member_handler->getGroupList();

            $module_id = $xoops->isModule() ? $xoops->module->getVar('mid', 'n') : 1;

            $xoops->tpl()->assign('groups', $group_list);
            foreach (array_keys($content_arr) as $i) {
                $content_id = $content_arr[$i]->getVar("content_id");
                $perms = '';
                $groups_ids_view = $gperm_handler->getGroupIds('page_view_item', $content_id, $module_id);
                $groups_ids_view = array_values($groups_ids_view);
                foreach (array_keys($group_list) as $j) {
                    $perms .= '<img id="loading_display' . $content_id . '_' . $j .'" src="' . $xoops->url('media/xoops/images/spinner.gif') . '" style="display:none;" alt="' . _AM_SYSTEM_LOADING . '" />';
                    if (in_array($j, $groups_ids_view)) {
                        $perms .= "<img class=\"cursorpointer\" id=\"display" . $content_id . "_" . $j . "\" onclick=\"Xoops.changeStatus( 'permissions.php', { op: 'update_view', content_id: " . $content_id . ", group: " . $j . ", status: 'no' }, 'display" . $content_id . "_" . $j ."', 'permissions.php' )\" src=\"" . $xoops->url('modules/system/images/icons/default/success.png') . "\" alt=\"" . _AM_PAGE_OFF . "\" title=\"" . _AM_PAGE_OFF . "\" />";
                    } else {
                        $perms .= "<img class=\"cursorpointer\" id=\"display" . $content_id . "_" . $j . "\" onclick=\"Xoops.changeStatus( 'permissions.php', { op: 'update_view', content_id: " . $content_id . ", group: " . $j . ", status: 'yes' }, 'display" . $content_id . "_" . $j ."', 'permissions.php' )\" src=\"" . $xoops->url('modules/system/images/icons/default/cancel.png') . "\" alt=\"" . _AM_PAGE_ON . "\" title=\"" . _AM_PAGE_ON . "\" />";

                    }
                    $perms .= $group_list[$j] . '<br />';
                }
                $content['id'] = $content_id;
                $content['title'] = $content_arr[$i]->getVar("content_title");
                $content['permissions'] = $perms;
                $xoops->tpl()->append_by_ref('content', $content);
                unset($content);
            }
            // Display Page Navigation
            if ($content_count > $nb_content) {
                $nav = new XoopsPageNav($content_count, $nb_content, $start, 'start', 'op=view');
                $xoops->tpl()->assign('nav_menu', $nav->renderNav(4));
            }
        } else {
            $xoops->tpl()->assign('error_message', _AM_PAGE_CONTENT_ERROR_NOCONTENT);
        }
        break;

    case 'update_view':
        $content_id = $system->cleanVars($_REQUEST, 'content_id', 0, 'int');
        $group = $system->cleanVars($_REQUEST, 'group', 0, 'int');
        $status = $system->cleanVars($_REQUEST, 'status', '', 'string');
        if ($content_id > 0 && $group > 0 && $status != '') {
            if ($status == 'no') {
                // deleting permissions
                $criteria = new CriteriaCompo();
                $criteria->add(new Criteria('gperm_groupid', $group, '='));
                $criteria->add(new Criteria('gperm_itemid', $content_id, '='));
                $criteria->add(new Criteria('gperm_modid', $xoopsModule->getVar('mid'),'='));
                $criteria->add(new Criteria('gperm_name', 'page_view_item', '='));
                $gperm_handler->deleteAll($criteria);
            } else {
                // add permissions
                $gperm_handler->addRight('page_view_item', $content_id, $group, $xoopsModule->getVar('mid'));
            }
        }
        break;
}

$xoops->footer();