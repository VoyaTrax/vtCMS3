<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page.php 10618 2013-01-01 14:16:08Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class page
{
    public function menu_related($content_id)
    {
        $xoops = Xoops::getInstance();
        // Get handler
        $related_Handler = $xoops->getModuleHandler('page_related', 'page');
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('related_contentid', $content_id));
        $criteria->setLimit(1);
        $related_arr = $related_Handler->getAll($criteria);
        foreach (array_keys($related_arr) as $i) {
            $related_group = $related_arr[$i]->getVar('related_group');
            $related_domenu = $related_arr[$i]->getVar('related_domenu');
            $related_navigation = $related_arr[$i]->getVar('related_navigation');
        }
        $save_previous = 0;
        $previous = 0;
        $save_next = false;
        $next = 0;
        $summary = array();
        $navigation_arr = array();
        if (count($related_arr) > 0) {
            unset($related_arr);
            $criteria = new CriteriaCompo();
            $criteria->add(new Criteria('related_group', $related_group));
            $criteria->setSort('related_weight');
            $criteria->setOrder('ASC');
            $related_Handler->table_link = $related_Handler->db->prefix('page_content');
            $related_Handler->field_link = 'content_id'; // champ de la table en jointure
            $related_Handler->field_object = 'related_contentid'; // champ de la table courante
            $related_arr = $related_Handler->getByLink($criteria);
            if (count($related_arr) > 0) {
                foreach (array_keys($related_arr) as $i) {
                    if ($related_arr[$i]->getVar('content_status') != 0) {
                        $navigation_arr[$related_arr[$i]->getVar('related_contentid')] = $related_arr[$i]->getVar('content_title');
                        if ($related_arr[$i]->getVar('related_contentid') == $content_id) {
                            $summary[] = $related_arr[$i]->getVar('content_title');
                            $previous = $save_previous;
                            $save_next = true;
                        } else {
                            $summary[] = '<a href="' . XOOPS_URL . '/modules/page/viewpage.php?id=' . $related_arr[$i]->getVar('related_contentid') . '" title="' . $related_arr[$i]->getVar('content_title') . '">' . $related_arr[$i]->getVar('content_title') . '</a>';
                            if ($save_next == true){
                                $next = $related_arr[$i]->getVar('related_contentid');
                                $save_next = false;
                            }
                        }
                        $save_previous = $related_arr[$i]->getVar('related_contentid');
                    }
                }
            }
        } else {
            $related_navigation = 0;
            $related_domenu = false;
        }

        $return['domenu'] = $related_domenu;
        $return['summary'] = $summary;
        $return['previous_id'] = $previous;
        if ($previous != 0) {
            $return['previous_title'] = $navigation_arr[$previous];
        }
        $return['next_id'] = $next;
        if ($next != 0) {
            $return['next_title'] = $navigation_arr[$next];
        }
        $return['navigation'] = $related_navigation;
        $return['select'] = $navigation_arr;
        return $return;
    }
}