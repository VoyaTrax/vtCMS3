<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page_content.php 10574 2012-12-27 20:46:10Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class PagePage_contentForm extends XoopsThemeForm
{
    /**
     * @param PagePage_content|XoopsObject $obj
     */
    public function __construct(PagePage_content &$obj)
    {
        $xoops = Xoops::getInstance();

        $title = $obj->isNew() ? sprintf( _AM_PAGE_CONTENT_ADD ) : sprintf( _AM_PAGE_CONTENT_EDIT );

        parent::__construct($title, 'form', 'content.php', 'post', true);

        if (!$obj->isNew()) {
            $this->addElement(new XoopsFormHidden('content_id', $obj->getVar('content_id')));
            $content_option = array();
            if ($obj->getVar('content_dopdf') == 1){
                array_push ($content_option, 'pdf');
            }
            if ($obj->getVar('content_doprint') == 1){
                array_push ($content_option, 'print');
            }
            if ($obj->getVar('content_domail') == 1){
                array_push ($content_option, 'mail');
            }
            if ($obj->getVar('content_doauthor') == 1){
                array_push ($content_option, 'author');
            }
            if ($obj->getVar('content_dodate') == 1){
                array_push ($content_option, 'date');
            }
            if ($obj->getVar('content_dohits') == 1){
                array_push ($content_option, 'hits');
            }
            if ($obj->getVar('content_dorating') == 1){
                array_push ($content_option, 'rating');
            }
            if ($obj->getVar('content_docoms') == 1){
                array_push ($content_option, 'coms');
            }
            if ($obj->getVar('content_doncoms') == 1){
                array_push ($content_option, 'ncoms');
            }
            if ($obj->getVar('content_dosocial') == 1){
                array_push ($content_option, 'social');
            }
            if ($obj->getVar('content_dotitle') == 1){
                array_push ($content_option, 'title');
            }
            if ($obj->getVar('content_donotifications') == 1){
                array_push ($content_option, 'notifications');
            }
            $content_status = $obj->getVar('content_status');
            $content_maindisplay = $obj->getVar('content_maindisplay');
            $content_weight = $obj->getVar('content_weight', 'e');
        } else {
            $content_option = array('title', 'author', 'date', 'hits', 'rating', 'coms', 'ncoms', 'notifications', 'pdf', 'print', 'mail', 'social');
            $content_status = 1;
            $content_maindisplay = 1;
            $content_weight = '0';
        }
        //title
        $this->addElement(new XoopsFormText(_AM_PAGE_CONTENT_TITLE, 'content_title', 6, 255, $obj->getVar('content_title'), ''), true);
        //short text
        $editor_configs=array();
        $editor_configs['name'] = 'content_shorttext';
        $editor_configs['value'] = $obj->getVar('content_shorttext', 'e');
        $editor_configs['editor'] = $xoops->getModuleConfig('page_editor');
        $editor_configs['rows'] = 6;
        $editor_configs['cols'] = 8;
        $this->addElement(new XoopsFormEditor(_AM_PAGE_CONTENT_SHORTTEXT, 'content_shorttext', $editor_configs), true);
        //text
        $editor_configs=array();
        $editor_configs['name'] = 'content_text';
        $editor_configs['value'] = $obj->getVar('content_text', 'e');
        $editor_configs['editor'] = $xoops->getModuleConfig('page_editor');
        $editor_configs['rows'] = 12;
        $editor_configs['cols'] = 8;
        $text = new XoopsFormEditor(_AM_PAGE_CONTENT_TEXT, 'content_text', $editor_configs);
        $text->setDescription(_AM_PAGE_CONTENT_TEXT_DSC);
        $this->addElement($text, false);
        //content_mkeyword
        $this->addElement(new XoopsFormTextArea(_AM_PAGE_CONTENT_MKEYWORD, 'content_mkeyword', $obj->getVar('content_mkeyword'), 1, 5, _AM_PAGE_CONTENT_MKEYWORD_DSC));
        //content_mdescription
        $this->addElement(new XoopsFormTextArea(_AM_PAGE_CONTENT_MDESCRIPTION, 'content_mdescription', $obj->getVar('content_mdescription'), 2, 5));
        //Weight
        $weight = new XoopsFormText(_AM_PAGE_CONTENT_WEIGHT, 'content_weight', 1, 5, $content_weight, '');
        $weight->setPattern('^\d+$', _AM_PAGE_CONTENT_ERROR_WEIGHT);
        $this->addElement($weight, true);
        //options
        $checkbox = new XoopsFormCheckbox(_AM_PAGE_CONTENT_OPTIONS, 'content_option', $content_option, true);
        $checkbox->addOption('title', _AM_PAGE_CONTENT_DOTITLE);
        $checkbox->addOption('author', _AM_PAGE_CONTENT_DOAUTHOR);
        $checkbox->addOption('date', _AM_PAGE_CONTENT_DODATE);
        $checkbox->addOption('hits', _AM_PAGE_CONTENT_DOHITS);
        $checkbox->addOption('rating', _AM_PAGE_CONTENT_DORATING);
        if ($xoops->isActiveModule('comments') == true) {
            $checkbox->addOption('coms', _AM_PAGE_CONTENT_DOCOMS);
            $checkbox->addOption('ncoms', _AM_PAGE_CONTENT_DONCOMS);
        }
        if ($xoops->isActiveModule('notifications') == true) {
            $checkbox->addOption('notifications', _AM_PAGE_CONTENT_DONOTIFICATIONS);
        }
        $checkbox->addOption('pdf', _AM_PAGE_CONTENT_DOPDF);
        if ($xoops->isActiveModule('pdf') == true) {
            $checkbox->addOption('print', _AM_PAGE_CONTENT_DOPRINT);
        }
        $checkbox->addOption('mail', _AM_PAGE_CONTENT_DOMAIL);
        if ($xoops->isActiveModule('xoosocialnetwork') == true) {
            $checkbox->addOption('social', _AM_PAGE_CONTENT_DOSOCIAL);
        }
        $checkbox->setDescription(_AM_PAGE_CONTENT_OPTIONS_DSC);
        $this->addElement($checkbox);
        //maindisplay
        $this->addElement(new XoopsFormRadioYN(_AM_PAGE_CONTENT_MAINDISPLAY, 'content_maindisplay', $content_maindisplay));
        //active
        $this->addElement(new XoopsFormRadioYN(_AM_PAGE_CONTENT_STATUS, 'content_status', $content_status));

        if ( $xoops->user->isAdmin($xoops->module->mid()) ) {
            //author
            if ($obj->isNew()) {
                $author = $xoops->isUser() ? $xoops->user->getVar('uid') : 0;
                $donnee['date_update'] = 0;
            }else{
                $author = $obj->getVar('content_author');
            }
            $this->addElement(new XoopsFormSelectUser(_AM_PAGE_CONTENT_AUTHOR, 'content_author', true, $author, 1, false), true);
            //date
            if (!$obj->isNew()) {
                $select_date = new XoopsFormElementTray(_AM_PAGE_CONTENT_DATEUPDATE);
                $date = new XoopsFormRadio('', 'date_update', 'N');
                $options = array('N' =>_AM_PAGE_CONTENT_DATEUPDATE_NO . ' (' . XoopsLocal::formatTimestamp($obj->getVar('content_create'),'s') . ')', 'Y' => _AM_PAGE_CONTENT_DATEUPDATE_YES);
                $date->addOptionArray($options);
                $select_date->addElement($date);
                $select_date->addElement(new XoopsFormTextDateSelect('', 'content_create', 2, time()));
                $this->addElement($select_date);
            }
            //permissions
            $member_handler = $xoops->getHandler('member');
            $group_list = $member_handler->getGroupList();
            $gperm_handler = $xoops->getHandler('groupperm');
            $full_list = array_keys($group_list);
            if(!$obj->isNew()) {
                $module_id = $xoops->isModule() ? $xoops->module->getVar('mid', 'n') : 1;
                $groups_ids_view = $gperm_handler->getGroupIds('page_view_item', $obj->getVar('content_id'), $module_id);
                $groups_ids_view = array_values($groups_ids_view);
                $groups_can_view_checkbox = new XoopsFormCheckBox(_AM_PAGE_CONTENT_PERMISSION, 'groups_view_item[]', $groups_ids_view);
            } else {
                $groups_can_view_checkbox = new XoopsFormCheckBox(_AM_PAGE_CONTENT_PERMISSION, 'groups_view_item[]', $full_list);
            }
            $groups_can_view_checkbox->addOptionArray($group_list);
            $this->addElement($groups_can_view_checkbox);
        }

        $this->addElement(new XoopsFormHidden( 'op', 'save' ) );
        $this->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit' ) );
    }
}