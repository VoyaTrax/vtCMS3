<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page_related.php 10309 2012-12-02 21:34:50Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class PagePage_relatedForm extends XoopsThemeForm
{
    /**
     * @param PagePage_related|XoopsObject $obj
     */
    public function __construct(PagePage_related &$obj)
    {
        $xoops = Xoops::getInstance();
        // Get handler
        $content_Handler = $xoops->getModuleHandler('page_content');
        $related_Handler = $xoops->getModuleHandler('page_related');

        $title = $obj->isNew() ? sprintf( _AM_PAGE_RELATED_ADD ) : sprintf( _AM_PAGE_RELATED_EDIT );

        parent::__construct($title, 'form', 'related.php', 'post', true);

        $secondary_max = 10;
        $this->addElement(new XoopsFormHidden( 'secondary_max', $secondary_max ) );
        $criteria_related = new CriteriaCompo();
        if (!$obj->isNew()) {
            $criteria_related->add(new Criteria('related_group', $obj->getVar('related_group'), '!='));
            $this->addElement(new XoopsFormHidden('related_id', $obj->getVar('related_id')));
            $related_domenu = $obj->getVar('related_domenu');
            for ($i = 1; $i <= $secondary_max; $i++) {
                $related_secondary[$i] = 0;
            }
            $criteria = new CriteriaCompo();
            $criteria->setSort('related_weight');
            $criteria->setOrder('ASC');
            $criteria->add(new Criteria('related_group', $obj->getVar('related_contentid')));
            $criteria->add(new Criteria('related_name', '/'));
            $related_arr = $related_Handler->getall($criteria);
            $j = 1;
            foreach (array_keys($related_arr) as $i) {
                $related_secondary[$j] = $related_arr[$i]->getVar("related_contentid");
                $j++;
            }
        } else {
            $related_domenu = 1;
            for ($i = 1; $i <= $secondary_max; $i++) {
                $related_secondary[$i] = 0;
            }

        }
        $compare_related_arr = $related_Handler->getall($criteria_related);
        if (count($compare_related_arr) > 0) {
            foreach (array_keys($compare_related_arr) as $i) {
                $compare[$compare_related_arr[$i]->getVar("related_contentid")] = $compare_related_arr[$i]->getVar("related_contentid");
            }
        } else {
            $compare = array();
        }
        //name
        $this->addElement(new XoopsFormText(_AM_PAGE_RELATED_NAME, 'related_name', 4, 255, $obj->getVar('related_name'), ''), true);
        //main
        $criteria = new CriteriaCompo();
        $criteria->setSort('content_title');
        $criteria->setOrder('ASC');
        $content_arr = $content_Handler->getall($criteria);
        $main = new XoopsFormSelect(_AM_PAGE_RELATED_MAIN, 'related_main', $obj->getVar('related_contentid'), 1, false);
        foreach (array_keys($content_arr) as $i) {
            if (!in_array($content_arr[$i]->getVar("content_id") , $compare)) {
                $main->addOption($i, $content_arr[$i]->getVar("content_title"));
            }
        }
        $main->setDescription(_AM_PAGE_RELATED_MAIN_DSC);
        $this->addElement($main,true);
        //secondary
        for ($i = 1; $i <= $secondary_max; $i++) {
            $secondary = new XoopsFormSelect(_AM_PAGE_RELATED_SECONDARY . ' ' . $i, 'related_secondary' . $i, $related_secondary[$i], 1, false);
            $secondary->addOption(0, '&nbsp;');
            foreach (array_keys($content_arr) as $j) {
                if (!in_array($content_arr[$j]->getVar("content_id") , $compare)) {
                    $secondary->addOption($j, $content_arr[$j]->getVar("content_title"));
                }
            }
            $this->addElement($secondary, false);
            unset($secondary);
        }
        //menu
        $menu = new XoopsFormRadioYN(_AM_PAGE_RELATED_MENU, 'related_domenu', $related_domenu);
        $menu->setDescription(_AM_PAGE_RELATED_MENU_DSC);
        $this->addElement($menu, false);
        //navigation
        $navigation = new XoopsFormSelect(_AM_PAGE_RELATED_NAVIGATION, 'related_navigation', $obj->getVar('related_navigation'), 1, false);
        $navigation->addOption(1, _AM_PAGE_RELATED_NAVIGATION_OPTION1);
        $navigation->addOption(2, _AM_PAGE_RELATED_NAVIGATION_OPTION2);
        $navigation->addOption(3, _AM_PAGE_RELATED_NAVIGATION_OPTION3);
        $navigation->addOption(4, _AM_PAGE_RELATED_NAVIGATION_OPTION4);
        $navigation->addOption(5, _AM_PAGE_RELATED_NAVIGATION_OPTION5);
        $navigation->setClass('span3');
        $this->addElement($navigation);

        $this->addElement(new XoopsFormHidden( 'op', 'save' ) );
        $this->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit' ) );
    }
}