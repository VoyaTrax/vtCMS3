<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page_related.php 10328 2012-12-07 00:56:07Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class PagePage_related extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('related_id', XOBJ_DTYPE_INT, null, false, 8);
        $this->initVar('related_name', XOBJ_DTYPE_TXTBOX, null, false );
        $this->initVar('related_group', XOBJ_DTYPE_INT, null, false, 5);
        $this->initVar('related_contentid', XOBJ_DTYPE_INT, null, false, 5);
        $this->initVar('related_domenu', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('related_navigation', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('related_weight',XOBJ_DTYPE_INT, null, false, 5);
        //joint
        $this->initVar('content_title', XOBJ_DTYPE_TXTBOX, null, false );
        $this->initVar('content_status', XOBJ_DTYPE_TXTBOX, null, false );
    }

    public function get_new_id()
    {
        $xoops = Xoops::getInstance();
        $new_id = $xoops->db()->getInsertId();
        return $new_id;
    }
}

class PagePage_relatedHandler extends XoopsPersistableObjectHandler
{
    /**
     * @param null|XoopsDatabase $db
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'page_related', 'pagepage_related', 'related_id', 'related_name');
    }
}