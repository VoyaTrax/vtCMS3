<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page_rating.php 10273 2012-11-25 20:56:32Z mageg $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class PagePage_rating extends XoopsObject
{ 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('rating_id',XOBJ_DTYPE_INT,null,false,10);
        $this->initVar('rating_contentid',XOBJ_DTYPE_INT,null,false,10);
        $this->initVar('rating_userid',XOBJ_DTYPE_INT,null,false,10);
        $this->initVar('rating_rating',XOBJ_DTYPE_OTHER,null,false,3);
        $this->initVar('rating_ip',XOBJ_DTYPE_TXTBOX, null, false);
        $this->initVar('rating_date',XOBJ_DTYPE_INT,null,false,10);
    }
}

class PagePage_ratingHandler extends XoopsPersistableObjectHandler
{
    /**
     * @param null|XoopsDatabase $db
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'page_rating', 'pagepage_rating', 'rating_id', 'rating_contentid');
    }
}