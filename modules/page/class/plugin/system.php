<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/**
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU public license
 * @author          Laurent JEN (aka DuGris)
 * @version         $Id: system.php 10666 2013-01-04 21:48:37Z trabis $
 */

class PageSystemPlugin extends Xoops_Module_Plugin_Abstract implements SystemPluginInterface
{
    /**
     * @param int $uid
     *
     * @return int
     */
    public function userPosts($uid)
    {
        $xoops = Xoops::getInstance();

        $content_Handler = $xoops->getModuleHandler('page_content');

        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('content_status', 0, '!='));
        $criteria->add(new Criteria('content_author', (int)$uid));

        return $content_Handler->getCount($criteria);
    }

    /**
     * @return array
     */
    public function waiting()
    {
        return array();
    }

    /**
     * Used to populate backend
     *
     * @param int $limit : Number of item for backend
     *                   Expects an array containing:
     *                   title   : Title for the backend items
     *                   link    : Link for the backend items
     *                   content : content for the backend items
     *                   date    : Date of the backend items
     *
     * @return array
     */
    public function backend($limit)
    {
        // TODO: Implement backend() method.
    }

    /**
     * Used to populate the User Block
     * Expects an array containing:
     *    name  : Name for the Link
     *    link  : Link relative to module
     *    image : Url of image to display, please use 16px*16px image
     *
     * @return array
     */
    public function userMenus()
    {
        // TODO: Implement userMenus() method.
    }
}