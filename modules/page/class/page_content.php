<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: page_content.php 10328 2012-12-07 00:56:07Z trabis $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class PagePage_content extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('content_id', XOBJ_DTYPE_INT, null, false, 11);
        $this->initVar('content_title', XOBJ_DTYPE_TXTBOX, null, false );
        $this->initVar('content_shorttext', XOBJ_DTYPE_TXTAREA, null, false);
        $this->initVar('content_text', XOBJ_DTYPE_TXTAREA, null, false);
        $this->initVar('content_create', XOBJ_DTYPE_INT, null, false, 10);
        $this->initVar('content_author', XOBJ_DTYPE_INT, null, false, 11);
        $this->initVar('content_status', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_hits', XOBJ_DTYPE_INT, null, false, 10);
        $this->initVar('content_rating',  XOBJ_DTYPE_OTHER, null, false, 10);
        $this->initVar('content_votes', XOBJ_DTYPE_INT, null, false, 11);
        $this->initVar('content_comments', XOBJ_DTYPE_INT, null, false, 11);
        $this->initVar('content_mkeyword', XOBJ_DTYPE_TXTAREA, null, false);
        $this->initVar('content_mdescription', XOBJ_DTYPE_TXTAREA, null, false);
        $this->initVar('content_maindisplay', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_weight',XOBJ_DTYPE_INT, null, false, 5);
        $this->initVar('content_dopdf', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_doprint', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_dosocial', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_doauthor', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_dodate', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_domail', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_dohits', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_dorating', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_docoms', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_doncoms', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_dotitle', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('content_donotifications', XOBJ_DTYPE_INT, null, false, 1);
        $this->initVar('dohtml', XOBJ_DTYPE_INT, 1, false);
    }

    public function get_new_id()
    {
        $xoops = Xoops::getInstance();
        $new_id = $xoops->db()->getInsertId();
        return $new_id;
    }
}

class PagePage_contentHandler extends XoopsPersistableObjectHandler
{
    /**
     * @param null|XoopsDatabase $db
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'page_content', 'pagepage_content', 'content_id', 'content_title');
    }
}