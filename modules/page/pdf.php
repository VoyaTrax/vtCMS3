<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * XXX
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @since           2.6.0
 * @author          Mage Grégory (AKA Mage)
 * @version         $Id: pdf.php 10446 2012-12-18 19:57:26Z mageg $
 */
include_once 'header.php';
$xoops = Xoops::getInstance();
$tpl = new XoopsTpl();

// Get ID
$content_id = $system->cleanVars($_REQUEST, 'id', 0, 'int');
// Get handler
$content_Handler = $xoops->getModuleHandler('page_content');
$related_Handler = $xoops->getModuleHandler('page_related');

$view_content = $content_Handler->get($content_id);

// Test if the page exist
if (count($view_content) == 0 || $view_content->getVar('content_status') == 0){
    $xoops->redirect('index.php', 3, _MD_PAGE_VIEWPAGE_NOPAGE);
    exit();
}

// Permission to view
$perm_view = ($gperm_handler->checkRight('page_view_item', $content_id, $groups, $xoops->module->getVar('mid'), false)) ? true : false ;
if(!$perm_view || $view_content->getVar('content_dopdf') == false) {
    $xoops->redirect("index.php", 2, _NOPERM);
    exit();
}

$tpl->assign('content_id' , $content_id);
$tpl->assign('content_title' , $view_content->getVar('content_title'));
$tpl->assign('content_shorttext' , $view_content->getVar('content_shorttext'));
$tpl->assign('content_text' , $view_content->getVar('content_text'));
$tpl->assign('content_author' , XoopsUser::getUnameFromId($view_content->getVar('content_author')));
$tpl->assign('content_authorid' , $view_content->getVar('content_author'));
$tpl->assign('content_hits' , sprintf(_MD_PAGE_VIEWPAGE_HITS, $view_content->getVar('content_hits')));
$tpl->assign('content_date' , XoopsLocal::formatTimestamp($view_content->getVar('content_create'), $xoops->getModuleConfig('page_dateformat')));
$tpl->assign('content_time' , XoopsLocal::formatTimestamp($view_content->getVar('content_create'), $xoops->getModuleConfig('page_timeformat')));
$tpl->assign('content_comments' , $view_content->getVar('content_comments'));
$tpl->assign('content_rating' , number_format($view_content->getVar('content_rating'), 1));
$tpl->assign('content_votes' , $view_content->getVar('content_votes'));
$tpl->assign('content_doauthor', $view_content->getVar('content_doauthor'));
$tpl->assign('content_dodate', $view_content->getVar('content_dodate'));
$tpl->assign('content_domail', $view_content->getVar('content_domail'));
$tpl->assign('content_dohits', $view_content->getVar('content_dohits'));
$tpl->assign('content_dorating', $view_content->getVar('content_dorating'));
$tpl->assign('content_dotitle', $view_content->getVar('content_dotitle'));
if ($view_content->getVar('content_docoms') == false){
    $ncoms = false;
} elseif ($view_content->getVar('content_doncoms') == false){
    $ncoms = false;
} else {
    $ncoms = true;
}
$tpl->assign('content_doncoms', $ncoms);
if ($view_content->getVar('content_dotitle') == false && $view_content->getVar('content_dorating') == false) {
    $header = false;
} else {
    $header = true;
}
$tpl->assign('content_doheader', $header);
if ($view_content->getVar('content_doauthor') == false && $view_content->getVar('content_dodate') == false && $view_content->getVar('content_dohits') == false && $ncoms == false) {
    $footer = false;
} else {
    $footer = true;
}
$tpl->assign('content_dofooter', $footer);

$page = new Page();

$related = $page->menu_related($content_id);

if ($related['domenu'] ) {
    $tpl->assign('summary', $related['summary']);
}

$content = $tpl->fetch('module:page|page_pdf.html');

$pdf = new Pdf();
$pdf->writeHtml($content, false);
$pdf->Output('example.pdf');