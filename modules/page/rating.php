<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: rating.php 10574 2012-12-27 20:46:10Z mageg $
 */

include_once 'header.php';
// Call header
$xoops = Xoops::getInstance();
$xoops->header();

// Get handler
$content_Handler = $xoops->getModuleHandler('page_content');
$rating_Handler = $xoops->getModuleHandler('page_rating');

// Get content_id
$content_id = $system->cleanVars($_REQUEST, 'content_id', 0, 'int');
// Get rating
$rating = $system->cleanVars($_REQUEST, 'rating', 0, 'string');
// Get extra url
$extra_url = $system->cleanVars($_REQUEST, 'extra_url', '', 'string');

$view_content = $content_Handler->get($content_id);
// Test if the page exist
if (count($view_content) == 0 || $view_content->getVar('content_status') == 0){
    $xoops->redirect('index.php', 3, _MD_PAGE_VIEWPAGE_NOPAGE);
    exit();
}
//pemissions
$perm_rate = ($gperm_handler->checkRight('page_global', 4, $groups, $xoops->module->getVar('mid'), false)) ? true : false ;
if ($perm_rate == false || $view_content->getVar('content_dorating') == false){
    if ($extra_url != '') {
        $xoops->redirect($extra_url, 1, _NOPERM);
    } else {
        $xoops->redirect('viewpage.php?id=' . $content_id, 1, _NOPERM);
    }
}

if (is_object($xoops->user)) {
    $uid = $xoops->user->getVar('uid');
} else {
    $uid = 0;
}

if ($uid != 0) {
    if ($uid == $view_content->getVar('content_author')){
        if ($extra_url != '') {
            $xoops->redirect($extra_url, 2, _MD_PAGE_RATING_DONOTVOTE);
        } else {
            $xoops->redirect('viewpage.php?id=' . $content_id, 2, _MD_PAGE_RATING_DONOTVOTE);
        }
        exit();
    }
    // Vote for the same resource more than once
    $criteria = new CriteriaCompo();
    $criteria->add(new Criteria('rating_contentid', $content_id));
    $criteria->add(new Criteria('rating_userid', $uid));
    $rating_count = $rating_Handler->getCount($criteria);
    if ($rating_count >= 1) {
        $xoops->redirect('viewpage.php?id=' . $content_id, 2, _MD_PAGE_RATING_VOTEONCE);
        if ($extra_url != '') {
            $xoops->redirect($extra_url, 2, _MD_PAGE_RATING_VOTEONCE);
        } else {
            $xoops->redirect('viewpage.php?id=' . $content_id, 2, _MD_PAGE_RATING_VOTEONCE);
        }
        exit();
    }
} else {
    // Vote for the same resource more than once per day
    $yesterday = (time()-86400);
    $criteria = new CriteriaCompo();
    $criteria->add(new Criteria('rating_contentid', $content_id));
    $criteria->add(new Criteria('rating_userid', $uid));
    $criteria->add(new Criteria('rating_ip', getenv("REMOTE_ADDR")));
    $criteria->add(new Criteria('rating_date', $yesterday, '>'));
    $rating_count = $rating_Handler->getCount($criteria);
    if ($rating_count >= 1) {
        if ($extra_url != '') {
            $xoops->redirect($extra_url, 2, _MD_PAGE_RATING_VOTEONCE);
        } else {
            $xoops->redirect('viewpage.php?id=' . $content_id, 2, _MD_PAGE_RATING_VOTEONCE);
        }
        exit();
    }
}

if (isset($_REQUEST['ok']) && $_REQUEST['ok'] == 1) {
    if (!$xoops->security()->check()) {
        $xoops->redirect('index.php', 3, implode(',', $xoops->security()->getErrors()));
    }
    $obj = $rating_Handler->create();
    $obj->setVar('rating_contentid', $content_id);
    $obj->setVar('rating_userid', $uid);
    $obj->setVar('rating_rating', $rating);
    $obj->setVar('rating_ip', getenv("REMOTE_ADDR"));
    $obj->setVar('rating_date', time());
    if ($rating_Handler->insert($obj)) {
        $criteria = new CriteriaCompo();
        $criteria->add(new Criteria('rating_contentid', $content_id));
        $rating_arr = $rating_Handler->getall($criteria);
        $total_vote = $rating_Handler->getCount($criteria);
        $total_rating = 0;
        foreach (array_keys($rating_arr) as $i) {
            $total_rating += $rating_arr[$i]->getVar('rating_rating');
        }
        $rating = $total_rating / $total_vote;
        $objcontent = $content_Handler->get($content_id);
        $objcontent->setVar('content_rating', number_format($rating, 1));
        $objcontent->setVar('content_votes', $total_vote);
        if ($content_Handler->insert($objcontent)) {
            if ($extra_url != '') {
                $xoops->redirect($extra_url, 2, _MD_PAGE_RATING_THANKS);
            } else {
                $xoops->redirect('viewpage.php?id=' . $content_id, 2, _MD_PAGE_RATING_THANKS);
            }
        }
        echo $objdownloads->getHtmlErrors();
    } else {
        echo $obj->getHtmlErrors();
    }
} else {
    // Define Stylesheet
    $xoops->theme()->addStylesheet('modules/system/css/admin.css');
    $xoops->confirm(array('ok' => 1, 'content_id' => $content_id, 'rating' => $rating), $_SERVER['REQUEST_URI'], sprintf(_MD_PAGE_RATING_CONFIRM, $rating, $view_content->getVar('content_title')));
}
$xoops->footer();