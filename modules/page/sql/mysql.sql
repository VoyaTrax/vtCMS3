#
# Table structure for table `page_content`
#

CREATE TABLE page_content (
  content_id int(11) unsigned NOT NULL auto_increment,
  content_title varchar(255) NOT NULL default '',
  content_shorttext text NOT NULL,
  content_text text NOT NULL,
  content_create int(10) NOT NULL default '0',
  content_author int(11) NOT NULL default '0',
  content_status tinyint(1) NOT NULL default '0',
  content_hits int(11) unsigned NOT NULL default '0',
  content_rating double(6,4) NOT NULL default '0.0000',
  content_votes int(11) unsigned NOT NULL default '0',
  content_comments int(11) unsigned NOT NULL default '0',
  content_mkeyword text NOT NULL,
  content_mdescription text NOT NULL,
  content_maindisplay tinyint(1) NOT NULL default '0',
  content_weight int(5) NOT NULL default '0',
  content_dopdf tinyint(1) NOT NULL default '0',
  content_doprint tinyint(1) NOT NULL default '0',
  content_dosocial tinyint(1) NOT NULL default '0',
  content_doinfo tinyint(1) NOT NULL default '0',
  content_doauthor tinyint(1) NOT NULL default '0',
  content_dodate tinyint(1) NOT NULL default '0',
  content_domail tinyint(1) NOT NULL default '0',
  content_dohits tinyint(1) NOT NULL default '0',
  content_dorating tinyint(1) NOT NULL default '0',
  content_docoms tinyint(1) NOT NULL default '0',
  content_doncoms tinyint(1) NOT NULL default '0',
  content_dotitle tinyint(1) NOT NULL default '0',
  content_donotifications tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (content_id),
  KEY content_status (content_status),
  KEY content_title (content_title(40))
) ENGINE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `page_related`
#

CREATE TABLE page_related (
  related_id int(8) unsigned NOT NULL auto_increment,  
  related_name varchar(255) NOT NULL default '',
  related_group int(5) unsigned NOT NULL default '0',
  related_contentid int(5) unsigned NOT NULL default '0',
  related_domenu tinyint(1) NOT NULL default '0',
  related_navigation tinyint(1) NOT NULL default '0',
  related_weight int(5) NOT NULL default '0',
  PRIMARY KEY  (related_id),
  KEY related_name (related_name(40)),
  KEY related_group (related_group)
) ENGINE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `page_rating`
#

CREATE TABLE page_rating (
  rating_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  rating_contentid INTEGER UNSIGNED NULL,
  rating_userid INTEGER UNSIGNED NULL,
  rating_rating DECIMAL(2,1) NULL,
  rating_ip varchar(60) NOT NULL default '',
  rating_date INTEGER UNSIGNED NULL,
  PRIMARY KEY(rating_id)
)ENGINE=MyISAM;
# --------------------------------------------------------