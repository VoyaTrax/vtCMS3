<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * page module
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         page
 * @since           2.6.0
 * @author          Mage Gr�gory (AKA Mage)
 * @version         $Id: viewpage.php 10321 2012-12-05 19:25:07Z trabis $
 */

include_once 'header.php';
$xoops = Xoops::getInstance();
// Call header
$xoops->header('page_viewpage.html');
// Parameters
$interval = 3600; //1 hour

// Get ID
$content_id = $system->cleanVars($_REQUEST, 'id', 0, 'int');
// Get handler
$content_Handler = $xoops->getModuleHandler('page_content');
$related_Handler = $xoops->getModuleHandler('page_related');

$view_content = $content_Handler->get($content_id);

// Test if the page exist
if (count($view_content) == 0 || $view_content->getVar('content_status') == 0){
    $xoops->redirect('index.php', 3, _MD_PAGE_VIEWPAGE_NOPAGE);
    exit();
}

// Permission to view
$perm_view = ($gperm_handler->checkRight('page_view_item', $content_id, $groups, $xoops->module->getVar('mid'), false)) ? true : false ;
if(!$perm_view) {
    $xoops->redirect("javascript:history.go(-1)", 2, _NOPERM);
    exit();
}

// add module css
$xoTheme->addStylesheet( $xoops->url('/modules/' . $xoops->module->getVar('dirname', 'n') . '/css/styles.css'), null );
$xoTheme->addStylesheet( $xoops->url('/modules/' . $xoops->module->getVar('dirname', 'n') . '/css/rating.css'), null );
// Javascript
$xoTheme->addScript( XOOPS_URL . '/modules/page/js/rating.js' );
$xoTheme->addScript( XOOPS_URL . '/modules/page/js/MetaData.js' );
$xoTheme->addScript( XOOPS_URL . '/modules/page/js/rating.pack.js' );

if (is_object($xoops->user)) {
    $uid = $xoops->user->getVar('uid');
} else {
    $uid = 0;
}
// hits
if ($view_content->getVar('content_author') != $uid && $view_content->getVar('content_dohits') != false) {
    if ( !isset( $_SESSION['page_hits' . $content_id] ) || isset( $_SESSION['page_hits' . $content_id] ) && ($_SESSION['page_hits' . $content_id]['content_time'] + $interval) <  time()) {
        $hits = $view_content->getVar('content_hits') + 1;
        $view_content->setVar('content_hits', $hits);
        $content_Handler->insert($view_content);
        $_SESSION['page_hits' . $content_id]['content_time'] = time();
    }
}


//Rating form
$content_rating = $view_content->getVar('content_rating');
$rating_form = '<form id="rating" action="rating.php">';
for($i=0.5; $i <= 6;){
    if ($content_rating >= ($i - 0.25) && $content_rating < ($i + 0.25)){
        $rating_form .= '<input name="rating" type="radio" class="star_rating {split:2}" value="' . $i . '" checked="checked"/>';
    }else{
        $rating_form .= '<input name="rating" type="radio" class="star_rating {split:2}" value="' . $i . '"/>';
    }
    $i += 0.5;
}
$rating_form .= '<input name="content_id" type="hidden" id="content_id" value="' . $content_id .'" />';
$rating_form .= '</form>';
$xoopsTpl->assign('rating_form' , $rating_form);

// content
$xoopsTpl->assign('content_id' , $content_id);
$xoopsTpl->assign('content_title' , $view_content->getVar('content_title'));
$xoopsTpl->assign('content_shorttext' , $view_content->getVar('content_shorttext'));
$xoopsTpl->assign('content_text' , $view_content->getVar('content_text'));
$xoopsTpl->assign('content_author' , XoopsUser::getUnameFromId($view_content->getVar('content_author')));
$xoopsTpl->assign('content_authorid' , $view_content->getVar('content_author'));
$xoopsTpl->assign('content_hits' , sprintf(_MD_PAGE_VIEWPAGE_HITS, $view_content->getVar('content_hits')));
$xoopsTpl->assign('content_date' , XoopsLocal::formatTimestamp($view_content->getVar('content_create'), $xoops->getModuleConfig('page_dateformat')));
$xoopsTpl->assign('content_time' , XoopsLocal::formatTimestamp($view_content->getVar('content_create'), $xoops->getModuleConfig('page_timeformat')));
$xoopsTpl->assign('content_comments' , $view_content->getVar('content_comments'));
$xoopsTpl->assign('content_rating' , number_format($view_content->getVar('content_rating'), 1));
$xoopsTpl->assign('content_votes' , $view_content->getVar('content_votes'));
$xoopsTpl->assign('content_dopdf', $view_content->getVar('content_dopdf'));
$xoopsTpl->assign('content_doprint', $view_content->getVar('content_doprint'));
$xoopsTpl->assign('content_dosocial', $view_content->getVar('content_dosocial'));
$xoopsTpl->assign('content_doauthor', $view_content->getVar('content_doauthor'));
$xoopsTpl->assign('content_dodate', $view_content->getVar('content_dodate'));
$xoopsTpl->assign('content_domail', $view_content->getVar('content_domail'));
$xoopsTpl->assign('content_dohits', $view_content->getVar('content_dohits'));
$xoopsTpl->assign('content_dorating', $view_content->getVar('content_dorating'));
$xoopsTpl->assign('content_dotitle', $view_content->getVar('content_dotitle'));
$xoopsTpl->assign('content_dosocial', $view_content->getVar('content_dosocial'));
$xoopsTpl->assign('content_donotifications', $view_content->getVar('content_donotifications'));
$xoopsTpl->assign('content_docoms', $view_content->getVar('content_docoms'));
if ($view_content->getVar('content_docoms') == false){
    $ncoms = false;
} elseif ($view_content->getVar('content_doncoms') == false){
    $ncoms = false;
} else {
    $ncoms = true;
}
$xoopsTpl->assign('content_doncoms', $ncoms);
if ($view_content->getVar('content_dotitle') == false && $view_content->getVar('content_dorating') == false) {
    $header = false;
} else {
    $header = true;
}
$xoopsTpl->assign('content_doheader', $header);
if ($view_content->getVar('content_doauthor') == false && $view_content->getVar('content_dodate') == false && $view_content->getVar('content_dohits') == false && $ncoms == false) {
    $footer = false;
} else {
    $footer = true;
}
$xoopsTpl->assign('content_dofooter', $footer);

$page = new Page();

$related = $page->menu_related($content_id);

if ($related['domenu'] ) {
    $xoopsTpl->assign('summary', $related['summary']);
}
if ($related['navigation'] != '' ) {
    $xoopsTpl->assign('navigation', $related['navigation']);
    $xoopsTpl->assign('related', $related);
}

// Meta
$xoopsTpl->assign('xoops_pagetitle', strip_tags($view_content->getVar('content_title')  . ' - ' . $xoops->module->name()));
$xoTheme->addMeta( 'meta', 'description', strip_tags($view_content->getVar('content_mdescription')));
$xoTheme->addMeta('meta', 'keywords', strip_tags($view_content->getVar('content_mkeyword')));
$xoops->footer();