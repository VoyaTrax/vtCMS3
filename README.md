##vtCMS version 3

Planned is to take the XOOPS 2.6.0-alpha2 and continue in an effort to keep everything simular to the 2.5.x series. What I'm thinking of removing _*EVERYTHING*_ that cause bloat in such a manner that it's useless. Such as removing unneded or old code, intead of commenting out.

There are things that I fell should be embedded into the core system, examples of that are:

  * An _About Us_ page. Configurable from the backend.
  * An _Imprint_ page. Configurable from the backend.
  * A _Terms_ page. Configurable from the backend.
  * A _Disclaimer_ page. Configurable from the backend.
  * A _Privacy Policy_ page. Configurable from the backend.
  * Captcha Configurablity from the backend.
  * European Cookie Law Compliance
  
>...there is probaly a lot more that just don't come to mind just now.
