<?php
/**
 * XOOPS comment edit
 *
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         include
 * @since           2.0.0
 * @author          Kazumi Ono (AKA onokazu) http://www.myweb.ne.jp/, http://jp.xoops.org/
 * @version         $Id: comment_edit.php 8064 2011-11-06 01:17:21Z beckmi $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

$xoops = Xoops::getInstance();

$xoops->isModule() or die('Restricted access');

include_once $xoops->path('include/comment_constants.php');

if (('system' != $xoops->module->getVar('dirname') && XOOPS_COMMENT_APPROVENONE == $xoops->moduleConfig['com_rule']) || (!$xoops->isUser() && !$xoops->moduleConfig['com_anonpost']) || !$xoops->isModule()) {
    $xoops->redirect(XOOPS_URL . '/user.php', 1, _NOPERM);
}

$xoops->loadLanguage('comment');

$com_id = isset($_GET['com_id']) ? intval($_GET['com_id']) : 0;
$com_mode = isset($_GET['com_mode']) ? htmlspecialchars(trim($_GET['com_mode']), ENT_QUOTES) : '';

if ($com_mode == '') {
    if ($xoops->isUser()) {
        $com_mode = $xoops->user->getVar('umode');
    } else {
        $com_mode = $xoops->getConfig('com_mode');
    }
}

if (!isset($_GET['com_order'])) {
    if ($xoops->isUser()) {
        $com_order = $xoops->user->getVar('uorder');
    } else {
        $com_order = $xoops->getConfig('com_order');
    }
} else {
    $com_order = intval($_GET['com_order']);
}

$comment_handler = $xoops->getHandlerComment();
$comment = $comment_handler->get($com_id);
$dohtml = $comment->getVar('dohtml');
$dosmiley = $comment->getVar('dosmiley');
$dobr = $comment->getVar('dobr');
$doxcode = $comment->getVar('doxcode');
$com_icon = $comment->getVar('com_icon');
$com_itemid = $comment->getVar('com_itemid');
$com_title = $comment->getVar('com_title', 'e');
$com_text = $comment->getVar('com_text', 'e');
$com_pid = $comment->getVar('com_pid');
$com_status = $comment->getVar('com_status');
$com_rootid = $comment->getVar('com_rootid');

if ($xoops->module->getVar('dirname') != 'system') {
    $xoops->header();
    include $xoops->path('include/comment_form.php');
    $xoops->footer();
} else {
    xoops_cp_header();
    include $xoops->path('include/comment_form.php');
    xoops_cp_footer();
}

?>