<?php
/**
 * XOOPS Version Definition
 *
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         kernel
 * @version         $Id: version.php 10599 2012-12-29 02:36:38Z beckmi $
 */
defined('XOOPS_ROOT_PATH') or die('Restricted access');

/**
 *  Define XOOPS version
 */
define('XOOPS_VERSION', 'XOOPS 2.6.0-Alpha 2');

?>