<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * XOOPS message detail
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         core
 * @since           2.0.0
 * @version         $Id: viewpmsg.php 10550 2012-12-25 19:37:34Z mageg $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mainfile.php';

$xoops = Xoops::getInstance();
$xoops->preload()->triggerEvent('core.viewpmsg.start');

$xoops->loadLanguage('pmsg');

if (!$xoops->isUser()) {
    $errormessage = _PM_SORRY . "<br />" . _PM_PLZREG . "";
    $xoops->redirect("user.php", 2, $errormessage);
} else {
    $pm_handler = $xoops->getHandlerPrivmessage();
    if (isset($_POST['delete_messages']) && isset($_POST['msg_id'])) {
        if (!$xoops->security()->check()) {
            echo implode('<br />', $xoops->security()->getErrors());
            exit();
        } else {
            if (empty($_REQUEST['ok'])) {
                $xoops->header('system_viewpmsg.html');
                // Define Stylesheet
                $xoops->theme()->addStylesheet('modules/system/css/admin.css');
                $xoops->confirm(array(
                        'ok' => 1, 'delete_messages' => 1, 'msg_id' => serialize(array_map("intval", $_POST['msg_id']))
                    ), $_SERVER['REQUEST_URI'], _PM_SURE_TO_DELETE);
                $xoops->footer();
            }
        }
        $_POST['msg_id'] = unserialize($_REQUEST['msg_id']);
        $size = count($_POST['msg_id']);
        $msg = $_POST['msg_id'];
        for ($i = 0; $i < $size; $i++) {
            $pm = $pm_handler->get(intval($msg[$i]));
            if ($pm->getVar('to_userid') == $xoops->user->getVar('uid')) {
                $pm_handler->delete($pm);
            }
            unset($pm);
        }
        $xoops->redirect("viewpmsg.php", 1, _PM_DELETED);
    }
    $xoops->header('system_viewpmsg.html');
    $criteria = new Criteria('to_userid', $xoops->user->getVar('uid'));
    $criteria->setOrder('DESC');
    $pm_arr = $pm_handler->getObjects($criteria);
    $total_messages = count($pm_arr);
    $xoops->tpl()->assign('display', true);
    $xoops->tpl()->assign('anonymous', $xoops->getConfig('anonymous'));
    $xoops->tpl()->assign('uid', $xoops->user->getVar("uid"));
    $xoops->tpl()->assign('total_messages', $total_messages);
    foreach (array_keys($pm_arr) as $i) {
        $messages['msg_id'] = $pm_arr[$i]->getVar("msg_id");
        $messages['read_msg'] = $pm_arr[$i]->getVar("read_msg");
        $messages['msg_image'] = $pm_arr[$i]->getVar("msg_image");
        $messages['posteruid'] = $pm_arr[$i]->getVar('from_userid');
        $messages['postername'] = XoopsUser::getUnameFromId($pm_arr[$i]->getVar('from_userid'));
        $messages['subject'] = $pm_arr[$i]->getVar("subject");
        $messages['msg_time'] = XoopsLocal::formatTimestamp($pm_arr[$i]->getVar('msg_time'));
        $xoops->tpl()->append('messages', $messages);
    }
    $xoops->tpl()->assign('token', $xoops->security()->getTokenHTML());
    $xoops->footer();
}