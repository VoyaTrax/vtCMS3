<?php
/**
 * XOOPS Kernel Class
 *
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         kernel
 * @since           2.0.0
 * @author          Kazumi Ono (AKA onokazu) http://www.myweb.ne.jp/, http://jp.xoops.org/
 * @version         $Id: comment.php 8064 2011-11-06 01:17:21Z beckmi $
 */
defined('XOOPS_ROOT_PATH') or die('Restricted access');

/**
 * A Comment
 *
 * @package     kernel
 *
 * @author        Kazumi Ono    <onokazu@xoops.org>
 * @copyright    copyright (c) 2000-2003 XOOPS.org
 */
class XoopsComment extends XoopsObject
{

    /**
     * Constructor
     **/
    public function __construct()
    {
        $this->initVar('com_id', XOBJ_DTYPE_INT, null, false);
        $this->initVar('com_pid', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_modid', XOBJ_DTYPE_INT, null, false);
        $this->initVar('com_icon', XOBJ_DTYPE_OTHER, null, false);
        $this->initVar('com_title', XOBJ_DTYPE_TXTBOX, null, true, 255, true);
        $this->initVar('com_text', XOBJ_DTYPE_TXTAREA, null, true, null, true);
        $this->initVar('com_created', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_modified', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_uid', XOBJ_DTYPE_INT, 0, true);
        $this->initVar('com_ip', XOBJ_DTYPE_OTHER, null, false);
        $this->initVar('com_sig', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_itemid', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_rootid', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_status', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('com_exparams', XOBJ_DTYPE_OTHER, null, false, 255);
        $this->initVar('dohtml', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('dosmiley', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('doxcode', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('doimage', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('dobr', XOBJ_DTYPE_INT, 0, false);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function id($format = 'n')
    {
        return $this->getVar('com_id', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_id($format = '')
    {
        return $this->getVar('com_id', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_pid($format = '')
    {
        return $this->getVar('com_pid', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_modid($format = '')
    {
        return $this->getVar('com_modid', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_icon($format = '')
    {
        return $this->getVar('com_icon', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_title($format = '')
    {
        return $this->getVar('com_title', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_text($format = '')
    {
        return $this->getVar('com_text', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_created($format = '')
    {
        return $this->getVar('com_created', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_modified($format = '')
    {
        return $this->getVar('com_modified', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_uid($format = '')
    {
        return $this->getVar('com_uid', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_ip($format = '')
    {
        return $this->getVar('com_ip', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_sig($format = '')
    {
        return $this->getVar('com_sig', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_itemid($format = '')
    {
        return $this->getVar('com_itemid', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_rootid($format = '')
    {
        return $this->getVar('com_rootid', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_status($format = '')
    {
        return $this->getVar('com_status', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function com_exparams($format = '')
    {
        return $this->getVar('com_exparams', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function dohtml($format = '')
    {
        return $this->getVar('dohtml', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function dosmiley($format = '')
    {
        return $this->getVar('dosmiley', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function doxcode($format = '')
    {
        return $this->getVar('doxcode', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function doimage($format = '')
    {
        return $this->getVar('doimage', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function dobr($format = '')
    {
        return $this->getVar('dobr', $format);
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return ($this->getVar('com_id') == $this->getVar('com_rootid'));
    }
}

/**
 * XOOPS comment handler class.
 *
 * This class is responsible for providing data access mechanisms to the data source
 * of XOOPS comment class objects.
 *
 *
 * @package     kernel
 * @subpackage  comment
 *
 * @author        Kazumi Ono    <onokazu@xoops.org>
 * @copyright    copyright (c) 2000-2003 XOOPS.org
 */
class XoopsCommentHandler extends XoopsPersistableObjectHandler
{
    /**
     * Constructor
     *
     * @param XoopsDatabase|null $db {@link XoopsDatabase}
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'xoopscomments', 'XoopsComment', 'com_id', 'com_title');
    }

    /**
     * Retrieves comments for an item
     *
     * @param   int     $module_id  Module ID
     * @param   int     $item_id    Item ID
     * @param   string  $order      Sort order
     * @param   int     $status     Status of the comment
     * @param   int     $limit      Max num of comments to retrieve
     * @param   int     $start      Start offset
     *
     * @return  array   Array of {@link XoopsComment} objects
     **/
    public function getByItemId($module_id, $item_id, $order = null, $status = null, $limit = null, $start = 0)
    {
        $criteria = new CriteriaCompo(new Criteria('com_modid', intval($module_id)));
        $criteria->add(new Criteria('com_itemid', intval($item_id)));
        if (isset($status)) {
            $criteria->add(new Criteria('com_status', intval($status)));
        }
        if (isset($order)) {
            $criteria->setOrder($order);
        }
        if (isset($limit)) {
            $criteria->setLimit($limit);
            $criteria->setStart($start);
        }
        return $this->getObjects($criteria);
    }

    /**
     * Gets total number of comments for an item
     *
     * @param   int     $module_id  Module ID
     * @param   int     $item_id    Item ID
     * @param   int     $status     Status of the comment
     *
     * @return  array   Array of {@link XoopsComment} objects
     **/
    public function getCountByItemId($module_id, $item_id, $status = null)
    {
        $criteria = new CriteriaCompo(new Criteria('com_modid', intval($module_id)));
        $criteria->add(new Criteria('com_itemid', intval($item_id)));
        if (isset($status)) {
            $criteria->add(new Criteria('com_status', intval($status)));
        }
        return $this->getCount($criteria);
    }

    /**
     * @param int $module_id
     * @param int|null $item_id
     * @return int
     */
    public function getCountByModuleId($module_id, $item_id = null)
    {
        $criteria = new CriteriaCompo(new Criteria('com_modid', intval($module_id)));
        if (isset($item_id)) {
            $criteria->add(new Criteria('com_itemid', intval($item_id)));
        }
        return $this->getCount($criteria);
    }

    /**
     * Get the top {@link XoopsComment}s
     *
     * @param   int     $module_id
     * @param   int     $item_id
     * @param   string  $order
     * @param   int     $status
     *
     * @return  array   Array of {@link XoopsComment} objects
     **/
    public function getTopComments($module_id, $item_id, $order, $status = null)
    {
        $criteria = new CriteriaCompo(new Criteria('com_modid', intval($module_id)));
        $criteria->add(new Criteria('com_itemid', intval($item_id)));
        $criteria->add(new Criteria('com_pid', 0));
        if (isset($status)) {
            $criteria->add(new Criteria('com_status', intval($status)));
        }
        $criteria->setOrder($order);
        return $this->getObjects($criteria);
    }

    /**
     * Retrieve a whole thread
     *
     * @param   int     $comment_rootid
     * @param   int     $comment_id
     * @param   int     $status
     *
     * @return  array   Array of {@link XoopsComment} objects
     **/
    public function getThread($comment_rootid, $comment_id, $status = null)
    {
        $criteria = new CriteriaCompo(new Criteria('com_rootid', intval($comment_rootid)));
        $criteria->add(new Criteria('com_id', intval($comment_id), '>='));
        if (isset($status)) {
            $criteria->add(new Criteria('com_status', intval($status)));
        }
        return $this->getObjects($criteria);
    }

    /**
     * Update
     *
     * @param   XoopsComment|XoopsObject  &$comment       {@link XoopsComment} object
     * @param   string  $field_name     Name of the field
     * @param   mixed   $field_value    Value to write
     *
     * @return  bool
     **/
    public function updateByField(XoopsObject &$comment, $field_name, $field_value)
    {
        $comment->unsetNew();
        $comment->setVar($field_name, $field_value);
        return $this->insert($comment);
    }

    /**
     * Delete all comments for one whole module
     *
     * @param   int $module_id  ID of the module
     * @return  bool
     **/
    public function deleteByModule($module_id)
    {
        return $this->deleteAll(new Criteria('com_modid', intval($module_id)));
    }

    /**
     * @param int $module_id
     * @param int $item_id
     * @return bool
     */
    function deleteByItemId($module_id, $item_id)
    {
        $module_id = intval($module_id);
        $item_id = intval($item_id);
        if ($module_id > 0 && $item_id > 0) {
            $comments = $this->getByItemId($module_id, $item_id);
            if (is_array($comments)) {
                $count = count($comments);
                $deleted_num = array();
                for ($i = 0; $i < $count; $i++) {
                    if (false != $this->delete($comments[$i])) {
                        // store poster ID and deleted post number into array for later use
                        $poster_id = $comments[$i]->getVar('com_uid');
                        if ($poster_id != 0) {
                            $deleted_num[$poster_id] = !isset($deleted_num[$poster_id]) ? 1
                                : ($deleted_num[$poster_id] + 1);
                        }
                    }
                }

                $member_handler = Xoops::getInstance()->getHandlerMember();
                foreach ($deleted_num as $user_id => $post_num) {
                    // update user posts
                    /* @var $member_handler XoopsMemberHandler */
                    $com_poster = $member_handler->getUser($user_id);
                    if (is_object($com_poster)) {
                        $member_handler->updateUserByField($com_poster, 'posts', $com_poster->getVar('posts') - $post_num);
                    }
                }
                return true;
            }
        }
        return false;
    }

}