<?php
/**
 * XOOPS Kernel Class
 *
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         kernel
 * @since           2.0.0
 * @author          Kazumi Ono (AKA onokazu) http://www.myweb.ne.jp/, http://jp.xoops.org/
 * @version         $Id: imagesetimg.php 8064 2011-11-06 01:17:21Z beckmi $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

/**
 * XOOPS Image Sets Image
 *
 * @package     kernel
 * @author      Kazumi Ono  <onokazu@xoops.org>
 * @copyright   (c) 2000-2003 The Xoops Project - www.xoops.org
 */
class XoopsImagesetimg extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('imgsetimg_id', XOBJ_DTYPE_INT, null, false);
        $this->initVar('imgsetimg_file', XOBJ_DTYPE_OTHER, null, false);
        $this->initVar('imgsetimg_body', XOBJ_DTYPE_SOURCE, null, false);
        $this->initVar('imgsetimg_imgset', XOBJ_DTYPE_INT, null, false);
    }

    /**
     * @param string $format
     * @return string
     */
    public function id($format = 'n')
    {
        return $this->getVar('imgsetimg_id', $format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function imgsetimg_id($format = '')
    {
        return $this->getVar('imgsetimg_id', $format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function imgsetimg_file($format = '')
    {
        return $this->getVar('imgsetimg_file', $format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function imgsetimg_body($format = '')
    {
        return $this->getVar('imgsetimg_body', $format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function imgsetimg_imgset($format = '')
    {
        return $this->getVar('imgsetimg_imgset', $format);
    }

}


/**
 * XOOPS imageset image handler class.
 * This class is responsible for providing data access mechanisms to the data source
 * of XOOPS imageset image class objects.
 *
 *
 * @author  Kazumi Ono <onokazu@xoops.org>
 */

class XoopsImagesetimgHandler extends XoopsPersistableObjectHandler
{
    /**
     * Constructor
     *
     * @param XoopsDatabase|null $db {@link XoopsDatabase}
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'imgsetimg', 'XoopsImagesetimg', 'imgsetimg_id', 'imgsetimg_file');
    }

    /**
     * Load {@link XoopsImageSetImg}s from the database
     *
     * @param   CriteriaElement|null  $criteria   {@link CriteriaElement}
     * @param   bool $id_as_key  Use the ID as key into the array
     * @return  array   Array of {@link XoopsImageSetImg} objects
     **/
    public function getObjects(CriteriaElement $criteria = null, $id_as_key = false)
    {
        $ret = array();
        $limit = $start = 0;
        $sql = 'SELECT DISTINCT i.* FROM ' . $this->db->prefix('imgsetimg') . ' i LEFT JOIN ' . $this->db->prefix('imgset_tplset_link') . ' l ON l.imgset_id=i.imgsetimg_imgset LEFT JOIN ' . $this->db->prefix('imgset') . ' s ON s.imgset_id=l.imgset_id';
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' ' . $criteria->renderWhere();
            $sql .= ' ORDER BY imgsetimg_id ' . $criteria->getOrder();
            $limit = $criteria->getLimit();
            $start = $criteria->getStart();
        }
        $result = $this->db->query($sql, $limit, $start);
        if (!$result) {
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $imgsetimg = new XoopsImagesetimg();
            $imgsetimg->assignVars($myrow);
            if (!$id_as_key) {
                $ret[] = $imgsetimg;
            } else {
                $ret[$myrow['imgsetimg_id']] = $imgsetimg;
            }
            unset($imgsetimg);
        }
        return $ret;
    }

    /**
     * Count some imagessetsimg
     *
     * @param   CriteriaElement|null  $criteria   {@link CriteriaElement}
     * @return  int
     **/
    public function getCount(CriteriaElement $criteria = null)
    {
        $sql = 'SELECT COUNT(i.imgsetimg_id) FROM ' . $this->db->prefix('imgsetimg') . ' i LEFT JOIN ' . $this->db->prefix('imgset_tplset_link') . ' l ON l.imgset_id=i.imgsetimg_imgset';
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' ' . $criteria->renderWhere() . ' GROUP BY i.imgsetimg_id';
        }
        if (!$result = $this->db->query($sql)) {
            return 0;
        }
        list ($count) = $this->db->fetchRow($result);
        return $count;
    }

    /**
     * @param int $imgset_id
     * @param bool $id_as_key = false documentation
     * @return array d
     * @author Kazumi Ono <onokazu@xoops.org>
     **/
    function getByImageset($imgset_id, $id_as_key = false)
    {
        return $this->getObjects(new Criteria('imgsetimg_imgset', intval($imgset_id)), $id_as_key);
    }

    /**
     * @param string $filename
     * @param int $imgset_id
     * @return bool
     * @author Kazumi Ono <onokazu@xoops.org>
     **/
    function imageExists($filename, $imgset_id)
    {
        $criteria = new CriteriaCompo(new Criteria('imgsetimg_file', $filename));
        $criteria->add(new Criteria('imgsetimg_imgset', intval($imgset_id)));
        if ($this->getCount($criteria) > 0) {
            return true;
        }
        return false;
    }
}

?>