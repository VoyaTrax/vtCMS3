<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * Xoops Kernel class
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         kernel
 * @since           2.0.0
 * @author          Kazumi Ono (AKA onokazu) http://www.myweb.ne.jp/, http://jp.xoops.org/
 * @version         $Id: avatar.php 8064 2011-11-06 01:17:21Z beckmi $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

/**
 * A Avatar
 *
 * @author Kazumi Ono <onokazu@xoops.org>
 * @copyright copyright (c) 2000 XOOPS.org
 *
 * @package kernel
 */
class XoopsAvatar extends XoopsObject
{
    /**
     * @var int
     */
    private $_userCount;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('avatar_id', XOBJ_DTYPE_INT, null, false);
        $this->initVar('avatar_file', XOBJ_DTYPE_OTHER, null, false, 30);
        $this->initVar('avatar_name', XOBJ_DTYPE_TXTBOX, null, true, 100);
        $this->initVar('avatar_mimetype', XOBJ_DTYPE_OTHER, null, false);
        $this->initVar('avatar_created', XOBJ_DTYPE_INT, null, false);
        $this->initVar('avatar_display', XOBJ_DTYPE_INT, 1, false);
        $this->initVar('avatar_weight', XOBJ_DTYPE_INT, 0, false);
        $this->initVar('avatar_type', XOBJ_DTYPE_OTHER, 0, false);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function id($format = 'n')
    {
        return $this->getVar('avatar_id', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_id($format = '')
    {
        return $this->getVar('avatar_id', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_file($format = '')
    {
        return $this->getVar('avatar_file', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_name($format = '')
    {
        return $this->getVar('avatar_name', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_mimetype($format = '')
    {
        return $this->getVar('avatar_mimetype', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_created($format = '')
    {
        return $this->getVar('avatar_created', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_display($format = '')
    {
        return $this->getVar('avatar_display', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_weight($format = '')
    {
        return $this->getVar('avatar_weight', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function avatar_type($format = '')
    {
        return $this->getVar('avatar_type', $format);
    }

    /**
     * Set User Count
     *
     * @param int $value
     */
    public function setUserCount($value)
    {
        $this->_userCount = intval($value);
    }

    /**
     * Get User Count
     *
     * @return unknown
     */
    public function getUserCount()
    {
        return $this->_userCount;
    }
}

/**
 * XOOPS avatar handler class. (Singleton)
 *
 * This class is responsible for providing data access mechanisms to the data source
 * of XOOPS avatar class objects.
 *
 * @author  Kazumi Ono <onokazu@xoops.org>
 * @copyright copyright (c) 2000 XOOPS.org
 * @package kernel
 */
class XoopsAvatarHandler extends XoopsPersistableObjectHandler
{
    /**
     * Constructor
     *
     * @param XoopsDatabase|null $db {@link XoopsDatabase}
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'avatar', 'XoopsAvatar', 'avatar_id', 'avatar_name');
    }

    /**
     * Fetch a row of objects from the database
     *
     * @param CriteriaElement|null $criteria
     * @param bool $id_as_key
     * @return array
     */
    public function getObjectsWithCount(CriteriaElement $criteria = null, $id_as_key = false)
    {
        $ret = array();
        $limit = $start = 0;
        $sql = 'SELECT a.*, COUNT(u.user_id) AS count FROM ' . $this->db->prefix('avatar') . ' a LEFT JOIN ' . $this->db->prefix('avatar_user_link') . ' u ON u.avatar_id=a.avatar_id';
        if (isset($criteria)) {
            $sql .= ' ' . $criteria->renderWhere();
            $sql .= ' GROUP BY a.avatar_id ORDER BY avatar_weight, avatar_id';
            $limit = $criteria->getLimit();
            $start = $criteria->getStart();
        }
        $result = $this->db->query($sql, $limit, $start);
        if (!$result) {
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $avatar = new XoopsAvatar();
            $avatar->assignVars($myrow);
            $avatar->setUserCount($myrow['count']);
            if (!$id_as_key) {
                $ret[] = $avatar;
            } else {
                $ret[$myrow['avatar_id']] = $avatar;
            }
            unset($avatar);
        }
        return $ret;
    }

    /**
     * Add user
     *
     * @param int $avatar_id
     * @param int $user_id
     * @return bool
     */
    public function addUser($avatar_id, $user_id)
    {
        $avatar_id = intval($avatar_id);
        $user_id = intval($user_id);
        if ($avatar_id < 1 || $user_id < 1) {
            return false;
        }
        $sql = sprintf("DELETE FROM %s WHERE user_id = %u", $this->db->prefix('avatar_user_link'), $user_id);
        $this->db->query($sql);
        $sql = sprintf("INSERT INTO %s (avatar_id, user_id) VALUES (%u, %u)", $this->db->prefix('avatar_user_link'), $avatar_id, $user_id);
        if (!$this->db->query($sql)) {
            return false;
        }
        return true;
    }

    /**
     * Get User
     *
     * @param XoopsAvatar $avatar
     * @return array
     */
    public function getUser(XoopsAvatar $avatar)
    {
        $ret = array();
        $sql = 'SELECT user_id FROM ' . $this->db->prefix('avatar_user_link') . ' WHERE avatar_id=' . $avatar->getVar('avatar_id');
        if (!$result = $this->db->query($sql)) {
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $ret[] = $myrow['user_id'];
        }
        return $ret;
    }

    /**
     * Get a list of Avatars
     *
     * @param string $avatar_type
     * @param string $avatar_display
     * @return array
     */
    public function getListByType($avatar_type = null, $avatar_display = null)
    {
        $criteria = new CriteriaCompo();
        if (isset($avatar_type)) {
            $avatar_type = ($avatar_type == 'C') ? 'C' : 'S';
            $criteria->add(new Criteria('avatar_type', $avatar_type));
        }
        if (isset($avatar_display)) {
            $criteria->add(new Criteria('avatar_display', intval($avatar_display)));
        }
        $avatars = $this->getObjects($criteria, true);
        $ret = array(
            'blank.gif' => _NONE
        );
        foreach (array_keys($avatars) as $i) {
            $ret[$avatars[$i]->getVar('avatar_file')] = $avatars[$i]->getVar('avatar_name');
        }
        return $ret;
    }
}