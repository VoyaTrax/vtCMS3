<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * XOOPS Kernel Class
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         kernel
 * @since           2.6.0
 * @author          trabis <lusopoemas@gmail.com>
 * @version         $Id: avataruserlink.php 8064 2011-11-06 01:17:21Z beckmi $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

class XoopsAvataruserlink extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('avatar_id', XOBJ_DTYPE_INT, null, true);
        $this->initVar('user_id', XOBJ_DTYPE_INT, null, true);
    }
}

class XoopsAvataruserlinkHandler extends XoopsPersistableObjectHandler
{
    /**
     * Constructor
     *
     * @param XoopsDatabase|null $db {@link XoopsDatabase}
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'avatar_user_link', 'XoopsAvataruserlink', 'avatar_id', 'user_id');
    }
}