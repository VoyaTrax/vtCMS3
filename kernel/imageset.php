<?php
/**
 * XOOPS Kernel Class
 *
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         kernel
 * @since           2.0.0
 * @author          Kazumi Ono (AKA onokazu) http://www.myweb.ne.jp/, http://jp.xoops.org/
 * @version         $Id: imageset.php 8064 2011-11-06 01:17:21Z beckmi $
 */

defined('XOOPS_ROOT_PATH') or die('Restricted access');

/**
 * XOOPS Image Sets
 *
 * @package     kernel
 * @author      Kazumi Ono  <onokazu@xoops.org>
 * @copyright   (c) 2000-2003 The Xoops Project - www.xoops.org
 */
class XoopsImageset extends XoopsObject
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initVar('imgset_id', XOBJ_DTYPE_INT, null, false);
        $this->initVar('imgset_name', XOBJ_DTYPE_TXTBOX, null, true, 50);
        $this->initVar('imgset_refid', XOBJ_DTYPE_INT, 0, false);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function id($format = 'n')
    {
        return $this->getVar('imgset_id', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function imgset_id($format = '')
    {
        return $this->getVar('imgset_id', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function imgset_name($format = '')
    {
        return $this->getVar('imgset_name', $format);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function imgset_refid($format = '')
    {
        return $this->getVar('imgset_refid', $format);
    }

}

/**
 * XOOPS imageset handler class.
 * This class is responsible for providing data access mechanisms to the data source
 * of XOOPS imageset class objects.
 *
 *
 * @author  Kazumi Ono <onokazu@xoops.org>
 */
class XoopsImagesetHandler extends XoopsPersistableObjectHandler
{
    /**
     * Constructor
     *
     * @param XoopsDatabase|null $db {@link XoopsDatabase}
     */
    public function __construct(XoopsDatabase $db = null)
    {
        parent::__construct($db, 'imgset', 'XoopsImageset', 'imgset_id', 'imgset_name');
    }

    /**
     * Load {@link XoopsImageSet}s from the database
     *
     * @param   CriteriaElement|null  $criteria   {@link CriteriaElement}
     * @param   boolean $id_as_key  Use the ID as key into the array
     * @return  array   Array of {@link XoopsImageSet} objects
     **/
    public function getObjects(CriteriaElement $criteria = null, $id_as_key = false)
    {
        $ret = array();
        $limit = $start = 0;
        $sql = 'SELECT DISTINCT i.* FROM ' . $this->db->prefix('imgset') . ' i LEFT JOIN ' . $this->db->prefix('imgset_tplset_link') . ' l ON l.imgset_id=i.imgset_id';
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' ' . $criteria->renderWhere();
            $limit = $criteria->getLimit();
            $start = $criteria->getStart();
        }
        $result = $this->db->query($sql, $limit, $start);
        if (!$result) {
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $imgset = new XoopsImageset();
            $imgset->assignVars($myrow);
            if (!$id_as_key) {
                $ret[] = $imgset;
            } else {
                $ret[$myrow['imgset_id']] = $imgset;
            }
            unset($imgset);
        }
        return $ret;
    }

    /**
     * Load {@link XoopsImage ThemeSet}s into a Database
     *
     * @param int $imgset_id
     * @param string $tplset_name
     * @return array
     */
    public function linkThemeset($imgset_id, $tplset_name)
    {
        $imgset_id = intval($imgset_id);
        $tplset_name = trim($tplset_name);
        if ($imgset_id <= 0 || $tplset_name == '') {
            return false;
        }
        if (!$this->unlinkThemeset($imgset_id, $tplset_name)) {
            return false;
        }
        $sql = sprintf("INSERT INTO %s (imgset_id, tplset_name) VALUES (%u, %s)", $this->db->prefix('imgset_tplset_link'), $imgset_id, $this->db->quoteString($tplset_name));
        $result = $this->db->query($sql);
        if (!$result) {
            return false;
        }
        return true;
    }

    /**
     * Load {@link XoopsImage ThemeSet}s into a Database
     *
     * @param int $imgset_id
     * @param string $tplset_name
     * @return array
     */
    public function unlinkThemeset($imgset_id, $tplset_name)
    {
        $imgset_id = intval($imgset_id);
        $tplset_name = trim($tplset_name);
        if ($imgset_id <= 0 || $tplset_name == '') {
            return false;
        }
        $sql = sprintf("DELETE FROM %s WHERE imgset_id = %u AND tplset_name = %s", $this->db->prefix('imgset_tplset_link'), $imgset_id, $this->db->quoteString($tplset_name));
        $result = $this->db->query($sql);
        if (!$result) {
            return false;
        }
        return true;
    }

    /**
     * Get a list of XoopsImageSet
     *
     * @param int|null $refid
     * @param string|null $tplset
     * @return array Array of {@link XoopsImageSet} objects
     */
    public function getList($refid = null, $tplset = null)
    {
        $criteria = new CriteriaCompo();
        if (isset($refid)) {
            $criteria->add(new Criteria('imgset_refid', intval($refid)));
        }
        if (isset($tplset)) {
            $criteria->add(new Criteria('tplset_name', $tplset));
        }
        $imgsets = $this->getObjects($criteria, true);
        $ret = array();
        foreach (array_keys($imgsets) as $i) {
            $ret[$i] = $imgsets[$i]->getVar('imgset_name');
        }
        return $ret;
    }
}
?>