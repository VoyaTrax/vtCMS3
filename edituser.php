<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 *  Xoops Edit User
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package         core
 * @since           2.0.0
 * @version         $Id: edituser.php 10764 2013-01-11 19:25:11Z trabis $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mainfile.php';

$xoops = Xoops::getInstance();
$xoops->preload()->triggerEvent('core.edituser.start');
$xoops->loadLanguage('user');

// If not a user, redirect
if (!$xoops->isUser()) {
    $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
    exit();
}

$request = Xoops_Request::getInstance();
// initialize $op variable
$op = $request->asStr('op', 'editprofile');

$myts = MyTextSanitizer::getInstance();
if ($op == 'saveuser') {
    if (!$xoops->security()->check()) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT . "<br />" . implode('<br />', $xoops->security()->getErrors()));
        exit();
    }
    $uid = $request->asInt('uid', 0);
    if (empty($uid) || $xoops->user->getVar('uid') != $uid) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
        exit();
    }
    $errors = array();
    if ($xoops->getConfig('allow_chgmail') == 1) {
        $email = $request->asStr('email', '');
        $email = $myts->stripSlashesGPC(trim($email));;
        if ($email == '' || ! $xoops->checkEmail($email)) {
            $errors[] = _US_INVALIDMAIL;
        }
    }
    $password = $request->asStr('password', '');
    $password = $myts->stripSlashesGPC(trim($password));
    if ($password != '') {
        if (strlen($password) < $xoops->getConfig('minpass')) {
            $errors[] = sprintf(_US_PWDTOOSHORT, $xoops->getConfig('minpass'));
        }
        $vpass = $request->asStr('vpass', '');
        $vpass = $myts->stripSlashesGPC(trim($vpass));
        if ($password != $vpass) {
            $errors[] = _US_PASSNOTSAME;
        }
    }
    if (count($errors) > 0) {
        $xoops->header();
        echo '<div>';
        foreach ($errors as $er) {
            echo '<span class="red bold">' . $er . '</span><br />';
        }
        echo '</div><br />';
        $op = 'editprofile';
    } else {
        $member_handler = $xoops->getHandlerMember();
        $edituser = $member_handler->getUser($uid);
        $edituser->setVar('name', $request->asStr('name', ''));
        if ($xoops->getConfig('allow_chgmail') == 1) {
            $edituser->setVar('email', $email, true);
        }
        if ($password != '') {
            $edituser->setVar('pass', md5($password), true);
        }
        $edituser->setVar('url', $xoops->formatURL($request->asStr('url', '')));
        $edituser->setVar('user_icq', $request->asStr('user_icq', ''));
        $edituser->setVar('user_from', $request->asStr('user_from', ''));
        $edituser->setVar('user_sig', XoopsLocal::substr($request->asStr('user_sig', ''), 0, 255));
        $edituser->setVar('user_viewemail', $request->asBool('user_viewemail', 0));
        $edituser->setVar('user_aim', $request->asStr('user_aim', ''));
        $edituser->setVar('user_yim', $request->asStr('user_yim', ''));
        $edituser->setVar('user_msnm', $request->asStr('user_msnm', ''));
        $edituser->setVar('attachsig', $request->asBool('attachsig', 0));
        $edituser->setVar('timezone_offset', $request->asFloat('timezone_offset', 0));
        $edituser->setVar('uorder', $request->asInt('uorder', 0));
        $edituser->setVar('umode', $request->asStr('umode', 'flat'));
        $edituser->setVar('notify_method', $request->asInt('notify_method', 1));
        $edituser->setVar('notify_mode', $request->asInt('notify_mode', 1));
        $edituser->setVar('bio', XoopsLocal::substr($request->asStr('bio', ''), 0, 255));
        $edituser->setVar('user_occ', $request->asStr('user_occ', ''));
        $edituser->setVar('user_intrest', $request->asStr('user_intrest', ''));
        $edituser->setVar('user_mailok', $request->asBool('user_mailok', 0));
        $usecookie = $request->asBool('user_mailok', 0);
        if (!$usecookie) {
            setcookie($xoops->getConfig('usercookie'), $xoops->user->getVar('uname'), time() + 31536000, '/', XOOPS_COOKIE_DOMAIN);
        } else {
            setcookie($xoops->getConfig('usercookie'));
        }
        if (! $member_handler->insertUser($edituser)) {
            $xoops->header();
            echo $edituser->getHtmlErrors();
            $xoops->footer();
        } else {
            $xoops->redirect('userinfo.php?uid=' . $uid, 1, _US_PROFUPDATED);
        }
        exit();
    }
}

if ($op == 'editprofile') {
    $xoops->header('system_edituser.html');
    $xoops->tpl()->assign('uid', $xoops->user->getVar("uid"));
    $xoops->tpl()->assign('editprofile', true);
    include_once $xoops->path('include/comment_constants.php');
    $form = new XoopsThemeForm(_US_EDITPROFILE, 'userinfo', 'edituser.php', 'post', true);
    $uname_label = new XoopsFormLabel(_US_NICKNAME, $xoops->user->getVar('uname'));
    $form->addElement($uname_label);
    $name_text = new XoopsFormText(_US_REALNAME, 'name', 30, 60, $xoops->user->getVar('name', 'E'));
    $form->addElement($name_text);
    $email_tray = new XoopsFormElementTray(_US_EMAIL, '<br />');
    if ($xoops->getConfig('allow_chgmail') == 1) {
        $email_text = new XoopsFormText('', 'email', 30, 60, $xoops->user->getVar('email'));
    } else {
        $email_text = new XoopsFormLabel('', $xoops->user->getVar('email'));
    }
    $email_tray->addElement($email_text);
    $email_cbox_value = $xoops->user->user_viewemail() ? 1 : 0;
    $email_cbox = new XoopsFormCheckBox('', 'user_viewemail', $email_cbox_value);
    $email_cbox->addOption(1, _US_ALLOWVIEWEMAIL);
    $email_tray->addElement($email_cbox);
    $form->addElement($email_tray);
    $url_text = new XoopsFormText(_US_WEBSITE, 'url', 30, 100, $xoops->user->getVar('url', 'E'));
    $form->addElement($url_text);

    $timezone_select = new XoopsFormSelectTimezone(_US_TIMEZONE, 'timezone_offset', $xoops->user->getVar('timezone_offset'));
    $icq_text = new XoopsFormText(_US_ICQ, 'user_icq', 15, 15, $xoops->user->getVar('user_icq', 'E'));
    $aim_text = new XoopsFormText(_US_AIM, 'user_aim', 18, 18, $xoops->user->getVar('user_aim', 'E'));
    $yim_text = new XoopsFormText(_US_YIM, 'user_yim', 25, 25, $xoops->user->getVar('user_yim', 'E'));
    $msnm_text = new XoopsFormText(_US_MSNM, 'user_msnm', 30, 100, $xoops->user->getVar('user_msnm', 'E'));
    $location_text = new XoopsFormText(_US_LOCATION, 'user_from', 30, 100, $xoops->user->getVar('user_from', 'E'));
    $occupation_text = new XoopsFormText(_US_OCCUPATION, 'user_occ', 30, 100, $xoops->user->getVar('user_occ', 'E'));
    $interest_text = new XoopsFormText(_US_INTEREST, 'user_intrest', 30, 150, $xoops->user->getVar('user_intrest', 'E'));
    $sig_tray = new XoopsFormElementTray(_US_SIGNATURE, '<br />');
    $sig_tarea = new XoopsFormDhtmlTextArea('', 'user_sig', $xoops->user->getVar('user_sig', 'E'));
    $sig_tray->addElement($sig_tarea);
    $sig_cbox_value = $xoops->user->getVar('attachsig') ? 1 : 0;
    $sig_cbox = new XoopsFormCheckBox('', 'attachsig', $sig_cbox_value);
    $sig_cbox->addOption(1, _US_SHOWSIG);
    $sig_tray->addElement($sig_cbox);
    $umode_select = new XoopsFormSelect(_US_CDISPLAYMODE, 'umode', $xoops->user->getVar('umode'));
    $umode_select->addOptionArray(array(
        'nest' => _NESTED ,
        'flat' => _FLAT ,
        'thread' => _THREADED));
    $uorder_select = new XoopsFormSelect(_US_CSORTORDER, 'uorder', $xoops->user->getVar('uorder'));
    $uorder_select->addOptionArray(array(
        XOOPS_COMMENT_OLD1ST => _OLDESTFIRST ,
        XOOPS_COMMENT_NEW1ST => _NEWESTFIRST));
    // RMV-NOTIFY
    // TODO: add this to admin user-edit functions...
    $xoops->loadLanguage('notification');
    include_once $xoops->path('include/notification_constants.php');
    $notify_method_select = new XoopsFormSelect(_NOT_NOTIFYMETHOD, 'notify_method', $xoops->user->getVar('notify_method'));
    $notify_method_select->addOptionArray(array(
        XOOPS_NOTIFICATION_METHOD_DISABLE => _NOT_METHOD_DISABLE ,
        XOOPS_NOTIFICATION_METHOD_PM => _NOT_METHOD_PM ,
        XOOPS_NOTIFICATION_METHOD_EMAIL => _NOT_METHOD_EMAIL));
    $notify_mode_select = new XoopsFormSelect(_NOT_NOTIFYMODE, 'notify_mode', $xoops->user->getVar('notify_mode'));
    $notify_mode_select->addOptionArray(array(
        XOOPS_NOTIFICATION_MODE_SENDALWAYS => _NOT_MODE_SENDALWAYS ,
        XOOPS_NOTIFICATION_MODE_SENDONCETHENDELETE => _NOT_MODE_SENDONCE ,
        XOOPS_NOTIFICATION_MODE_SENDONCETHENWAIT => _NOT_MODE_SENDONCEPERLOGIN));
    $bio_tarea = new XoopsFormTextArea(_US_EXTRAINFO, 'bio', $xoops->user->getVar('bio', 'E'));
    $cookie_radio_value = empty($_COOKIE[$xoops->getConfig('usercookie')]) ? 0 : 1;
    $cookie_radio = new XoopsFormRadioYN(_US_USECOOKIE, 'usecookie', $cookie_radio_value, _YES, _NO);
    $pwd_text = new XoopsFormPassword('', 'password', 10, 32);
    $pwd_text2 = new XoopsFormPassword('', 'vpass', 10, 32);
    $pwd_tray = new XoopsFormElementTray(_US_PASSWORD . '<br />' . _US_TYPEPASSTWICE);
    $pwd_tray->addElement($pwd_text);
    $pwd_tray->addElement($pwd_text2);
    $mailok_radio = new XoopsFormRadioYN(_US_MAILOK, 'user_mailok', $xoops->user->getVar('user_mailok'));
    $uid_hidden = new XoopsFormHidden('uid', $xoops->user->getVar('uid'));
    $op_hidden = new XoopsFormHidden('op', 'saveuser');
    $submit_button = new XoopsFormButton('', 'submit', _US_SAVECHANGES, 'submit');

    $form->addElement($timezone_select);
    $form->addElement($icq_text);
    $form->addElement($aim_text);
    $form->addElement($yim_text);
    $form->addElement($msnm_text);
    $form->addElement($location_text);
    $form->addElement($occupation_text);
    $form->addElement($interest_text);
    $form->addElement($sig_tray);
    $form->addElement($umode_select);
    $form->addElement($uorder_select);
    $form->addElement($notify_method_select);
    $form->addElement($notify_mode_select);
    $form->addElement($bio_tarea);
    $form->addElement($pwd_tray);
    $form->addElement($cookie_radio);
    $form->addElement($mailok_radio);
    $form->addElement($uid_hidden);
    $form->addElement($op_hidden);
    //$form->addElement($token_hidden);
    $form->addElement($submit_button);
    if ($xoops->getConfig('allow_chgmail') == 1) {
        $form->setRequired($email_text);
    }
    $form->display();
    $xoops->footer();
}

if ($op == 'avatarform') {
    if (!$xoops->isActiveModule('avatars')) {
        $xoops->redirect('index.php', 3, _NOPERM);
    }
    $helper = Avatars::getInstance();
    $xoops->header('system_edituser.html');
    $xoops->tpl()->assign('uid', $xoops->user->getVar("uid"));
    $xoops->tpl()->assign('avatarform', true);
    $oldavatar = $xoops->user->getVar('user_avatar');
    if (!empty($oldavatar) && $oldavatar != 'blank.gif') {
        $xoops->tpl()->assign('oldavatar', XOOPS_UPLOAD_URL . '/' . $oldavatar);
    }
    if ($xoops->getModuleConfig('avatars_allowupload', 'avatars') == 1 && $xoops->user->getVar('posts') >= $xoops->getModuleConfig('avatars_postsrequired', 'avatars')) {
        $form = new XoopsThemeForm(_US_UPLOADMYAVATAR, 'uploadavatar', 'edituser.php', 'post', true);
        $form->setExtra('enctype="multipart/form-data"');
        $form->addElement(new XoopsFormLabel(_US_MAXPIXEL, $xoops->getModuleConfig('avatars_imagewidth', 'avatars') . ' x ' . $xoops->getModuleConfig('avatars_imageheight', 'avatars')));
        $form->addElement(new XoopsFormLabel(_US_MAXIMGSZ, $xoops->getModuleConfig('avatars_imagefilesize', 'avatars')));
        $form->addElement(new XoopsFormFile(_US_SELFILE, 'avatarfile', $xoops->getModuleConfig('avatars_imagefilesize', 'avatars')), true);
        $form->addElement(new XoopsFormHidden('op', 'avatarupload'));
        $form->addElement(new XoopsFormHidden('uid', $xoops->user->getVar('uid')));
        $form->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
        $form->display();
    }
    $avatar_handler = $helper->getHandlerAvatar();
    $form2 = new XoopsThemeForm(_US_CHOOSEAVT, 'uploadavatar', 'edituser.php', 'post', true);
    $avatar_select = new XoopsFormSelect('', 'user_avatar', $xoops->user->getVar('user_avatar'));
    $avatar_list = $avatar_handler->getListByType('S', true);
    $avatar_selected = $xoops->user->getVar("user_avatar", "E");
    $avatar_selected = in_array($avatar_selected, array_keys($avatar_list)) ? $avatar_selected : "blank.gif";
    $avatar_select->addOptionArray($avatar_list);
    $avatar_select->setExtra("onchange='showImgSelected(\"avatar\", \"user_avatar\", \"uploads\", \"\", \"" . XOOPS_URL . "\")'");
    $avatar_tray = new XoopsFormElementTray(_US_AVATAR, '&nbsp;');
    $avatar_tray->addElement($avatar_select);
    $avatar_tray->addElement(new XoopsFormLabel('', "<a href=\"javascript:openWithSelfMain('" . XOOPS_URL . "/modules/avatars/popup.php','avatars',600,400);\">" . _LIST . "</a><br />"));
    $avatar_tray->addElement(new XoopsFormLabel('', "<br /><img src='" . XOOPS_UPLOAD_URL . "/" . $avatar_selected . "' name='avatar' id='avatar' alt='' />"));
    $form2->addElement($avatar_tray);
    $form2->addElement(new XoopsFormHidden('uid', $xoops->user->getVar('uid')));
    $form2->addElement(new XoopsFormHidden('op', 'avatarchoose'));
    $form2->addElement(new XoopsFormButton('', 'submit2', _SUBMIT, 'submit'));
    $form2->display();
    $xoops->footer();
}

if ($op == 'avatarupload') {
    if (!$xoops->isActiveModule('avatars')) {
        $xoops->redirect('index.php', 3, _NOPERM);
    }
    if (!$xoops->security()->check()) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT . "<br />" . implode('<br />', $xoops->security()->getErrors()));
        exit();
    }
    $uid = $request->asInt('uid', 0);
    if (empty($uid) || $xoops->user->getVar('uid') != $uid) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
        exit();
    }

    $helper = Avatars::getInstance();

    $xoops_upload_file = $request->asArray('xoops_upload_file', array());

    if ( $xoops->getModuleConfig('avatars_allowupload', 'avatars') == 1 && $xoops->user->getVar('posts') >= $xoops->getModuleConfig('avatars_postsrequired', 'avatars') ) {
        $uploader = new XoopsMediaUploader( XOOPS_UPLOAD_PATH . '/avatars', array(
            'image/gif' ,
            'image/jpeg' ,
            'image/pjpeg' ,
            'image/x-png' ,
            'image/png'), $xoops->getModuleConfig('avatars_imagefilesize', 'avatars'), $xoops->getModuleConfig('avatars_imagewidth', 'avatars'), $xoops->getModuleConfig('avatars_imageheight', 'avatars'));
        if ($uploader->fetchMedia($xoops_upload_file[0])) {
            $uploader->setPrefix('cavt');
            if ($uploader->upload()) {
                $avatar_handler = $helper->getHandlerAvatar();
                $avatar = $avatar_handler->create();
                $avatar->setVar('avatar_file', 'avatars/' . $uploader->getSavedFileName());
                $avatar->setVar('avatar_name', $xoops->user->getVar('uname'));
                $avatar->setVar('avatar_mimetype', $uploader->getMediaType());
                $avatar->setVar('avatar_display', 1);
                $avatar->setVar('avatar_type', 'C');
                if (!$avatar_handler->insert($avatar)) {
                    @unlink($uploader->getSavedDestination());
                } else {
                    $oldavatar = $xoops->user->getVar('user_avatar');
                    if (! empty($oldavatar) && false !== strpos(strtolower($oldavatar), "cavt")) {
                        $avatars = $avatar_handler->getObjects(new Criteria('avatar_file', $oldavatar));
                        if (! empty($avatars) && count($avatars) == 1 && is_object($avatars[0])) {
                            $avatar_handler->delete($avatars[0]);
                            $oldavatar_path = realpath(XOOPS_UPLOAD_PATH . '/' . $oldavatar);
                            if (0 === strpos($oldavatar_path, XOOPS_UPLOAD_PATH) && is_file($oldavatar_path)) {
                                unlink($oldavatar_path);
                            }
                        }
                    }
                    $sql = sprintf("UPDATE %s SET user_avatar = %s WHERE uid = %u", $xoops->db()->prefix('users'), $xoops->db()->quoteString( 'avatars/' . $uploader->getSavedFileName()), $xoops->user->getVar('uid'));
                    $xoops->db()->query($sql);
                    $avatar_handler->addUser($avatar->getVar('avatar_id'), $xoops->user->getVar('uid'));
                    $xoops->redirect('userinfo.php?t=' . time() . '&amp;uid=' . $xoops->user->getVar('uid'), 3, _US_PROFUPDATED);
                }
            }
        }
        $xoops->redirect("edituser.php?op=avatarform", 3, $uploader->getErrors());
    }
}

if ($op == 'avatarchoose') {
    if (!$xoops->isActiveModule('avatars')) {
        $xoops->redirect('index.php', 3, _NOPERM);
    }
    if (!$xoops->security()->check()) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT . "<br />" . implode('<br />', $xoops->security()->getErrors()));
    }
    $uid = $request->asInt('uid', 0);
    if (empty($uid) || $xoops->user->getVar('uid') != $uid) {
        $xoops->redirect('index.php', 3, _US_NOEDITRIGHT);
        exit();
    }
    $helper = Avatars::getInstance();

    $user_avatar = $request->asStr('user_avatar', 'avatars/blank.gif');
    $avatar_handler = $helper->getHandlerAvatar();
    $user_avatarpath = realpath(XOOPS_UPLOAD_PATH . '/' . $user_avatar);
    if (0 === strpos($user_avatarpath, realpath(XOOPS_UPLOAD_PATH)) && is_file($user_avatarpath)) {
        $oldavatar = $xoops->user->getVar('user_avatar');
        $xoops->user->setVar('user_avatar', $user_avatar);
        $member_handler = $xoops->getHandlerMember();
        if (!$member_handler->insertUser($xoops->user)) {
            $xoops->header();
            echo $xoops->user->getHtmlErrors();
            $xoops->footer();
        }
        if ($oldavatar && preg_match("/^cavt/", strtolower(substr($oldavatar,8)))) {
            $avatars = $avatar_handler->getObjects(new Criteria('avatar_file', $oldavatar));
            if (!empty($avatars) && count($avatars) == 1 && is_object($avatars[0])) {
                $avatar_handler->delete($avatars[0]);
                $oldavatar_path = realpath(XOOPS_UPLOAD_PATH . '/' . $oldavatar);
                if (0 === strpos($oldavatar_path, realpath(XOOPS_UPLOAD_PATH)) && is_file($oldavatar_path)) {
                    unlink($oldavatar_path);
                }
            }
        }
        if ($user_avatar != 'avatars/blank.gif') {
            $avatars = $avatar_handler->getObjects(new Criteria('avatar_file', $user_avatar));
            if (is_object($avatars[0])) {
                $avatar_handler->addUser($avatars[0]->getVar('avatar_id'), $xoops->user->getVar('uid'));
            }
        }
    }
    $xoops->redirect('userinfo.php?uid=' . $uid, 0, _US_PROFUPDATED);
}