<?php
/*
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/**
 * XOOPS User
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU General Public License (GPL)
 * @package         core
 * @since           2.0.0
 * @author          Kazumi Ono <webmaster@myweb.ne.jp>
 * @version         $Id: userinfo.php 10633 2013-01-02 22:33:15Z dugris $
 */

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'mainfile.php';

$xoops = Xoops::getInstance();
$xoops->preload()->triggerEvent('core.userinfo.start');

$xoops->loadLanguage('user');
include_once $xoops->path('modules/system/constants.php');

$uid = intval($_GET['uid']);
if ($uid <= 0) {
    $xoops->redirect('index.php', 3, _US_SELECTNG);
    exit();
}
$gperm_handler = $xoops->getHandlerGroupperm();
$groups = $xoops->isUser() ? $xoops->user->getGroups() : XOOPS_GROUP_ANONYMOUS;

$isAdmin = $gperm_handler->checkRight('system_admin', XOOPS_SYSTEM_USER, $groups);
if ($xoops->isUser()) {
    if ($uid == $xoops->user->getVar('uid')) {
        $xoopsConfigUser = $xoops->getConfigs();
        $xoops->header('system_userinfo.html');
        $xoops->tpl()->assign('user_ownpage', true);
        $xoops->tpl()->assign('lang_editprofile', _US_EDITPROFILE);
        $xoops->tpl()->assign('lang_avatar', _US_AVATAR);
        $xoops->tpl()->assign('lang_inbox', _US_INBOX);
        $xoops->tpl()->assign('lang_logout', _US_LOGOUT);
        if ($xoopsConfigUser['self_delete'] == 1) {
            $xoops->tpl()->assign('user_candelete', true);
            $xoops->tpl()->assign('lang_deleteaccount', _US_DELACCOUNT);
        } else {
            $xoops->tpl()->assign('user_candelete', false);
        }
        $thisUser = $xoops->user;
    } else {
        $member_handler = $xoops->getHandlerMember();
        $thisUser = $member_handler->getUser($uid);
        if (!is_object($thisUser) || !$thisUser->isActive()) {
            $xoops->redirect("index.php", 3, _US_SELECTNG);
        }
        $xoops->header('system_userinfo.html');
        $xoops->tpl()->assign('user_ownpage', false);
    }
} else {
    $member_handler = $xoops->getHandlerMember();
    $thisUser = $member_handler->getUser($uid);
    if (!is_object($thisUser) || !$thisUser->isActive()) {
        $xoops->redirect("index.php", 3, _US_SELECTNG);
    }
    $xoops->header('system_userinfo.html');
    $xoops->tpl()->assign('user_ownpage', false);
}
$myts = MyTextSanitizer::getInstance();
if ($xoops->isUser() && $isAdmin) {
    $xoops->tpl()->assign('lang_editprofile', _US_EDITPROFILE);
    $xoops->tpl()->assign('lang_deleteaccount', _US_DELACCOUNT);
    $xoops->tpl()->assign('user_uid', $thisUser->getVar('uid'));
}

if ($xoops->isActiveModule('avatars')) {
    $xoops->tpl()->assign('avatars', true);
}else{
    $xoops->tpl()->assign('avatars', false);
}

$xoops->tpl()->assign('xoops_pagetitle', sprintf(_US_ALLABOUT, $thisUser->getVar('uname')));
$xoops->tpl()->assign('lang_allaboutuser', sprintf(_US_ALLABOUT, $thisUser->getVar('uname')));
$xoops->tpl()->assign('lang_avatar', _US_AVATAR);

$avatar = "";
if ($thisUser->getVar('user_avatar') && "blank.gif" != $thisUser->getVar('user_avatar')) {
    $avatar = XOOPS_UPLOAD_URL . "/" . $thisUser->getVar('user_avatar');
}
$xoops->tpl()->assign('user_avatarurl', $avatar);
$xoops->tpl()->assign('lang_realname', _US_REALNAME);
$xoops->tpl()->assign('user_realname', $thisUser->getVar('name'));
$xoops->tpl()->assign('lang_website', _US_WEBSITE);
if ($thisUser->getVar('url', 'E') == '') {
    $xoops->tpl()->assign('user_websiteurl', '');
} else {
    $xoops->tpl()->assign('user_websiteurl', '<a href="' . $thisUser->getVar('url', 'E') . '" rel="external">' . $thisUser->getVar('url') . '</a>');
}
$xoops->tpl()->assign('lang_email', _US_EMAIL);
$xoops->tpl()->assign('lang_privmsg', _US_PM);
$xoops->tpl()->assign('lang_icq', _US_ICQ);
$xoops->tpl()->assign('user_icq', $thisUser->getVar('user_icq'));
$xoops->tpl()->assign('lang_aim', _US_AIM);
$xoops->tpl()->assign('user_aim', $thisUser->getVar('user_aim'));
$xoops->tpl()->assign('lang_yim', _US_YIM);
$xoops->tpl()->assign('user_yim', $thisUser->getVar('user_yim'));
$xoops->tpl()->assign('lang_msnm', _US_MSNM);
$xoops->tpl()->assign('user_msnm', $thisUser->getVar('user_msnm'));
$xoops->tpl()->assign('lang_location', _US_LOCATION);
$xoops->tpl()->assign('user_location', $thisUser->getVar('user_from'));
$xoops->tpl()->assign('lang_occupation', _US_OCCUPATION);
$xoops->tpl()->assign('user_occupation', $thisUser->getVar('user_occ'));
$xoops->tpl()->assign('lang_interest', _US_INTEREST);
$xoops->tpl()->assign('user_interest', $thisUser->getVar('user_intrest'));
$xoops->tpl()->assign('lang_extrainfo', _US_EXTRAINFO);
$var = $thisUser->getVar('bio', 'N');
$xoops->tpl()->assign('user_extrainfo', $myts->displayTarea($var, 0, 1, 1));
$xoops->tpl()->assign('lang_statistics', _US_STATISTICS);
$xoops->tpl()->assign('lang_membersince', _US_MEMBERSINCE);
$var = $thisUser->getVar('user_regdate');
$xoops->tpl()->assign('user_joindate', XoopsLocal::formatTimestamp($var, 's'));
$xoops->tpl()->assign('lang_rank', _US_RANK);
$xoops->tpl()->assign('lang_posts', _US_POSTS);
$xoops->tpl()->assign('lang_basicInfo', _US_BASICINFO);
$xoops->tpl()->assign('lang_more', _US_MOREABOUT);
$xoops->tpl()->assign('lang_myinfo', _US_MYINFO);
$xoops->tpl()->assign('user_posts', $thisUser->getVar('posts'));
$xoops->tpl()->assign('lang_lastlogin', _US_LASTLOGIN);
$xoops->tpl()->assign('lang_notregistered', _US_NOTREGISTERED);
$xoops->tpl()->assign('lang_signature', _US_SIGNATURE);
$xoops->tpl()->assign('lang_posts', _US_POSTS);
$var = $thisUser->getVar('user_sig', 'N');
$xoops->tpl()->assign('user_signature', $myts->displayTarea($var, 0, 1, 1));
if ($thisUser->getVar('user_viewemail') == 1) {
    $xoops->tpl()->assign('user_email', $thisUser->getVar('email', 'E'));
} else {
    if ($xoops->isUser()) {
        // All admins will be allowed to see emails, even those that are not allowed to edit users (I think it's ok like this)
        if ($xoops->userIsAdmin || ($xoops->user->getVar("uid") == $thisUser->getVar("uid"))) {
            $xoops->tpl()->assign('user_email', $thisUser->getVar('email', 'E'));
        } else {
            $xoops->tpl()->assign('user_email', '&nbsp;');
        }
    }
}
if ($xoops->isUser()) {
    $xoops->tpl()->assign('user_pmlink', "<a href=\"javascript:openWithSelfMain('" . XOOPS_URL . "/pmlite.php?send2=1&amp;to_userid=" . $thisUser->getVar('uid') . "', 'pmlite', 450, 380);\"><img src=\"" . XOOPS_URL . "/images/icons/pm.gif\" alt=\"" . sprintf(_SENDPMTO, $thisUser->getVar('uname')) . "\" /></a>");
} else {
    $xoops->tpl()->assign('user_pmlink', '');
}
if ($xoops->isActiveModule('userrank')) {
    $userrank = $thisUser->rank();
    if (isset($userrank['image']) && $userrank['image']) {
        $xoops->tpl()->assign('user_rankimage', '<img src="' . XOOPS_UPLOAD_URL . '/' . $userrank['image'] . '" alt="" />');
    }
    $xoops->tpl()->assign('user_ranktitle', $userrank['title']);
}
$date = $thisUser->getVar("last_login");
if (!empty($date)) {
    $xoops->tpl()->assign('user_lastlogin', XoopsLocal::formatTimestamp($date, "m"));
}

$module_handler = $xoops->getHandlerModule();
$criteria = new CriteriaCompo(new Criteria('hasmain', 1));
$criteria->add(new Criteria('isactive', 1));
$criteria->add(new Criteria('weight', 0, '>'));
$modules = $module_handler->getObjectsArray($criteria, true);
$moduleperm_handler = $xoops->getHandlerGroupperm();
$groups = $xoops->isUser() ? $xoops->user->getGroups() : XOOPS_GROUP_ANONYMOUS;
$read_allowed = $moduleperm_handler->getItemIds('module_read', $groups);

foreach (array_keys($modules) as $i) {    if (in_array($i, $read_allowed)) {        $plugin = Xoops_Module_Plugin::getPlugin($modules[$i]->getVar('dirname'), 'search');
        if (method_exists($plugin, 'search')) {            $results = $plugin->search('', '', 5, 0, $thisUser->getVar('uid'));

            if (is_array($results) && count($results) > 0) {                $count = count($results);

                foreach ($results as $k => $result) {                    if (isset($result['image']) && $result['image'] != '') {
                        $results[$k]['image'] = $xoops->url('modules/' . $modules[$i]->getVar('dirname') . '/' . $result['image']);
                    } else {
                        $results[$k]['image'] = $xoops->url('images/icons/posticon2.gif');
                    }

                    if (!preg_match("/^http[s]*:\/\//i", $result['link'])) {
                        $results[$k]['link'] = $xoops->url("modules/" . $modules[$i]->getVar('dirname') . "/" . $result['link']);
                    }

                    $results[$k]['title'] = $myts->htmlspecialchars($result['title']);
                    $results[$k]['title_highligh'] = $myts->htmlspecialchars($result['title']);
                    if (!empty($result['time'])) {
                        $results[$k]['time'] = $result['time'] ? XoopsLocal::formatTimestamp($result['time']) : '';
                    }
                    if (!empty($results[$k]['uid'])) {
                        $results[$k]['uid'] = @intval($results[$k]['uid']);
                        $results[$k]['uname'] = XoopsUser::getUnameFromId($results[$k]['uid'], true);
                    }
                }
                if ($count == 5) {
                    $showall_link = '<a href="search.php?action=showallbyuser&amp;mid=' . $modules[$i]->getVar('mid') . '&amp;uid=' . $thisUser->getVar('uid') . '">' . _US_SHOWALL . '</a>';
                } else {
                    $showall_link = '';
                }
                $xoops->tpl()->append('modules', array(
                        'name' => $modules[$i]->getVar('name'),
                        'image' => $xoops->url('modules/' . $modules[$i]->getVar('dirname') . '/icons/logo_large.png'),
                        'result' => $results,
                        'showall_link' => $showall_link
                    ));

            }
        }
    }
}
$xoops->footer();